# twoBirds

![twoBirds logo](http://twobirds.gitlab.io/twobirds-core/dist/twoBirds.svg)

Welcome Birdies ;-)

**STATUS**
- refactoring: all outdated functionality was thrown out (and that was a lot).
- tB is written in and supports typeScript now
- System clientside runtime size minified is < 30kb for the lib.

**CAVEAT**
- we are heavily developing currently - semver rules do NOT apply to V9
- from v9.1 upwards tB may feature braking changes with every minor version.
- If you need newer functionality, there may be some search / replace necessary in your app code.
- if not, stay in a minor version level.
- Starting with V10, semver rules will apply.

**twoBirds is a** ...
- **object oriented** and **native class based**,
- lightweight,
- web component,
- event-driven,
- observable/data-driven,
- loosely coupled,
- vanilla,

... **javascript library** that maps class instances to DOM nodes.

- twoBirds is as easy as jQuery, but on application level.
- twoBirds seamlessly integrates with any other JS lib ( read: Angular, React, Ember, Svelte, ... you can mix in tB functionality or replace elements completely)

**Micro Components**

twoBirds uses a unique technology named [Micro Components](https://gitlab.com/twoBirds/twobirds-core/-/blob/master/MicroComponentProposal_draft.md?ref_type=heads) which I am developing a polyfill for and I plan to separate that into an RFC. Basically it adds an object/instance collection to the HTMLElement instances in the DOM. "Micro Component"s are the functional blocks that map to a dom node, and they can provide any functionality the dom offers. Also there is some functionality for easier coding included. You can use more than one "Micro Component" in one DOM node / HTML element, and you can use them in any other frontend library as well as these are not aware of them.

**What you can do with it**

- **add JS functionality to an existing webpage** in the most simple way, no matter which framework was used on the server side ([progressive enhancement](https://en.wikipedia.org/wiki/Progressive_enhancement))

- **build completely encapsuled CEs ( [Autonomous Custom Elements](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements) ) and UBEs ( [User-defined Built-in Elements](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements) )** to use in your own project or deliver as a standalone element to a customers web project.

- **build a [single page application](https://en.wikipedia.org/wiki/Single-page_application)** from ground up and optionally make it a [progressive web app](https://en.wikipedia.org/wiki/Progressive_Web_Apps)

- **mix all of the above** to optimize for search engines, or migrate an existing serverside rendered webpage to a SPA on the fly

- twoBirds uses **vanilla Javascript** or **typeScript** plus some conventions and utilities.

### Examples

```javascript
import {TB, CE} from 'twobirds-core';

class MyTest extends TB{
	constructor( target, init ){
		super( target, init );

		// all properties set in the constructor will become observables automatically!
		this.bool = false;
	}

	oneConnected(){
		// this is the web component "onConnected" callback
		// all methods with 'on[A-Z]...' or 'one[A-Z]...' will be connected to their listeners automatically
		// methods with 'one[A-Z]...' will only run once

		// minifier help
		let that = this;

		// a simple property, not an observable!
		that.str = 'Hello World';

		// react on this.bool change
		that.bool.observe( (v) => console.log('this.bool has changed to', v) );

		// callback should be triggered...
		that.bool = true;
	}
}
// here it is: your first web component as a ready-to-use tag!
new CE('my-test', MyTest);
// from now on you can simply insert <my-test> into your HTML and you have a working instance of this web component

```

and the same in typescript...


```typescript
import {TB, CE} from 'twobirds-core';

interface MyTest {
	bool: any; // because observable
	str: string;
}

class MyTest extends TB{
	constructor( target, init ){
		super( target, init );
		this.bool = false;
	}

	oneConnected(){
		let that = this;
		that.str = 'Hello World';
		that.bool.observe( (v) => console.log('this.bool has changed to', v) );
		that.bool = true;
	}
}

new CE('my-test', MyTest);

```

- There is no need to learn any abstraction layers that declarative frameworks like angular, react or svelte use. Your code base is pure object oriented Javascript, remember!

- tB application development is build-free while developing (when using vanilla JS), because it has a built-in lazy importer. TS must be compiled of course.

- tB debugging is dead easy: the browser internal debugger will give you full runtime insight into objects.

### Principles

tB strictly follows the [KISS](http://principles-wiki.net/principles:keep_it_simple_stupid) and [DRY](http://principles-wiki.net/principles:don_t_repeat_yourself) doctrines.

The tB core can be seen as an intermediate step between a simple wrapper/helper library like jQuery and a fully fledged JS framework.

Like the first, it has selectors for the DOM and reasonable modification methods.

Like the latter, it incorporates a higher level abstraction scheme for application objects.

tB technically aims to be the minimum possible toolkit to create an application.

[API documentation](http://twobirds.gitlab.io/twobirds-core/docs/modules.html)

(docs pure typescript, not annotated yet)

### Supplementary information // Outdated, To Be Done

[Use Patreon to support this](https://www.patreon.com/twoBirds)

[Donate via PayPal](https://paypal.me/FThuerigen)

You can also [contact me](mailTo:fthuerigen@googlemail.com?subject=regarding%20twoBirds%20...") if you need a specific twoBirds plugin tailored for your needs or if a question arises.
