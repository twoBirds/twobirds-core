import * as tb from 'twobirds-core';

const { createTb, TB, CE, $d, loadAll } = tb;

(() => {
	interface Dom {
		a: any;
		b: any;
		c: string;
		form: TestFormForm;
	}

	class Dom extends TB {
		constructor(tg: HTMLElement, i: any) {
			super(tg, i);

			// observable properties
			this.a = 41;
		}

		oneConnected() {
			let that = this;
			that.form = that.siblings('TestFormForm') as TestFormForm;

			that.a.observe((v: number) => {
				that.form.str = `Dom.a = ${v} now!`;
			});

			// normal properties
			that.b = 42;
			that.c = 'Hello World';
		}
	}

	interface TestFormForm {
		bool: any;
		num: any;
		str: any;
		dom: Dom;
		form: any;
		form2: any;
	}

	class TestFormForm extends TB {
		constructor(tg: HTMLElement, i: any) {
			super(tg, i);

			createTb(tg, Dom);

			// onservable properties
			Object.assign(this, {
				bool: true,
				num: 1,
				str: 'Hello World',
				form: {}, // 2way bind against form inputs later
				form2: {},
			});
		}

		oneConnected(ev: any) {
			let that = this,
				req = [
					'/test/form/form.shadowCss',
					'/test/form/form.html',
					'/test/form/form.json',
				];

			that.dom = that.siblings('Dom') as Dom;

			that.str.observe((v: any) => console.log(v));

			that.dom.a = 42;

			loadAll(...req).then((result: any) => {
				that.render(result);
			});
		}

		render(result: string[]) {
			let that = this,
				css = result[0],
				html = result[1],
				data = JSON.parse(result[2]);
			that.shadow(css); // create shadow and add css to it
			$d(that._sh).append(html); // add dom to shadowRoot

			that.values('form'); // 2 way bind input values to property named 'form' on every input field change

			that.form.observe((formValues: any) => {
				let changed = Object.assign(formValues, {
					textInput: formValues.textInput.toUpperCase(),
				});
				that.form2 = changed; // which in turn replaces DOM placeholders
			});

			that.form2.bind(that._sh); // observable 'form' on change update DOM placeholders

			that.form = data; // set data to form observable -> will update all inputs, and all DOM placeholders

			// this.values('formValues', data);
			// (this as any).formValues.bindDom(target);
		}
	}

	new CE('test-form-form', TestFormForm);
})();
