const { TB, CE, $d } = tb;
(() => {
    class TestFamilyChild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-grandchild/>'.repeat(3));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-child', TestFamilyChild);
})();
