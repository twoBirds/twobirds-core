/// <reference types="twobirds-core" />
import * as tb from 'twobirds-core';

const { TB, CE, $d } = tb;

(() => {
	class TestFamilyParent extends TB {
		constructor(target: HTMLElement, config: any) {
			super(target, config);
		}

		oneConnected(ev: any) {
			let fragment = $d('<test-family-child/>'.repeat(10));

			$d(this._tg).append(fragment);
		}
	}

	new CE('test-family-parent', TestFamilyParent);
})();
