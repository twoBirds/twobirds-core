const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGrandparent extends TB {
        constructor(target, config) {
            super(target, config);
        }
        onClick(ev) {
            console.log('click', ev);
        }
        oneConnected(ev) {
            let that = this, fragment = $d('<test-family-parent/>'.repeat(5));
            that.trigger('oneTest');
            that.trigger('oneTest');
            $d(that._tg).append(fragment);
        }
        oneTest(ev) {
        }
    }
    new CE('test-family-grandparent', TestFamilyGrandparent);
})();
