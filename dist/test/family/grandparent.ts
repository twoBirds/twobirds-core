/// <reference types="twobirds-core" />
import * as tb from 'twobirds-core';

const { TB, CE, $d } = tb;

(() => {
	class TestFamilyGrandparent extends TB {
		constructor(target: HTMLElement, config: any) {
			super(target, config);
		}

		onClick(ev: Event) {
			// listener for a DOM event
			console.log('click', ev);
		}

		oneConnected(ev: Event) {
			// callback for the web component specific 'connected' event, only once
			let that = this,
				fragment = $d('<test-family-parent/>'.repeat(5));

			that.trigger('oneTest');
			that.trigger('oneTest'); // this one will find no listener, since its defined as once = true, see below

			$d(that._tg).append(fragment);
		}

		oneTest(ev: Event) {
			// listener for a TB internal 'oneTest' event, only once
			// console.log('oneTest', ev);
		}
	}

	new CE('test-family-grandparent', TestFamilyGrandparent);
})();
