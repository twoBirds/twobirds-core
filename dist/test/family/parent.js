const { TB, CE, $d } = tb;
(() => {
    class TestFamilyParent extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-child/>'.repeat(10));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-parent', TestFamilyParent);
})();
