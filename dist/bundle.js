import * as tb from "/tb/tb.js";
(()=>{
const { createTb, TB, CE, $d, loadAll } = tb;
(() => {
    class Dom extends TB {
        constructor(tg, i) {
            super(tg, i);
            this.a = 41;
        }
        oneConnected() {
            let that = this;
            that.form = that.siblings('TestFormForm');
            that.a.observe((v) => {
                that.form.str = `Dom.a = ${v} now!`;
            });
            that.b = 42;
            that.c = 'Hello World';
        }
    }
    class TestFormForm extends TB {
        constructor(tg, i) {
            super(tg, i);
            createTb(tg, Dom);
            Object.assign(this, {
                bool: true,
                num: 1,
                str: 'Hello World',
                form: {},
                form2: {},
            });
        }
        oneConnected(ev) {
            let that = this, req = [
                '/test/form/form.shadowCss',
                '/test/form/form.html',
                '/test/form/form.json',
            ];
            that.dom = that.siblings('Dom');
            that.str.observe((v) => console.log(v));
            that.dom.a = 42;
            loadAll(...req).then((result) => {
                that.render(result);
            });
        }
        render(result) {
            let that = this, css = result[0], html = result[1], data = JSON.parse(result[2]);
            that.shadow(css);
            $d(that._sh).append(html);
            that.values('form');
            that.form.observe((formValues) => {
                let changed = Object.assign(formValues, {
                    textInput: formValues.textInput.toUpperCase(),
                });
                that.form2 = changed;
            });
            that.form2.bind(that._sh);
            that.form = data;
        }
    }
    new CE('test-form-form', TestFormForm);
})();
})();
(()=>{
const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGrandparent extends TB {
        constructor(target, config) {
            super(target, config);
        }
        onClick(ev) {
            console.log('click', ev);
        }
        oneConnected(ev) {
            let that = this, fragment = $d('<test-family-parent/>'.repeat(5));
            that.trigger('oneTest');
            that.trigger('oneTest');
            $d(that._tg).append(fragment);
        }
        oneTest(ev) {
        }
    }
    new CE('test-family-grandparent', TestFamilyGrandparent);
})();
})();
(()=>{
const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGreatgrandchild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected() {
            this.randomBorder();
        }
        randomBorder() {
            let that = this;
            setInterval(() => {
                that._tg.style.borderColor = randomBorderColor();
            }, Math.floor(Math.random() * 10000));
        }
    }
    function random(min, max) {
        var random = Math.floor(Math.random() * (max - min + 1) + min);
        return random;
    }
    function randomBorderColor() {
        return ('rgb(' +
            random(0, 255) +
            ',' +
            random(0, 255) +
            ',' +
            random(0, 255) +
            ')');
    }
    new CE('test-family-greatgrandchild', TestFamilyGreatgrandchild);
})();
})();
(()=>{
const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGrandchild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-greatgrandchild/>'.repeat(2));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-grandchild', TestFamilyGrandchild);
})();
})();
(()=>{
const { TB, CE, $d } = tb;
(() => {
    class TestFamilyParent extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-child/>'.repeat(10));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-parent', TestFamilyParent);
})();
})();
(()=>{
const { TB, CE, $d } = tb;
(() => {
    class TestFamilyChild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-grandchild/>'.repeat(3));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-child', TestFamilyChild);
})();
})();
