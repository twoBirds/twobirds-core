module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            clean: ['dist', 'build', 'build/jasmine', 'build/tbMin'],
        },

        exec: {
            rmBuildDist: {
                command: 'rm -rf build && rm -rf dist',
                stdout: true,
            },
            tsc: {
                command: 'npx tsc',
                stdout: true,
            },
            typedoc: {
                command: 'npx typedoc',
                stdout: true,
            },
            echo_grunt_version: {
                command: function (grunt) {
                    return 'echo ' + grunt.version;
                },
                stdout: true,
            },
        },

        copy: {
            before: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/tbSource',
                        src: '*.ts',
                        dest: 'build',
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/twobirds-tstest/dist',
                        src: '**',
                        dest: 'build/',
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/twobirds-tstest/dist',
                        src: '**',
                        dest: 'dist/',
                    },
                ],
            },
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'node_modules/jasmine-core/lib/jasmine-core',
                        src: ['*.js', '*.css'],
                        dest: 'build/jasmine',
                    },
                    {
                        expand: true,
                        cwd: 'src/tbJasmine',
                        src: '*.css',
                        dest: 'dist/jasmine',
                    },
                    {
                        expand: true,
                        cwd: 'src',
                        src: ['twoBirds.svg'],
                        dest: 'public/docs',
                    },
                    {
                        expand: true,
                        cwd: 'src',
                        src: ['twoBirds.svg'],
                        dest: 'dist',
                    },
                    {
                        expand: true,
                        cwd: 'dist/tb',
                        src: '*.js',
                        dest: 'src/tbDocs',
                    },
                    {
                        expand: true,
                        cwd: 'build/tbSource',
                        src: ['*.*'],
                        dest: 'dist/tb',
                    },
                    {
                        expand: true,
                        cwd: 'docs',
                        src: ['**/*.*'],
                        dest: 'dist/docs',
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/twobirds-tstest/dist',
                        src: ['**/*.*'],
                        dest: 'public/tstest',
                    },
                    {
                        expand: true,
                        cwd: 'dist/tb',
                        src: ['*.js'],
                        dest: 'public',
                    },
                ],
            },
        },

        concat: {
            options: {
                separator: '\n\n',
                //stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n\n',
            },
            dist: {
                files: {
                    'src/tb/tb.js': [
                        'src/tbSource/tb.core.js',
                        'src/tbSource/tb.dom.js',
                        'src/tbSource/tb.utils.js',
                        'src/tbSource/tb.es6.js',
                        'src/tbSource/tb.bootstrap.js',
                    ],
                    'dist/tb/tb.js': [
                        'src/tbSource/tb.core.js',
                        'src/tbSource/tb.dom.js',
                        'src/tbSource/tb.utils.js',
                        'src/tbSource/tb.es6.js',
                        'src/tbSource/tb.bootstrap.js',
                    ],
                },
                nonull: true,
            },
        },

        uglify: {
            options: {
                mangle: {},
            },
            my_target: {
                options: {
                    mangle: true,
                },
                files: [
                    {
                        'dist/tb/tb-min.js': ['src/tb/tb.js'],
                    },
                ],
            },
        },

        jshint: {
            options: {
                esversion: 6,
                force: true,
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: false,
                newcap: false,
                noarg: false,
                sub: true,
                undef: true,
                boss: true,
                debug: true,
                eqnull: true,
                node: true,
                laxbreak: true,
                laxcomma: true,
                globals: {
                    exports: true,
                    module: false,
                    tb: true,
                    window: false,
                    document: false,
                    HTMLElement: false,
                    Element: false,
                    customElements: false,
                    HTMLCollection: false,
                    NodeList: false,
                    XMLHttpRequest: false,
                    ActiveXObject: false,
                },
            },
            all: ['src/tbSource/*.js'],
        },

    });

    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Default task(s).
    grunt.registerTask('default', [
        'exec:rmBuildDist',
        'copy:before',
        'exec:tsc',
        'exec:typedoc',
        // 'concat',
        // 'yuidoc',
        // 'uglify',
        // 'jshint',
        'copy:main',
    ]);
};
