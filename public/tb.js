var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _InputHandler_e, _InputHandler_type, _InputHandler_radioBase, _SelectHandler_e, _SelectHandler_multi, _TextAreaHandler_e, _DomSelector_set, _TbSelector_set;
function isObject(value) {
    return value != null && typeof value === 'object';
}
const deepEqual = (o1, o2) => {
    const k1 = Object.keys(o1);
    const k2 = Object.keys(o2);
    if (k1.length !== k2.length) {
        return false;
    }
    for (const key of k1) {
        const v1 = o1[key], v2 = o2[key], recurse = isObject(v1) && isObject(v2);
        ;
        if ((recurse && !deepEqual(v1, v2))
            || (!recurse && v1 !== v2)) {
            return false;
        }
    }
    return true;
};
const _makeLoadData = (e) => {
    var _a, _b;
    let tagName = (_a = e === null || e === void 0 ? void 0 : e.localName) !== null && _a !== void 0 ? _a : '', isAttribute = ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')) || '', isUBE = isAttribute ? true : false, isCE = tagName.indexOf('-') !== -1 ? true : false, ubeSelector = isUBE ? `${tagName}[is=\"${isAttribute}\"]:defined` : '', ceSelector = isCE ? tagName + ':defined' : '', fileBaseName = isUBE
        ? isAttribute
        : isCE
            ? tagName
            : '', fileName = fileBaseName.length ? '/' + fileBaseName.split('-').join('/') + '.js' : '', selector = isUBE
        ? ubeSelector
        : isCE
            ? ceSelector
            : '';
    return (isUBE || isCE) ? [e, selector, fileName, fileBaseName] : false;
};
const words = (classes) => {
    let classSet = new Set();
    classes.forEach(c => {
        c = c.trim();
        c.split(' ').forEach(e => {
            if (e !== '') {
                classSet.add(e);
            }
        });
    });
    classes = Array.from(classSet);
    return classes;
};
const getInputName = (e) => {
    let ret = e instanceof HTMLInputElement
        ? 'input'
        : e instanceof HTMLSelectElement
            ? 'select'
            : e instanceof HTMLTextAreaElement
                ? 'textarea'
                : '';
    return ret;
};
const loadingScripts = new Set();
const _loadRequirements = (node) => {
    let undefinedSet = new Set(), element = node instanceof HTMLTemplateElement ? node.content : node, head = document.head, allPromises = [], e;
    ;
    if (element instanceof HTMLTemplateElement === false) {
        let data = _makeLoadData(element);
        if (data && !data[0].matches(data[1])) {
            undefinedSet.add(data);
        }
    }
    Array.from(element.childNodes).forEach((node) => {
        let data = _makeLoadData(node);
        if (data && !data[0].matches(data[1])) {
            undefinedSet.add(data);
        }
    });
    if (!!undefinedSet.size) {
        undefinedSet.forEach((data) => {
            if (!getCustomElement(data[3]) && !loadingScripts.has(data[2])) {
                loadingScripts.add(data[2]);
                const se = document.createElement('script');
                se.setAttribute('src', data[2]);
                se.setAttribute('blocking', 'render');
                se.setAttribute('type', 'module');
                let s = data[2];
                se.onload = () => {
                    loadingScripts.delete(s);
                };
                head.append(se);
                let e = data[0];
            }
            else {
                return;
            }
        });
    }
    return Promise.all(allPromises);
};
const _htmlToElements = (html) => {
    let template = document.createElement('template');
    html = html
        .replace(/<([A-Za-z0-9\-]*)([^>\/]*)(\/>)/gi, "<$1$2></$1>");
    template.innerHTML = html;
    template.content.normalize();
    if (template.content.childNodes.length) {
        _loadRequirements(template);
    }
    return Array.from(template.content.childNodes);
};
const _getElementList = (selector, rootNode) => {
    let result = new Set();
    ;
    if (!selector) {
        return [];
    }
    else if (typeof selector === 'string') {
        selector = selector
            .replace(/\r/g, '')
            .replace(/\n/g, '')
            .replace(/\t/g, '')
            .replace(/>\s*</g, '><')
            .trim();
        if (!selector.match(/^<.*>$/)) {
            const nodeList = rootNode.querySelectorAll(selector);
            nodeList.forEach((node) => {
                result.add(node);
            });
            return [...result];
        }
        else {
            return _htmlToElements(selector);
        }
    }
    else if (selector instanceof HTMLElement || selector instanceof ShadowRoot) {
        result.add(selector);
    }
    else if (selector instanceof DomSelector) {
        selector.forEach(e => {
            result.add(e);
        });
    }
    else if (selector instanceof TbSelector) {
        selector.forEach(e => {
            result.add(e._tg);
        });
    }
    else if (selector instanceof TB) {
        result.add(selector._tg);
    }
    else if (selector instanceof NodeList) {
        selector.forEach(n => {
            result.add(n);
        });
    }
    else if (selector instanceof HTMLCollection) {
        Array.from(selector).forEach(e => {
            result.add(e);
        });
    }
    else if (selector instanceof Array) {
        selector.forEach(e => {
            result.add(e);
        });
    }
    return [...result];
};
const _addListener = (domNode, eventName, handler, capture = false, once = false) => {
    let options = {};
    if (capture) {
        options.capture = capture;
    }
    if (once) {
        options.once = once;
    }
    domNode.addEventListener(eventName, handler, options);
};
const _removeListener = (domNode, eventName, handler) => {
    domNode.removeEventListener(eventName, handler);
};
const nativeEventNames = Object.keys(HTMLElement.prototype).filter(key => /^on/.test(key));
const _autoAttachListeners = (target, instance) => {
    Object
        .getOwnPropertyNames(instance.__proto__)
        .filter(key => /^on[A-Z]|one[A-Z]/.test(key))
        .forEach(key => {
        let once = /^one/.test(key), withoutOnOne = key.replace(/^on[e]{0,1}/, ''), nativeEventName = 'on' + withoutOnOne.toLowerCase(), listener = Object.getPrototypeOf(instance)[key].bind(instance), isNative = nativeEventNames.indexOf(nativeEventName) > -1;
        if (isNative) {
            _addListener(target, withoutOnOne.toLowerCase(), listener, false, once);
        }
        else {
            _addListener(target, '[TB]' + key, listener, true, once);
        }
    });
};
const getId = () => {
    let i;
    if (i = crypto === null || crypto === void 0 ? void 0 : crypto.randomUUID()) {
        return i;
    }
    return ('id-' +
        new Date().getTime() +
        '-' +
        Math.random().toString().replace(/\./, ''));
};
class InputBase {
    constructor(e) {
    }
}
const getInputHandlers = (e) => {
    let inputList = $d('input,select,textarea', e).array, handlers = {};
    inputList.forEach((i) => {
        const name = i.getAttribute('name') || 'undefined';
        if (name) {
            let h;
            switch (getInputName(i)) {
                case 'input':
                    h = new InputHandler(i, e);
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: () => { return h.val; },
                        set: (value) => { h.val = value; }
                    });
                    break;
                case 'select':
                    h = new SelectHandler(i);
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: () => { return h.val; },
                        set: (value) => { h.val = value; }
                    });
                    break;
                case 'textarea':
                    h = new TextAreaHandler(i);
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: () => { return h.val; },
                        set: (value) => { h.val = value; }
                    });
                    break;
                default:
                    console.warn('handler missing:', name, 'for', i);
                    break;
            }
        }
    });
    return handlers;
};
class InputHandler extends InputBase {
    constructor(e, radioBase) {
        super(e);
        _InputHandler_e.set(this, void 0);
        _InputHandler_type.set(this, void 0);
        _InputHandler_radioBase.set(this, void 0);
        __classPrivateFieldSet(this, _InputHandler_e, e, "f");
        __classPrivateFieldSet(this, _InputHandler_type, e.type, "f");
        __classPrivateFieldSet(this, _InputHandler_radioBase, radioBase, "f");
    }
    get val() {
        let that = this, type = __classPrivateFieldGet(that, _InputHandler_type, "f"), ret;
        switch (type) {
            case 'radio':
                const check = $d(`input[type="radio"][name="${__classPrivateFieldGet(this, _InputHandler_e, "f").getAttribute('name')}"]`, __classPrivateFieldGet(this, _InputHandler_radioBase, "f"))
                    .filter(e => e.checked);
                if (!check.length)
                    return '';
                ret = check.array[0];
                return ret.value;
            case 'checkbox':
                return __classPrivateFieldGet(that, _InputHandler_e, "f").checked;
            default:
                return __classPrivateFieldGet(that, _InputHandler_e, "f").value;
                break;
        }
        return '';
    }
    set val(value) {
        let that = this, type = __classPrivateFieldGet(that, _InputHandler_type, "f"), ret;
        switch (type) {
            case 'radio':
                const selection = `input[type="radio"][name="${__classPrivateFieldGet(this, _InputHandler_e, "f").getAttribute('name')}"]`, radios = $d(selection, __classPrivateFieldGet(this, _InputHandler_radioBase, "f"));
                if (!radios.length)
                    return;
                if (value === '') {
                    radios.forEach(r => { if (r.checked)
                        r.checked = false; });
                    return;
                }
                const setRadioElement = radios.filter(r => r.getAttribute('value') === value);
                if (!setRadioElement.length)
                    return;
                setRadioElement.array[0].checked = true;
                return;
                break;
            case 'checkbox':
                __classPrivateFieldGet(that, _InputHandler_e, "f").checked = value;
                break;
            default:
                __classPrivateFieldGet(that, _InputHandler_e, "f").value = value;
                break;
        }
        return;
    }
}
_InputHandler_e = new WeakMap(), _InputHandler_type = new WeakMap(), _InputHandler_radioBase = new WeakMap();
class SelectHandler extends InputBase {
    constructor(e) {
        super(e);
        _SelectHandler_e.set(this, void 0);
        _SelectHandler_multi.set(this, false);
        __classPrivateFieldSet(this, _SelectHandler_e, e, "f");
        __classPrivateFieldSet(this, _SelectHandler_multi, e.type === 'select-multiple', "f");
    }
    get val() {
        const that = this, ret = [];
        const options = $d(__classPrivateFieldGet(that, _SelectHandler_e, "f").querySelectorAll('option'))
            .filter(o => o.selected)
            .forEach(o => { if (o.selected)
            ret.push(o.value); });
        if (__classPrivateFieldGet(this, _SelectHandler_multi, "f"))
            return ret;
        return ret.length ? ret[0] : '';
    }
    set val(value) {
        const that = this;
        value = typeof value === 'string' ? [value] : value;
        const options = $d(__classPrivateFieldGet(that, _SelectHandler_e, "f").querySelectorAll('option'))
            .forEach(o => {
            if (value.indexOf(o.value) > -1) {
                o.setAttribute('selected', true);
                o.selected = true;
            }
            else {
                o.removeAttribute('selected');
                o.selected = false;
            }
        });
        return;
    }
}
_SelectHandler_e = new WeakMap(), _SelectHandler_multi = new WeakMap();
class TextAreaHandler extends InputBase {
    constructor(e) {
        super(e);
        _TextAreaHandler_e.set(this, void 0);
        __classPrivateFieldSet(this, _TextAreaHandler_e, e, "f");
    }
    get val() {
        return __classPrivateFieldGet(this, _TextAreaHandler_e, "f").value;
    }
    set val(value) {
        __classPrivateFieldGet(this, _TextAreaHandler_e, "f").value = value;
        return;
    }
}
_TextAreaHandler_e = new WeakMap();
class DomSelector {
    constructor(selection, element = document.body) {
        _DomSelector_set.set(this, new Set());
        let that = this, undefinedTags = new Set();
        if (!selection) {
            return that;
        }
        if (selection instanceof Array) {
            selection.forEach((select) => {
                _getElementList(select, element).forEach((e) => {
                    var _a, _b;
                    if (((_a = e.localName) === null || _a === void 0 ? void 0 : _a.indexOf('-')) !== -1 || ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')))
                        _loadRequirements(e);
                    __classPrivateFieldGet(that, _DomSelector_set, "f").add(e);
                });
            });
        }
        else {
            _getElementList(selection, element).forEach((e) => {
                var _a, _b;
                if (((_a = e.localName) === null || _a === void 0 ? void 0 : _a.indexOf('-')) !== -1 || ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')))
                    _loadRequirements(e);
                __classPrivateFieldGet(that, _DomSelector_set, "f").add(e);
            });
        }
    }
    get set() {
        return __classPrivateFieldGet(this, _DomSelector_set, "f");
    }
    get array() {
        return Array.from(__classPrivateFieldGet(this, _DomSelector_set, "f"));
    }
    set set(set) {
        __classPrivateFieldSet(this, _DomSelector_set, set, "f");
        return;
    }
    get length() {
        return __classPrivateFieldGet(this, _DomSelector_set, "f").size;
    }
    $t(nameSpace) {
        let result = new TbSelector();
        __classPrivateFieldGet(this, _DomSelector_set, "f").forEach((e) => {
            if (e._tb) {
                Object.values(e._tb).forEach((tb) => {
                    result.set.add(tb);
                });
            }
        });
        if (nameSpace) {
            result.filter(tb => tb.nameSpace === nameSpace);
        }
        return result;
    }
    add(selection, target) {
        let that = this;
        $d(selection, target).forEach(element => {
            __classPrivateFieldGet(that, _DomSelector_set, "f").add(element);
        });
        return that;
    }
    addClass(...classes) {
        classes = words(classes);
        this.forEach(element => {
            element.classList.add(...classes);
        });
        return this;
    }
    after(selection, target) {
        let that = this, after = $d(selection, target);
        ;
        if (!after.length)
            return that;
        after
            .first()
            .forEach(element => {
            that.array.reverse().forEach((e) => {
                e.after(element);
            });
        });
        return that;
    }
    append(selection, target) {
        let that = this, addTo = that.array['0'], append = $d(selection, target);
        if (addTo) {
            append.forEach((e) => {
                addTo.append(e);
            });
        }
        return that;
    }
    appendTo(selection, target) {
        let that = this, appendTo = $d(selection, target).array['0'];
        if (appendTo) {
            that.forEach((e) => {
                appendTo.append(e);
            });
        }
        return that;
    }
    attr(first, second) {
        let that = this, attributes = {};
        if (!that.length)
            return that;
        if (!arguments.length) {
            Array.from([...that.set][0].attributes).forEach((attribute) => {
                attributes[attribute.name] = attribute.value;
            });
            return attributes;
        }
        if (typeof first === 'string') {
            if (second === undefined) {
                return [...that.set][0].getAttribute(first);
            }
            if (second === null) {
                [...that.set].forEach((e) => {
                    e.removeAttribute(first);
                });
                return that;
            }
            if (typeof second === 'object') {
                second = JSON.stringify(second);
            }
            [...that.set].forEach((e) => {
                e.setAttribute(first, second);
            });
            return that;
        }
        if (typeof first === 'object') {
            [...that.set].forEach((e) => {
                Object.keys(first).forEach((key) => {
                    $d(e).attr(key, first.key);
                });
            });
            return that;
        }
        return that;
    }
    before(selection, target) {
        let that = this, before = $d(selection, target);
        ;
        if (!before.length)
            return that;
        before
            .first()
            .forEach(element => {
            that.forEach((e) => {
                e.before(element);
            });
        });
        return that;
    }
    children(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            Array.from(e.children).forEach((c) => {
                set.add(c);
            });
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    descendants(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            e.querySelectorAll('*').forEach((h) => {
                set.add(h);
            });
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    empty() {
        return this.filter((e) => false);
    }
    first(selection, rootNode) {
        let that = this, set = new Set(), compare = false;
        ;
        if (selection) {
            compare = $d(selection, rootNode);
        }
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            if (e.parentNode) {
                set.add(e.parentNode.children[0]);
            }
        });
        that = $d([...set]);
        if (selection) {
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    has(element) {
        return __classPrivateFieldGet(this, _DomSelector_set, "f").has(element);
    }
    hasClass(...classes) {
        classes = words(classes);
        return this.every(e => {
            return Array.from(e.classList).every(c => e.contains(c));
        });
    }
    html(html) {
        let that = this;
        if (html) {
            that.forEach((e) => {
                !e.parentNode
                    ? (() => {
                        console.warn('Cannot access the .innerHTML of a  HTMLElement that hasnt been inserted yet', e);
                        console.trace(e, this);
                        console.info('If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;');
                    })()
                    : e.innerHTML = html;
            });
            return that;
        }
        return that.array[0].innerHTML;
    }
    last(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            var _a;
            let siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                set.add(Array.from(siblings).at(-1));
            }
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    load() {
        let that = this;
        if (!that.length) {
            that.add(document.body);
        }
        that.forEach(e => _loadRequirements(e));
        return that;
    }
    next(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            var _a;
            let siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                let arr = Array.from(siblings), pos = arr.indexOf(e) + 1;
                if (pos < arr.length) {
                    set.add(arr.at(pos));
                }
            }
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    normalize() {
        let that = this;
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => { e.normalize(); });
        return that;
    }
    off(eventName, cb) {
        let that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ')
            : [eventName];
        that.array.forEach((e) => {
            eventNames.forEach(function (n) {
                _removeListener(e, n, cb);
            });
        });
        return that;
    }
    on(eventName, cb, capture = false, once = false) {
        let that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ').filter((s) => s !== '')
            : [eventName];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        that.forEach((e) => {
            eventNames.forEach(function (n) {
                _addListener(e, n, wrapper, capture, once);
            });
        });
        return wrapper;
    }
    one(eventName, cb, capture = false) {
        let that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ').filter((s) => s !== '')
            : [eventName];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        that.forEach((e) => {
            eventNames.forEach(function (n) {
                _addListener(e, n, wrapper, capture, true);
            });
        });
        return wrapper;
    }
    parent(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            if (e.parentNode && e.parentNode.tagName !== 'HTML') {
                set.add(e.parentNode);
            }
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    parents(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            let current = e;
            while (current.parentNode && current.parentNode.tagName !== 'HTML') {
                current = current.parentNode;
                set.add(current);
            }
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    prepend(selection, target) {
        let that = this, addTo = that.array['0'], prepend = $d(selection, target);
        if (addTo) {
            prepend.forEach((e) => {
                addTo.prepend(e);
            });
        }
        return that;
    }
    prev(selection, rootNode) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            var _a;
            let siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                let arr = Array.from(siblings), pos = arr.indexOf(e) - 1;
                if (pos > -1) {
                    set.add(arr.at(pos));
                }
            }
        });
        that = $d([...set]);
        if (selection) {
            let compare = $d(selection, rootNode);
            that.filter((e) => compare.has(e));
        }
        return that;
    }
    removeClass(...classes) {
        classes = words(classes);
        this.forEach(element => {
            element.classList.remove(...classes);
        });
        return this;
    }
    text(text) {
        let that = this;
        if (text) {
            that.forEach((e) => {
                !e.parentNode
                    ? (() => {
                        console.warn('Cannot access the .textContent of a  HTMLElement that hasnt been inserted yet', e);
                        console.trace(e, this);
                        console.info('If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;');
                    })()
                    : e.innerText = text;
            });
            return that;
        }
        let t = that.array[0].innerText;
        return t !== null && t !== void 0 ? t : '';
    }
    toggleClass(...classes) {
        classes = words(classes);
        this.forEach(element => {
            element.classList.toggle(...classes);
        });
        return this;
    }
    trigger(eventName, data = {}, bubbles = false, cancelable = false, composed = false) {
        let that = this, ev, options = { bubbles, cancelable, composed }, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ')
            : [eventName];
        __classPrivateFieldGet(that, _DomSelector_set, "f").forEach((e) => {
            eventNames.forEach(function (n) {
                ev = new Event(n, options);
                ev.data = data;
                e.dispatchEvent(ev);
            });
        });
        return that;
    }
    val(p1, p2) {
        if (!this.length)
            return this;
        const that = this;
        let result, handlers = getInputHandlers(that.array[0]);
        if (!p1) {
            result = Object.assign({}, handlers);
        }
        else {
            if (isObject(p1)) {
                Object.assign(handlers, p1);
                result = that;
            }
            else if (typeof p1 === 'string') {
                if (p2 === undefined) {
                    result = handlers[p1];
                }
                else {
                    handlers[p1] = p2;
                    result = that;
                }
            }
        }
        return result;
    }
    values(values) {
        const that = this, holder = {};
        let data = {};
        if (!that.length) {
            console.warn('no dom selection to search inputs in');
            return {};
        }
        let handlers = getInputHandlers(that.array[0]);
        Object.keys(handlers).forEach((key) => {
            data[key] = handlers[key];
        });
        if (Object.keys(data).length === 0) {
            console.warn('no inputs in this dom selection', that.array[0], data);
        }
        if (values) {
            Object
                .keys(values)
                .forEach(key => {
                if (handlers.hasOwnProperty(key)) {
                    console.log('key found', key);
                    handlers[key] = values[key];
                }
            });
        }
        return structuredClone(Object.assign(handlers));
    }
    at(index) {
        return $d(this.array.at(index));
    }
    concat(items) {
        let that = this, arr = items instanceof DomSelector
            ? items.array
            : items;
        arr.forEach((e) => {
            __classPrivateFieldGet(that, _DomSelector_set, "f").add(e);
        });
        return that;
    }
    entries() {
        return this.array.entries();
    }
    every(predicate, thisArg) {
        return this.array.every(predicate, thisArg);
    }
    filter(predicate, thisArg) {
        __classPrivateFieldSet(this, _DomSelector_set, new Set(this.array.filter(predicate, thisArg)), "f");
        return this;
    }
    forEach(predicate, thisArg) {
        this.array.forEach(predicate, thisArg);
        return this;
    }
    includes(item) {
        return this.array.indexOf(item, 0) !== -1;
    }
    indexOf(item, start) {
        return this.array.indexOf(item, start);
    }
    map(predicate, thisArg) {
        return this.array.map(predicate, thisArg);
    }
    pop() {
        return this.array.pop();
    }
    push(...items) {
        items.forEach((item) => {
            __classPrivateFieldGet(this, _DomSelector_set, "f").add(item);
        });
        return this;
    }
    shift() {
        let that = this, element = __classPrivateFieldGet(that, _DomSelector_set, "f").size ? that.array[0] : undefined;
        if (element) {
            __classPrivateFieldGet(that, _DomSelector_set, "f").delete(element);
        }
        return element;
    }
    slice(start, end) {
        return $d(this.array.slice(start, end));
    }
    some(predicate, thisArg) {
        return this.array.some(predicate, thisArg);
    }
    splice(start, deleteCount, ...items) {
        let that = this, arr = that.array;
        arr.splice(start, deleteCount, ...items);
        return $d(arr);
    }
}
_DomSelector_set = new WeakMap();
;
class TbSelector {
    constructor(selection, element = document.body) {
        _TbSelector_set.set(this, new Set());
        if (!selection) {
            return this;
        }
        $d(selection, element).forEach((e) => {
            $t(e).forEach((t) => {
                __classPrivateFieldGet(this, _TbSelector_set, "f").add(t);
            });
        });
    }
    get set() {
        return __classPrivateFieldGet(this, _TbSelector_set, "f");
    }
    get array() {
        return Array.from(__classPrivateFieldGet(this, _TbSelector_set, "f"));
    }
    set set(set) {
        __classPrivateFieldSet(this, _TbSelector_set, set, "f");
        return;
    }
    get length() {
        return __classPrivateFieldGet(this, _TbSelector_set, "f").size;
    }
    set length(value) {
    }
    $d(selection, element = document.body) {
        let result = new DomSelector(), set = new Set(), compare = !!selection || false, compareSelection = new Set();
        if (compare) {
            compareSelection = $d(selection, element).set;
        }
        __classPrivateFieldGet(this, _TbSelector_set, "f").forEach((e) => {
            if (!compare || compareSelection.has(e._tg)) {
                set.add(e._tg);
            }
        });
        result.set = set;
        return result;
    }
    children(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e.children(nameSpace).forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        return that;
    }
    descendants(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e.descendants(nameSpace).forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        return that;
    }
    first(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e
                .parent()
                .children()
                .$d()
                .first()
                .$t()
                .forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        if (nameSpace) {
            that.filter((tb) => tb.nameSpace = nameSpace);
        }
        return that;
    }
    has(element) {
        return __classPrivateFieldGet(this, _TbSelector_set, "f").has(element);
    }
    last(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e
                .parent()
                .children()
                .$d()
                .last()
                .$t()
                .forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        if (nameSpace) {
            that.filter((e) => e.nameSpace = nameSpace);
        }
        return that;
    }
    next(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            let elements = new Set(), element;
            element = e._tg;
            while (element.nextElementSibling !== null) {
                element = element.nextElementSibling;
                elements.add(element);
            }
            $t([...elements])
                .$d()
                .first()
                .$t()
                .forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        if (nameSpace) {
            that.filter((e) => e.nameSpace = nameSpace);
        }
        return that;
    }
    ns(nameSpace) {
        return this.filter((tb) => tb.nameSpace === nameSpace);
    }
    off(name, cb) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter((s) => s !== '')
            : [name];
        that.$d().array.forEach((e) => {
            eventNames.forEach(function (n) {
                _removeListener(e, `[TB]${n}`, cb);
            });
        });
        return that;
    }
    on(name, cb, once = false) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter((s) => s !== '')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        that.forEach((tb) => {
            eventNames.forEach(function (n) {
                _addListener(tb._tg, `[TB]${n}`, wrapper, false, once);
            });
        });
        return wrapper;
    }
    one(name, cb) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter((s) => s !== '')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        that.forEach((tb) => {
            eventNames.forEach(function (n) {
                _addListener(tb._tg, `[TB]${n}`, wrapper, false, true);
            });
        });
        return wrapper;
    }
    parent(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e.parent(nameSpace).forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        return that;
    }
    parents(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            e.parents(nameSpace).forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        return that;
    }
    prev(nameSpace) {
        let that = this, set = new Set();
        __classPrivateFieldGet(that, _TbSelector_set, "f").forEach((e) => {
            let elements = new Set(), element;
            element = e._tg;
            while (element.previousElementSibling !== null) {
                element = element.previousElementSibling;
                elements.add(element);
            }
            $t([...elements])
                .$d()
                .last()
                .$t()
                .forEach((tb) => {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _TbSelector_set, set, "f");
        if (nameSpace) {
            that.filter((e) => e.nameSpace = nameSpace);
        }
        return that;
    }
    trigger(ev, data = {}, bubble = 'l') {
        let that = this;
        that.forEach((tb) => {
            tb.trigger(ev, data, bubble);
        });
        return that;
    }
    at(index) {
        return $t(this.array.at(index));
    }
    concat(items) {
        let that = this;
        items.forEach((tb) => {
            __classPrivateFieldGet(that, _TbSelector_set, "f").add(tb);
        });
        return that;
    }
    entries() {
        return this.array.entries();
    }
    every(predicate, thisArg) {
        return this.array.every(predicate, thisArg);
    }
    filter(predicate, thisArg) {
        return $t(this.array.filter(predicate, thisArg));
    }
    forEach(predicate, thisArg) {
        let that = this;
        that.array.forEach(predicate, thisArg);
        return that;
    }
    includes(item) {
        return this.array.indexOf(item) !== -1;
    }
    indexOf(item, start) {
        return this.array.indexOf(item, start);
    }
    map(predicate, thisArg) {
        return this.array.map(predicate, thisArg);
    }
    pop() {
        let that = this, arr = that.array, result = arr.pop();
        __classPrivateFieldSet(that, _TbSelector_set, new Set(arr), "f");
        return result;
    }
    push(item) {
        let that = this;
        __classPrivateFieldGet(that, _TbSelector_set, "f").add(item);
        return that;
    }
    shift() {
        let that = this, first = this.array.shift();
        __classPrivateFieldGet(that, _TbSelector_set, "f").delete(first);
        return first;
    }
    slice(start, end) {
        return $t(this.array.slice(start, end));
    }
    some(predicate, thisArg) {
        return this.array.some(predicate, thisArg);
    }
    splice(start, deleteCount, ...items) {
        let that = this, arr = that.array, result = arr.splice(start, deleteCount, ...items);
        return $t(result);
    }
}
_TbSelector_set = new WeakMap();
;
class TbEvent extends Event {
    constructor(type, data = {}, bubble = 'l') {
        super(type);
        this.data = data;
        this.bubble = bubble;
    }
}
;
export const debounce = (func, milliseconds) => {
    let timeout;
    return () => {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            func(...arguments);
        }, milliseconds);
    };
};
export const nameSpace = (ns, obj, val = null, orig) => {
    let leftOver = ns.split('.');
    if (!orig) {
        orig = obj;
    }
    let last = leftOver.shift(), lns = leftOver.join('.');
    if (val !== null && last.length && obj[last] === undefined) {
        obj[last] = {};
    }
    let lob = obj[last];
    if (lns.length === 0) {
        if (val !== null) {
            obj[last] = val;
            return orig;
        }
        return obj[last];
    }
    if (isObject(obj[last])) {
        return nameSpace(lns, obj[last], val, orig);
    }
    else {
        if (val) {
            return orig;
        }
        return lob[last];
    }
};
export const parse = function (what, parseThis) {
    var args = Array.from(arguments);
    if (!args.length) {
        console.error('no arguments give to parse');
        return;
    }
    if (args.length === 1) {
        return args[1];
    }
    else if (args.length > 2) {
        while (args.length > 1) {
            args[0] = parse(args[0], args[1]);
            args.splice(1, 1);
        }
        return args[0];
    }
    if (typeof what === 'string') {
        var vars = what.match(/\{[^\{\}]*\}/g);
        if (!!vars) {
            vars
                .forEach(function (pPropname) {
                var propname = pPropname.substr(1, pPropname.length - 2), value = nameSpace(propname, parseThis);
                if (typeof value !== 'undefined') {
                    what = what.replace(pPropname, value);
                }
            });
        }
    }
    else if (isObject(what)) {
        switch (what.constructor) {
            case Object:
                Object
                    .keys(what)
                    .forEach(function (pKey) {
                    if (what.hasOwnProperty(pKey)) {
                        what[pKey] = parse(what[pKey], parseThis);
                    }
                });
                break;
            case Array:
                what
                    .forEach(function (pValue, pKey, original) {
                    original[pKey] = parse(what[pKey], parseThis);
                });
                break;
        }
    }
    return what;
};
export class TB {
    constructor(target, data) {
        let that = this;
        Object.defineProperty(that, '_id', { enumerable: false, writable: false, configurable: false, value: getId() });
        Object.defineProperty(that, '_tg', { get: () => { return target; } });
        Object.defineProperty(that, '_in', { value: data, writable: false, configurable: false });
    }
    children(nameSpace) {
        let that = this, id = getId(), target = that._tg;
        ;
        target.setAttribute('data-tempid', id);
        let selector = $d(target)
            .descendants()
            .$t()
            .filter(e => $t(e).parent().$d().attr('data-tempid') === id);
        target.removeAttribute('data-tempid');
        if (nameSpace) {
            selector.filter((e) => e.nameSpace = nameSpace);
        }
        return selector;
    }
    descendants(nameSpace) {
        let that = this, id = getId(), target = that._tg;
        ;
        target.setAttribute('data-tempid', id);
        let selector = $d(target)
            .descendants()
            .$t();
        target.removeAttribute('data-tempid');
        if (nameSpace) {
            selector.filter((e) => e.nameSpace = nameSpace);
        }
        return selector;
    }
    off(name, cb) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        eventNames.forEach((name) => {
            $d(that._tg).off(`[TB]${name}`, cb);
        });
        return this;
    }
    on(name, cb, once = false) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        eventNames.forEach((n) => {
            _addListener(that._tg, `[TB]${name}`, wrapper, false, once);
        });
        return wrapper;
    }
    one(name, cb) {
        let that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        eventNames.forEach((n) => {
            _addListener(that._tg, `[TB]${name}`, wrapper, false, true);
        });
        return wrapper;
    }
    parent(nameSpace) {
        let that = this, result;
        result = $d(that._tg)
            .parents()
            .filter(e => e.tb().length)
            .first()
            .$t();
        if (nameSpace) {
            result.filter((e) => e.nameSpace = nameSpace);
        }
        return result;
    }
    parents(nameSpace) {
        let that = this, result;
        result = $d(that._tg)
            .parents()
            .filter(e => e.tb().length)
            .$t();
        if (nameSpace) {
            result.filter((e) => e.nameSpace = nameSpace);
        }
        return result;
    }
    shadow(style) {
        let that = this;
        if (!that._sh) {
            that._sh = that._tg.attachShadow({ mode: "open" });
            that._sh.close = () => {
                that._sh = that._tg.attachShadow({ mode: "closed" });
            };
        }
        else {
            console.warn('IGNORED: shadow DOM already created! You may modify this._sh, however...');
        }
        if (style) {
            if (style instanceof HTMLStyleElement) {
                that._sh.appendChild(style);
            }
            else {
                let styleElement = document.createElement('style');
                styleElement.textContent = style.replace(/[\s]/g, ' ').replace(/  /g, ' ');
                that._sh.appendChild(styleElement);
            }
        }
        return that._sh;
    }
    siblings(nameSpace) {
        let that = this, target = that._tg;
        if (nameSpace !== undefined) {
            let i = target._tb[nameSpace];
            return i;
        }
        return target._tb;
    }
    values(name, initial = {}) {
        let that = this;
        this[name] = values(that._sh !== undefined ? that._sh : that._tg, initial, that, name);
    }
    trigger(ev, data = {}, bubble = 'l') {
        let that = this, tbEvent = ev instanceof TbEvent ? ev : new TbEvent(ev, data, bubble);
        ;
        $d(that._tg).trigger(tbEvent.type, data);
        setTimeout(() => {
            if (tbEvent.bubble.indexOf('u') > -1) {
                that
                    .parent()
                    .trigger(new TbEvent(tbEvent.type, tbEvent.data, 'lu'));
            }
            if (tbEvent.bubble.indexOf('d') > -1) {
                that
                    .children()
                    .forEach((tbInstance) => {
                    tbInstance.trigger(new TbEvent(tbEvent.type, tbEvent.data, 'ld'));
                });
            }
        }, 0);
        return that;
    }
}
export const values = (target, values = {}, tb, observable) => {
    var _a;
    let handlers = getInputHandlers(target);
    function updateForm(values) {
        Object
            .keys(values)
            .forEach((key) => {
            if (handlers.hasOwnProperty(key) && values.hasOwnProperty(key)) {
                handlers[key] = values[key];
            }
        });
    }
    if (Object.keys(values).length > 0) {
        updateForm(values);
    }
    if (observable !== undefined) {
        let obs = tb[observable];
        if (!((_a = obs.constructor) === null || _a === void 0 ? void 0 : _a.hasFormHandler)) {
            obs.constructor.hasFormHandler = true;
            obs.observe(updateForm);
        }
        tb[observable].observe(() => { });
        tb[observable].observe(() => { });
        $d('input, textarea', target).on('keyup change', () => { tb[observable] = structuredClone(Object.assign(handlers)); });
        $d('select', target).on('keyup select change', () => { tb[observable] = structuredClone(Object.assign(handlers)); });
    }
    return observable ? tb[observable] : Object.assign({}, handlers);
};
export const load = (fileName) => {
    let promise;
    if (/\.css$/.test(fileName)) {
        promise = new Promise((resolve, reject) => {
            let link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', fileName);
            link.onload = () => {
                loadingScripts.delete(fileName);
                resolve('ccs loaded ' + fileName);
            };
            link.onerror = () => {
                loadingScripts.delete(fileName);
                console.warn('could not load ' + fileName);
                resolve('ccs NOT loaded!');
            };
            $d(document.head).append(link);
        });
    }
    else if (/\.js$/.test(fileName)) {
        promise = new Promise((resolve, reject) => {
            const se = document.createElement('script');
            se.setAttribute('src', fileName);
            se.setAttribute('blocking', 'render');
            se.setAttribute('type', 'module');
            loadingScripts.add(fileName);
            se.onload = () => {
                loadingScripts.delete(fileName);
                resolve('js loaded ' + fileName);
            };
            se.onerror = () => {
                loadingScripts.delete(fileName);
                console.warn('could not load ' + fileName);
                resolve('js NOT loaded!');
            };
            document.head.append(se);
        });
    }
    else {
        promise = fetch(fileName).then((p) => p.text());
    }
    return promise;
};
export const loadAll = (...files) => {
    let promiseArray = [];
    files = words(files);
    files.forEach(fileName => {
        promiseArray.push(load(fileName));
    });
    return Promise.all(promiseArray);
};
export const $d = (selection, element = document) => {
    let set = new Set();
    if (selection instanceof Array) {
        selection.forEach(value => {
            $d(value, element).forEach(n => {
                set.add(n);
            });
        });
        return new DomSelector([...set]);
    }
    else {
        return new DomSelector(selection, element);
    }
};
export const $t = (selection, element = document) => {
    return $d(selection, element).$t();
};
const _convertToObservables = (target, propertyName) => {
    Object.keys(target).forEach((key) => {
        if (key[0] === '_')
            return;
        let val = target[key], Constructor;
        if (!val.prototype) {
            switch (typeof val) {
                case "string":
                    Constructor = String;
                    break;
                case "number":
                    Constructor = Number;
                    break;
                case "boolean":
                    Constructor = Boolean;
                    break;
                case "object":
                    if (val instanceof Array) {
                        Constructor = Array;
                    }
                    else {
                        Constructor = Object;
                    }
                    break;
                default:
                    return;
            }
        }
        let Observable = class extends Constructor {
            constructor(val) {
                super(val);
                this.constructor.callbacks = [];
                if (isObject(val)) {
                    this.constructor.bound = [];
                    Object.getOwnPropertyNames(val).forEach((key) => {
                        this[key] = val[key];
                    });
                }
            }
            observe(f) {
                this.constructor.callbacks.push(f);
                return this;
            }
            bind(target) {
                const that = this, iso = isObject(that.valueOf()), bound = that.constructor.bound;
                if (iso) {
                    function walk(element) {
                        if (!!element['nodeType'] && element.nodeType === 3) {
                            var placeholders = Array.from(element.nodeValue && element.nodeValue.match(/\{[^\{\}]*\}/g) || []);
                            if (placeholders.length > 0) {
                                var f = (function (template) {
                                    return function (values) {
                                        var t, changed = false;
                                        if (placeholders) {
                                            if (iso) {
                                                placeholders.forEach(function (placeholder) {
                                                    if (f.values[placeholder] !== values[placeholder]) {
                                                        f.values[placeholder] = values[placeholder];
                                                        changed = true;
                                                    }
                                                });
                                            }
                                            else {
                                                f.values = values;
                                                changed = true;
                                            }
                                            ;
                                        }
                                        if (changed) {
                                            t = template;
                                            if (iso) {
                                                element.nodeValue = parse(t, f.values);
                                            }
                                            else {
                                                element.nodeValue = t.replace(/\{.*\}/g, f.values);
                                            }
                                            ;
                                        }
                                        return values;
                                    };
                                })(element.nodeValue || '');
                                f.values = iso ? {} : undefined;
                                placeholders = Array.from(placeholders).map((pKey) => pKey.replace(/[\{\}]/g, '').trim());
                                let initial = iso ? {} : undefined;
                                placeholders.forEach(function (pKey) {
                                    if (iso) {
                                        initial[pKey] = "";
                                    }
                                });
                                bound.push(f);
                            }
                            Array.from(element.childNodes)
                                .forEach(function (childNode) {
                                walk(childNode);
                            });
                            that.notify();
                        }
                        if (!!element['nodeType'] && element.nodeType === 1) {
                            Array.from(element.attributes)
                                .forEach(function (attributeNode) {
                                var placeholders = Array.from(attributeNode.value && attributeNode.value.match(/\{[^\{\}]*\}/g) || []);
                                if (placeholders.length > 0) {
                                    var f = (function (template) {
                                        return function (values) {
                                            var t, changed = false;
                                            if (placeholders) {
                                                placeholders.forEach(function (pKey) {
                                                    if (f.values[pKey] !== values[pKey]) {
                                                        f.values[pKey] = values[pKey];
                                                        changed = true;
                                                    }
                                                });
                                            }
                                            if (changed) {
                                                t = template;
                                                element.nodeValue = parse(t, f.values);
                                            }
                                            return values;
                                        };
                                    })(attributeNode.value || '');
                                    f.values = {};
                                    placeholders = Array.from(placeholders).map((pKey) => pKey.replace(/[\{\}]/g, '').trim());
                                    let initial = {};
                                    placeholders.forEach(function (pKey) {
                                        initial[pKey] = "";
                                    });
                                    bound.push(f);
                                }
                            });
                            Array.from(element.childNodes)
                                .forEach(function (childNode) {
                                walk(childNode);
                            });
                            that.notify();
                        }
                        if (!!element['nodeType'] && element.nodeType === 11) {
                            Array.from(element.childNodes)
                                .forEach(function (childNode) {
                                walk(childNode);
                            });
                        }
                    }
                    walk(target);
                    if (bound.length > 0) {
                        that.notify();
                    }
                }
                else {
                    console.warn('IGNORED - cannot bind a non-Object to the dom, IS:', typeof that.valueOf());
                }
                return this;
            }
            notify() {
                let that = this, data = isObject(that.valueOf()) && (that.valueOf() instanceof Array) === false
                    ? structuredClone(Object.assign({}, that))
                    : that.valueOf();
                that.constructor.callbacks.forEach((f) => {
                    f(data);
                });
                if (isObject(that.valueOf())) {
                    that.constructor.bound.forEach((f) => {
                        f(data);
                    });
                }
                return this;
            }
        };
        let o = new Observable(val);
        Object.defineProperty(target, key, {
            enumerable: true,
            get() {
                if (isObject(o.valueOf())) {
                    let check = structuredClone(Object.assign({}, o));
                    setTimeout(debounce(function checkChanges() {
                        let state = structuredClone(Object.assign({}, o));
                        if (!deepEqual(state, check)) {
                            o.notify();
                        }
                    }, 0), 0);
                }
                return o;
            },
            set(val) {
                var _a, _b;
                if (typeof val === typeof o.valueOf()) {
                    let iso = isObject(o.valueOf()) && (o.valueOf() instanceof Array) === false;
                    if (iso) {
                        let old = structuredClone(Object.assign({}, o));
                        val = structuredClone(Object.assign({}, val));
                        let hasFormHandler = (_b = (_a = o.constructor) === null || _a === void 0 ? void 0 : _a.hasFormHandler) !== null && _b !== void 0 ? _b : false, callbacks = o.constructor.callbacks, bound = o.constructor.bound;
                        o = new Observable(structuredClone(Object.assign({}, val)));
                        o.constructor.callbacks = hasFormHandler;
                        o.constructor.callbacks = callbacks;
                        o.constructor.bound = bound;
                        setTimeout(debounce(function checkChanges() {
                            let state = structuredClone(Object.assign({}, o));
                            if (!deepEqual(state, old)) {
                                o.notify();
                            }
                        }, 0), 0);
                    }
                    else {
                        let old = o.valueOf(), callbacks = o.constructor.callbacks;
                        o = new Observable(val);
                        o.constructor.callbacks = callbacks;
                        if (val !== old) {
                            o.notify();
                        }
                    }
                }
                else {
                    console.warn('IGNORED - cannot change observable types, IS:', typeof o.valueOf(), '- CANNOT BECOME:', typeof val);
                }
            }
        });
        target[key] = val;
    });
};
export function createTb(target, definitionClass, data = {}) {
    if (!target) {
        throw new Error('no target HTML element given');
    }
    if (!definitionClass) {
        throw new Error('no definition class given');
    }
    const instance = new definitionClass(target, data);
    _autoAttachListeners(target, instance);
    target._tb = target._tb || {};
    target._tb[definitionClass.name] = instance;
    _convertToObservables(instance);
    let nameSpace = definitionClass.name || target.tagName.toLowerCase().split('-').join('.');
    Object.defineProperty(instance, '_ns', { value: nameSpace, writable: false, configurable: false });
    target.dispatchEvent(new Event('[TB]oneInit'));
    target.dispatchEvent(new Event('[TB]onInit'));
    return instance;
}
const createCustomElement = (tagName, definitionClass, observedAttributes = [], userDefinedBuiltinObject = 'x') => {
    if (customElements.get(tagName) === undefined) {
        let wantUBE = userDefinedBuiltinObject === 'x' ? false : true, extendClass = HTMLElement;
        if (!userDefinedBuiltinObject) {
            if (tagName.indexOf('-') === -1) {
                throw ('createCustomElement: pTagName must be a string containing at least one hyphen: "-" ');
            }
        }
        else {
            let e = document.createElement(userDefinedBuiltinObject);
            if (e instanceof HTMLUnknownElement) {
            }
            else {
                extendClass = window[document.createElement(userDefinedBuiltinObject).constructor.name];
            }
        }
        customElements.define(tagName, class extends extendClass {
            constructor(data) {
                super();
                let that = this;
                createTb(that, definitionClass, data);
            }
            static get observedAttributes() {
                return observedAttributes;
            }
            connectedCallback() {
                let that = this;
                that.dispatchEvent(new Event('[TB]oneConnected'));
                that.dispatchEvent(new Event('[TB]onConnected'));
            }
            disconnectedCallback() {
                let that = this;
                that.dispatchEvent(new Event('[TB]oneDisconnected'));
                that.dispatchEvent(new Event('[TB]onDisconnected'));
            }
            adoptedCallback() {
                let that = this;
                that.dispatchEvent(new Event('[TB]oneAdopted'));
                that.dispatchEvent(new Event('[TB]onAdopted'));
            }
            attributeChangedCallback(name, oldValue, newValue) {
                let that = this, ev, data = { name: name, oldValue: oldValue, newValue: newValue };
                ev = new Event('[TB]oneAttributeChanged');
                ev.data = data;
                that.dispatchEvent(ev);
                ev = new Event('[TB]onAttributeChanged');
                ev.data = data;
                that.dispatchEvent(ev);
            }
        }, wantUBE
            ? { extends: userDefinedBuiltinObject.toLocaleLowerCase() }
            : undefined);
    }
};
const getCustomElement = (tagName) => {
    return customElements.get(tagName);
};
const CEconstructors = {};
const CEinventory = new Set();
export class CE {
    constructor(tagName, definitionClass, observedAttributes = []) {
        if (!CEinventory.has(tagName)) {
            createCustomElement(tagName, definitionClass, observedAttributes);
            CEinventory.add(tagName);
            CEconstructors[definitionClass.name] = getCustomElement(tagName);
        }
        else {
            console.warn('IGNORED: new CE(...) - custom element', tagName, 'is already defined!', [...CEinventory]);
        }
    }
    static get(tagName) {
        if (tagName) {
            let constructor = getCustomElement(tagName);
            return constructor;
        }
        return CEinventory;
    }
}
const UBEconstructors = {};
const UBEinventory = new Set();
export class UBE {
    constructor(tagName, definitionClass, observedAttributes = [], userDefinedBuiltinObject = 'x') {
        if (!UBEinventory.has(tagName)) {
            createCustomElement(tagName, definitionClass, observedAttributes, userDefinedBuiltinObject);
            UBEinventory.add(tagName);
            UBEconstructors[definitionClass.name] = getCustomElement(tagName);
        }
        else {
            console.warn('IGNORED: new UBE(...) - user defined built-in element', userDefinedBuiltinObject, 'is already defined!', [...UBEinventory]);
        }
    }
    static get(tagName) {
        if (tagName) {
            let constructor = getCustomElement(tagName);
            return constructor;
        }
        return UBEinventory;
    }
}
