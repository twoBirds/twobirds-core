export declare type singleSelector = string | ShadowRoot | HTMLElement | HTMLElement[] | HTMLCollection | NodeList | DSelector | TB | TSelector | undefined;
export declare type selection = singleSelector | singleSelector[];
declare class DomSelector {
    #private;
    get set(): Set<HTMLElement>;
    get array(): HTMLElement[];
    set set(set: Set<HTMLElement>);
    get length(): number;
    constructor(selection?: selection, element?: any);
    $t(nameSpace?: string): TSelector;
    add(selection: selection, target?: HTMLElement): DSelector;
    addClass(...classes: string[]): DSelector;
    after(selection: selection, target?: HTMLElement): DSelector;
    append(selection: selection, target?: HTMLElement): DSelector;
    appendTo(selection: selection, target?: HTMLElement): DSelector;
    attr(): LooseObject;
    attr(obj: LooseObject): DSelector;
    attr(name: string): string | null;
    attr(first?: string | LooseObject, second?: string | object | null): DSelector;
    before(selection: selection, target?: HTMLElement): DSelector;
    children(selection?: selection, rootNode?: HTMLElement): DSelector;
    descendants(selection?: selection, rootNode?: HTMLElement): DSelector;
    empty(): DSelector;
    first(selection?: selection, rootNode?: HTMLElement): DSelector;
    has(element: HTMLElement): boolean;
    hasClass(...classes: string[]): boolean;
    html(html?: string): string | DSelector;
    last(selection?: selection, rootNode?: HTMLElement): DSelector;
    load(): DSelector;
    next(selection?: selection, rootNode?: HTMLElement): DSelector;
    normalize(): DSelector;
    off(eventName: string, cb: Function): DSelector;
    on(eventName: string, cb: Function, capture?: boolean, once?: boolean): Function;
    one(eventName: string, cb: Function, capture?: boolean): Function;
    parent(selection?: selection, rootNode?: HTMLElement): DSelector;
    parents(selection?: selection, rootNode?: HTMLElement): DSelector;
    prev(selection?: selection, rootNode?: HTMLElement): DSelector;
    removeClass(...classes: string[]): DSelector;
    text(text?: string): string | DSelector;
    toggleClass(...classes: string[]): DSelector;
    trigger(eventName: string, data?: any, bubbles?: boolean, cancelable?: boolean, composed?: boolean): DSelector;
    val(p1?: any, p2?: any): any;
    values(values?: LooseObject): LooseObject;
    at(index: number): DSelector;
    concat(items: DSelector | any[]): DSelector;
    entries(): IterableIterator<[number, any]>;
    every(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): boolean;
    filter(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): DSelector;
    forEach(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): DSelector;
    includes(item: HTMLElement): boolean;
    indexOf(item: HTMLElement, start?: number): number;
    map(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): Array<any>;
    pop(): HTMLElement;
    push(...items: HTMLElement[]): DSelector;
    shift(): HTMLElement | undefined;
    slice(start: number, end?: number): DSelector;
    some(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): boolean;
    splice(start: number, deleteCount?: number | undefined): DSelector;
}
export interface DSelector extends DomSelector {
}
declare class TbSelector {
    #private;
    get set(): Set<TB>;
    get array(): TB[];
    set set(set: Set<TB>);
    get length(): number;
    set length(value: number);
    constructor(selection?: selection, element?: HTMLElement);
    $d(selection?: selection, element?: HTMLElement): DSelector;
    children(nameSpace?: string): TSelector;
    descendants(nameSpace?: string): TSelector;
    first(nameSpace?: string): TSelector;
    has(element: TB): boolean;
    last(nameSpace?: string): TSelector;
    next(nameSpace?: string): TSelector;
    ns(nameSpace: string): TSelector;
    off(name: string, cb: Function): TSelector;
    on(name: string, cb: Function, once?: boolean): Function;
    one(name: string, cb: Function): Function;
    parent(nameSpace?: string): TSelector;
    parents(nameSpace?: string): TSelector;
    prev(nameSpace?: string): TSelector;
    trigger(ev: string, data?: any, bubble?: string): TSelector;
    at(index: number): TSelector;
    concat(items: TSelector): TSelector;
    entries(): IterableIterator<[number, any]>;
    every(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): boolean;
    filter(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): TSelector;
    forEach(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): TSelector;
    includes(item: TB): boolean;
    indexOf(item: TB, start?: number): number;
    map(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): Array<any>;
    pop(): TB;
    push(item: TB): TSelector;
    shift(): TB | undefined;
    slice(start: number, end?: number): TSelector;
    some(predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any): boolean;
    splice(start: number, deleteCount?: number | undefined): TSelector;
}
export interface TSelector extends TbSelector {
}
export interface LooseObject {
    [key: string | symbol]: any;
}
export interface THTMLElement extends HTMLElement {
    _tb: Object;
}
interface TbEvent extends Event {
    data: any;
    bubble: string;
}
declare class TbEvent extends Event {
    constructor(type: string, data?: any, bubble?: string);
}
export interface TEvent extends TbEvent {
}
export declare const debounce: (func: Function, milliseconds: number) => Function;
export declare const nameSpace: (ns: string, obj: LooseObject, val?: any, orig?: any) => any;
export declare const parse: (what: string | Object | any[], parseThis: Object) => any;
export interface TB {
    _id: string;
    _in: any;
    _ns: string;
    _sh: ShadowRoot;
    _tg: HTMLElement;
}
export declare class TB {
    constructor(target: HTMLElement, data?: any);
    children(nameSpace?: string): TSelector;
    descendants(nameSpace?: string): TSelector;
    off(name: string, cb: Function): this;
    on(name: string, cb: Function, once?: boolean): Function;
    one(name: string, cb: Function): Function;
    parent(nameSpace?: string): TSelector;
    parents(nameSpace?: string): TSelector;
    shadow(style: string | HTMLStyleElement): ShadowRoot;
    siblings(nameSpace?: string): Object | TB | void;
    values(name: string, initial?: Object): void;
    trigger(ev: TEvent | string, data?: any, bubble?: string): TB;
}
export declare const values: (target: HTMLElement | ShadowRoot, values?: Object, tb?: TB, observable?: string) => LooseObject;
export declare const load: (fileName: string) => Promise<any>;
export declare const loadAll: (...files: string[]) => Promise<any>;
export declare const $d: (selection: selection, element?: any) => DSelector;
export declare const $t: (selection?: selection, element?: any) => TSelector;
export declare const createTb: (target: LooseObject, definitionClass: new (...args: any[]) => any, data?: any) => TB;
export declare class CE {
    constructor(tagName: string, definitionClass: {
        new (...args: any[]): any;
    }, observedAttributes?: String[]);
    static get(tagName?: string): Set<any> | {
        new (...args: any[]): any;
    } | void;
}
export declare class UBE {
    constructor(tagName: string, definitionClass: {
        new (...args: any[]): any;
    }, observedAttributes?: String[], userDefinedBuiltinObject?: string);
    static get(tagName?: string): Set<any> | {
        new (...args: any[]): any;
    } | void;
}
export {};
