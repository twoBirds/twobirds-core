const { createTb, TB, CE, $d, loadAll } = tb;
(() => {
    class Dom extends TB {
        constructor(tg, i) {
            super(tg, i);
            this.a = 41;
        }
        oneConnected() {
            let that = this;
            that.form = that.siblings('TestFormForm');
            that.a.observe((v) => {
                that.form.str = `Dom.a = ${v} now!`;
            });
            that.b = 42;
            that.c = 'Hello World';
        }
    }
    class TestFormForm extends TB {
        constructor(tg, i) {
            super(tg, i);
            createTb(tg, Dom);
            Object.assign(this, {
                bool: true,
                num: 1,
                str: 'Hello World',
                form: {},
                form2: {},
            });
        }
        oneConnected(ev) {
            let that = this, req = [
                '/test/form/form.shadowCss',
                '/test/form/form.html',
                '/test/form/form.json',
            ];
            that.dom = that.siblings('Dom');
            that.str.observe((v) => console.log(v));
            that.dom.a = 42;
            loadAll(...req).then((result) => {
                that.render(result);
            });
        }
        render(result) {
            let that = this, css = result[0], html = result[1], data = JSON.parse(result[2]);
            that.shadow(css);
            $d(that._sh).append(html);
            that.values('form');
            that.form.observe((formValues) => {
                let changed = Object.assign(formValues, {
                    textInput: formValues.textInput.toUpperCase(),
                });
                that.form2 = changed;
            });
            that.form2.bind(that._sh);
            that.form = data;
        }
    }
    new CE('test-form-form', TestFormForm);
})();
