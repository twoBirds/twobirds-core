const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGreatgrandchild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected() {
            this.randomBorder();
        }
        randomBorder() {
            let that = this;
            setInterval(() => {
                that._tg.style.borderColor = randomBorderColor();
            }, Math.floor(Math.random() * 10000));
        }
    }
    function random(min, max) {
        var random = Math.floor(Math.random() * (max - min + 1) + min);
        return random;
    }
    function randomBorderColor() {
        return ('rgb(' +
            random(0, 255) +
            ',' +
            random(0, 255) +
            ',' +
            random(0, 255) +
            ')');
    }
    new CE('test-family-greatgrandchild', TestFamilyGreatgrandchild);
})();
