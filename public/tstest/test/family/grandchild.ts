/// <reference types="twobirds-core" />
import * as tb from 'twobirds-core';

const { TB, CE, $d } = tb;

(() => {
	class TestFamilyGrandchild extends TB {
		constructor(target: HTMLElement, config: any) {
			super(target, config);
		}

		oneConnected(ev: any) {
			let fragment = $d('<test-family-greatgrandchild/>'.repeat(2));

			$d(this._tg).append(fragment);
		}
	}

	new CE('test-family-grandchild', TestFamilyGrandchild);
})();
