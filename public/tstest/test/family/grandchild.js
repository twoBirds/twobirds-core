const { TB, CE, $d } = tb;
(() => {
    class TestFamilyGrandchild extends TB {
        constructor(target, config) {
            super(target, config);
        }
        oneConnected(ev) {
            let fragment = $d('<test-family-greatgrandchild/>'.repeat(2));
            $d(this._tg).append(fragment);
        }
    }
    new CE('test-family-grandchild', TestFamilyGrandchild);
})();
