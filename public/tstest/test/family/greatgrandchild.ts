/// <reference types="twobirds-core" />
import * as tb from 'twobirds-core';

const { TB, CE, $d } = tb;

(() => {
	class TestFamilyGreatgrandchild extends TB {
		constructor(target: HTMLElement, config: any) {
			super(target, config);

			// let that = this;

			// $d(target).one( "connected", () => that.connected() );
		}

		oneConnected() {
			this.randomBorder();
		}

		randomBorder() {
			let that = this;

			setInterval(() => {
				that._tg.style.borderColor = randomBorderColor();
			}, Math.floor(Math.random() * 10000));
		}
	}

	function random(min: number, max: number): number {
		var random = Math.floor(Math.random() * (max - min + 1) + min);
		return random;
	}

	function randomBorderColor(): string {
		return (
			'rgb(' +
			random(0, 255) +
			',' +
			random(0, 255) +
			',' +
			random(0, 255) +
			')'
		);
	}

	new CE('test-family-greatgrandchild', TestFamilyGreatgrandchild);
})();
