const { TbClass, createCustomElement } = tb;
(() => {
    class LazyLoad extends TbClass {
        constructor(target, data) {
            super(target, data);
        }
    }
    createCustomElement('lazy-load', LazyLoad);
})();
