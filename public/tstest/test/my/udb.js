const { TbClass, $d, createCustomElement } = tb;
(() => {
    class MyUdb extends TbClass {
        constructor(target, data) {
            super(target, data);
            this.a = 42;
            let t = $d(target);
            t.on('connected', (ev) => {
                ev.stopPropagation();
                $d(target).array[0].innerHTML = 'test button';
            });
            t.on('attributeChanged', (ev) => {
                let d = ev.data;
                if (d.name === 'at' && d.newValue) {
                    $d(target).array[0].innerHTML = d.newValue;
                    $d(target).attr('at', null);
                }
                ev.stopPropagation();
            });
        }
    }
    createCustomElement('my-udb', MyUdb, ['at'], 'button');
})();
