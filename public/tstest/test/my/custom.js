const { TbClass, TbEvent, DomSelector, $d, createCustomElement } = tb;
$d('<lazy-load>');
(() => {
    class MyCustom extends TbClass {
        constructor(target, data) {
            super(target, data);
            let that = this;
            this.a = 42;
            console.log(that);
            $d(target).text('cccc');
            this.off = {};
            let e = $d(target);
            [
                ['connected', (ev) => that.connected(ev)],
                ['attributeChanged', (ev) => that.attributeChanged(ev)]
            ]
                .forEach((ev) => {
                this.off[ev[0]] = e.on(ev[0], ev[1]);
            });
        }
        connected(ev) {
            ev.stopPropagation();
            let that = this, target = $d(that.target);
            target.text('{text}');
            target.append($d('<lazy-load>'));
            that
                .store('text', 'initial')
                .bindDom(that.target);
        }
        attributeChanged(ev) {
            ev.stopPropagation();
            if (ev.data.name === 'text') {
                this.text = ev.data.newValue;
            }
        }
    }
    createCustomElement('my-custom', MyCustom, ['text']);
})();
