const { TbClass, createCustomElement } = tb;
(() => {
    class MyMin extends TbClass {
        constructor(target, data) {
            super(target, data);
        }
    }
    createCustomElement('my-min', MyMin, ['at']);
})();
