***Proposal to extend HTMLElement and add a first-class function to Javascript - MICRO COMPONENTS***

This is a **draft**.

**twoBirds** already uses this technology: in twoBirds apps you can find an **_tb** property in HTMLElement instances. In this document is referred as **_mc**.

The technology is currently forked out into a polyfill project of its own - with the goal of becoming a web standard.

This document is the **RFC** document to be submitted **to the WhatWG** eventually.

## Problem Statement

1. You cannot add anything else than a string to the attributes of a DOM node.

2. You can, however, curry an HTMLElement instance like this:

```javascript
let div = document.querySelectorAll('div')[0];
div.x = 1;  // the div now has a property x that holds the numeric value 1
```

But this violates JS best practices, because you have changed a native elements first class interface.

Also in modular programming, assume two modules that for some reason use that same property key.

You may introduce side effects which are hard to debug.

## Desired Archievement

1. I want to enrich a standard HTMLElement instance with encapsuled and modular javascript functionality, without having to resort to web components or big frameworks.

2. I want to import and use this functionality (**Micro Components**) in any framework that allows for direct DOM access.

3. I want a single location in an HTMLElement instance where all this functionality is stored and sufficently organized.

4. I want to be able to use selectors to find those instances in the DOM.

## Proposal

Introducing the concept of **Micro Components**.

Micro Components can be of any type, e.g. simple values, plain objects, proxy objects or class instances.

Most likely they will be proxy objects or class instances.

For example **Micro Components** can be added to to a DOM element like this:

before:
```HTML
<button></button>
```

code: (assume "Button" is some class in a library)
```javascript
import 'Button' from 'tb/ui';

// select first button element in DOM
let myButton = document.querySelectorAll('button')[0];

// this will add the micro component to myButton._mc using attachMC():
myButton.attachMC( 'tb_ui_button', new Button( myButton, { ...config data } ) );

// the same using the proxy variant described below:
myButton._mc.tb_ui_button = new Button( myButton, { ...config data } );
```

after:
```HTML
<button _mc></button>
```

```javascript
console.log( myButton._mc );
// { tb_ui_button: { target: <button>, onClick: <function> } }
```


delete:
```javascript
delete myButton._mc.tb_ui_button;
```

after:
```HTML
<button></button>
```

```javascript
console.log( myButton?._mc );
// undefined
```


## Use Cases

This provides modular low-level Javascript functionality that works in any UI framework that has native DOM access.

Since the proposed _mc property can hold any number of micro components, you can compose complex functionality from simpler modules.

This is one of the requirements of enterprise programming, where many teams in different projects ideally share specific business or UI logic.


## Detailed Design WIP

### 1. Add a first class function **attachMC( target: HTMLElement, key: string, initialValue: any = null ): void** to Javascript

```javascript
window.attachMC = function( target: HTMLElement, key: string, element: any ):void { // any is intentional here!

	if (!element || this?.mc[key]) return;	// silent failure.
											// Existing MCs should not be overwritten.
											// To do this, explicitly delete the previous MC.

	// create the _mc proxy object if it doesnt exist yet
	this._mc = this._mc || getProxy( element, {

	});

	this._mc[ key ] = initialValue;		// add the given element to the _mc object
	this.setAttribute( '_mc', '' );		// this way you can use querySelector to find elements that contain MCs. This is optional, but may help in locating microcomponents in the DOM.
	return element;						// chained - to be able to directly call the elements interface
};
// Note 1: since functionality is stored in an object, you cannot have 2 microcomponents with the same name in one DOM node.
// Note 2: the _mc property contains the underscore so debuggers display it on top. The visibility in the DOM is intended, MCs are meant to work as an interface also.
```

setAttribute( '_mc', '' ) is optional and makes querySelector result sets smaller when looking for DOM nodes containing MCs. Example:

```javascript
document.querySelectorAll('div[_mc]');
```
The underscore in the attribute name is necessary to avoid interference with existing attributes other frameworks or application code may use.

### 2. Add a method **attachMC( key: string, initialValue: any = null ): void** to the HTMLElement class

```javascript
HTMLElement.prototype.attachMC = function( key: string, element: any ):void { // any is intentional here!
	return attachMC( this, key, element );
};
```

3. Implement this as a proxy with getter/setter functionality to automatically remove the _mc attribute if the hash object is empty after some deletion. Usage:
```javascript
// create it
myDomButton._mc.tb_ui_button = new Button( myDomButton, { ...config data } ) );

// delete it
delete myDomButton._mc.tb_ui_button;
```

The existing polyfill uses all of the variants for best leverage.

**Suggestion:**
The **name** of a **Micro Component** should follow the naming scheme for Custom Element tag names, but replacing "-" with "_".

Using _ allows for **myButton._mc.tb_ui_button** syntax in javascript instead of the clumsy  **myButton._mc["tb-ui-button"]**.

An example for a framework microcomponent could be named "tb_ui_button":
- "tb" is the framework namespace
- "ui" is the name of the name of the micro component library
- "button" is the name of the microcomponent in that library

This can be enforced in the code, if so desired.

## Alternatives Considered
I could not come up with a standard way of doing this using the existing codebase.

Ultimately this is meant to enable javascript frameworks to produce code that can be imported into other frameworks without interference.

You can do this with most frameworks that compile to web component code, but then you need to import all of it.

This approach differs in that you can insert tailor-made modular code for for every need, and you can compose complex element functionality from simpler modules.

Also, you can implement state-driven execution chains using proxy properties in different modules separated by their concerns.

## Backwards Compatibility
Since this leaves all standard functionality untouched, it is **backwards compatible**. The polyfill will work in ES3, even in hbbtv apps.

## Performance and Security Considerations

- There should be **little to no impact** on **browser performance** based on the implementation.

If implemented as attachMC() there is **no performance impact**.

If implemented as a **proxy object property of the HTMLElement**, the time and mem usage for this property should be considered, since it is added to every DOM node whether it is used or not.

IMHO the best alternative is to create the proxy inside the attachMC() function. That way you have the best leverage of low footprint vs. feature richness.

- There is **no impact** on **security**.
This is meant to give an interface to and in between frameworks.

Since **Micro Components** show up in the debugger, they allow for easy debugging.

If this is not wanted for security reasons, you can just avoid MCs for safety relevant functionality.

You can use all existing functionality like Object.freeze() of course, so all of javascripts built in safety functions work.

## Other Considerations
- When this is implemented, for existing JS codebases, you should look up "_mc" and "attachMC" occurences in your code to avoid name clashing.

## Stakeholders
- **Browser Vendors** must implement the functionality as a proxy property and/or attachMC() function.
- **Web Developers** can right away use it any way they want. Even now, using the polyfill.
To my knowledge there are no other stakeholders.

## Last toughts
- I am currently setting up a polyfill project for this.
