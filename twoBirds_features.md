# twoBirds: A JavaScript Framework for Granular, Scalable Development

twoBirds is a JavaScript framework designed for building complex, large-scale web applications by focusing on granular control and modularity through micro-components.
It differs from more conventional frameworks like React, Vue, and Svelte by emphasizing manual wiring of state, observables, and DOM updates, which gives developers fine-grained control over the behavior and scalability of their applications.
It also aims at enforcing SOLID principles by design. twoBirds can be programmed using vanilla JS or Typescript, depending on the desired QA level.

# Basic Ideas

- Absolute precedence of standard methodology where it exists. No abstraction to mask functionality that HTML, CSS or JS can do themselves. Example: no JSX.
- Simplicity: one should easily be able to pick up all tB functionality in a day at most. Experienced programmers can do it in 1-2 hours.
- Interoperability: should be able to mix with any other framework that provides DOM access in its coding.
- Enterprise: aims at making enterprise programming easier, reusable and less error prone.
- Beginners: also aims at being a low level starter kit using vanilla JS.
- DRY
- KISS
- SOLID

# Key Features:

## 1. Micro Component System:
- In twoBirds, functionality is broken down into **micro components**, which are **instances of classes** that can be attached directly to HTML DOM elements.
- **Multiple micro components** can operate on the **same DOM element**, enabling high modularity and separation of concerns with tight or loose coupling as desired.
- Micro components are stored in a property called _tb, which is attached to the DOM element.

## 2. Manual Wiring of Observables:
- Every property defined in a micro-component’s constructor becomes observable.
- When a property changes, any attached callbacks are triggered, enabling a data-driven execution of component methods.
Unlike typical frameworks, twoBirds avoids complex global state management systems like Redux or Context. Instead, developers wire functionalities manually, providing complete control over the flow of data and the timing of state updates.

## 3. Efficient DOM Updates:
- twoBirds features DOM diffing functionality for elements with HTML placeholders in text nodes and attributes.
- You can bind any observable object directly to the DOM.
- This ensures that only the necessary parts of the DOM are updated when data changes, optimizing performance without relying on a virtual DOM.

## 4. Scalable State Management:
twoBirds’ scaling strategy involves both **hard** and **soft wiring** of functionality. It gives developers direct control over data flow, allowing complex applications to scale without introducing unnecessary overhead or dependencies.

## 5. Selector System:
twoBirds features a robust selector system designed to facilitate easy querying and manipulation of micro-components and DOM elements:

- **DSelector**: Used for selecting and manipulating DOM elements. It allows developers to filter, map, and operate on HTML elements easily.
- **TSelector**: Designed for micro-components, enabling developers to interact with the micro-component instances and access their properties and methods.
- **Transformation Methods**: The **d()** method allows a TSelector to retrieve associated DOM elements, while the **t()** method enables a DSelector to get associated micro-components.

These selectors provide flexible and intuitive ways to interact with both the DOM and the application structure.

## 6. No JSX or Hooks:
Instead of JSX or hooks, twoBirds uses plain HTML strings and manually defined observables. DOM lifecycle methods are integrated with twoBirds observables, providing lifecycle control in a way that’s unique to the framework.

## 7. Performance and Flexibility:
While the twoBirds bundle may be larger than Svelte for small applications, it remains smaller than React’s and offers greater flexibility in handling asynchronous data, DOM updates, and component interaction.

## 8. Interoperability:
- twoBirds micro components are plain encapsulated JavaScript modules and can be used in any framework that provides access to the DOM. You can simply import and use them.
- twoBirds web components are native web components and can be used with any framework that supports web components. Internally they use microcomponents as well.
- Both micro components and web components can be easily integrated into plain old websites without the need for a modern framework.

## 9. Ease of Debugging:
Debugging in twoBirds is simplified by its manual wiring system. There are no hidden magic processes (e.g., React's reconciliation), which makes it easier to trace issues in state propagation or component updates without relying on additional debugging tools.

# Use Cases Where twoBirds Excels:

- Large, Complex Applications: twoBirds is ideal for projects where developers need fine-grained control over state management and performance. Its manual control over data flow and DOM updates ensures it scales well without introducing unwanted complexity.

- Modular Development: Its micro-component system allows for highly modular designs, enabling developers to combine and reuse components without tightly coupling them together

-  Highly Interactive UIs: When precise control over UI interactions and DOM updates is critical, twoBirds’ observable-based reactivity and efficient DOM diffing provide a performance edge.
