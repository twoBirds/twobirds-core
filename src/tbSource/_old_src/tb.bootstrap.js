
/*
only used internally
only body tag with data-tb tag for definition
other tags will be defined in app code
*/
tb.assumeTb = (function(pSetter){ 
    let isTb = pSetter;
    return function(pParam){
        //console.log('assumeTb pParam', pParam);
        if ( 
            !!window
            && typeof pParam === 'object'
            && !!pParam.nodeType
            && pParam.nodeType === 1    // html node
            && pParam !== document.head
            && pParam.parentNode !== document.head
            && isTb
        ){
            // scan for AACEs and load + re-insert
            //console.log('scan for ACEs: ', pParam);
            let fileName, 
                lastIndex, 
                nameSpace,
                tagName;

            tb.dom(pParam)
                .add( tb.dom(pParam).descendants() )
                .forEach(function(pElement){
                    
                    let tagName = pElement.tagName.toLowerCase(),
                        isLoading,
                        fileName,
                        hasTbClassCode;

                    if ( tagName.indexOf('-') === -1 || window.customElements.get(tagName) ){
                        return;
                    }

                    //console.log(' undefined tagName', tagName, 'in', pElement.parentNode );

                    fileName = tagName.split('-');
                    lastIndex = fileName.length - 1;

                    // normalize filename ->
                    fileName[lastIndex] = 
                        fileName[lastIndex].substr(0,1).toUpperCase() +
                        fileName[lastIndex].substr(1);
                            
                    nameSpace = fileName.join('.');

                    fileName = '/'+fileName.join('/') + '.js';     

                    //console.log(fileName);

                    isLoading = !!tb.require.get(fileName),
                    hasTbClassCode = !!tb.namespace(nameSpace).get(); // jshint ignore:line

                    // create element definition callback
                    // console.log( tagName, nameSpace, pElement );

                    let define = (function( nameSpace, tagName, element ){ return function define(){

                        if( !window.customElements.get(tagName) ){

                            tb.createCustomElement( 
                                tagName, 
                                tb.namespace( nameSpace, window || process || {} ).get(),
                                Array
                                    .from( element.attributes )
                                    .map( function(pAttribute){ 
                                        return pAttribute.name; 
                                    })
                            );                            

                        }

                    };})( nameSpace, tagName, pElement );

                    if ( !hasTbClassCode ){
                        tb.require( fileName )
                            .then( define );
                    } else {
                        define();
                    }
 
                    tb.assumeTb( false );
                });
 
            return;
        } else if ( typeof pParam === 'boolean' ){
            isTb = pParam;
        }
        return isTb;
    };
})(false); // default: dont assume custom tags to resolve to tB classes


// make it a node module
if (typeof module !== 'undefined') {
    module.exports = tb;
} else {
    /**
     * document.ready bootstrap
     */
    (function(){

        function domReady () {
            // find all tb head & body nodes and add tb objects if not yet done
            tb.attach( document.body );
            tb.assumeTb( document.body );
        }

        // Mozilla, Opera, Webkit
        if ( document.addEventListener ) {
            document.addEventListener( "DOMContentLoaded", function(){
                document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
                setTimeout(domReady, 50);
            }, false );

        } else if ( document.attachEvent ) { // IE
            // ensure firing before onload
            document.attachEvent("onreadystatechange", function(){
                if ( document.readyState === "complete" ) {
                    document.detachEvent( "onreadystatechange", arguments.callee );
                    setTimeout(domReady, 50);
                }
            });
        }

    })();
}
