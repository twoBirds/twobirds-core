"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var _e, _type, _radioBase, _e_1, _multi, _e_2, _set, _set_1;
exports.__esModule = true;
exports.UBE = exports.CE = exports.createTb = exports.$t = exports.$d = exports.loadAll = exports.load = exports.values = exports.TB = exports.parse = exports.nameSpace = exports.debounce = exports.TbEvent = exports.TbSelector = exports.DomSelector = void 0;
/*
internal helpers
*/
function isObject(value) {
    return value != null && typeof value === 'object';
}
var deepEqual = function (o1, o2) {
    var k1 = Object.keys(o1);
    var k2 = Object.keys(o2);
    if (k1.length !== k2.length) {
        return false;
    }
    for (var _i = 0, k1_1 = k1; _i < k1_1.length; _i++) {
        var key = k1_1[_i];
        var v1 = o1[key], v2 = o2[key], recurse = isObject(v1) && isObject(v2);
        ;
        if ((recurse && !deepEqual(v1, v2))
            || (!recurse && v1 !== v2)) {
            return false;
        }
    }
    return true;
};
var _makeLoadData = function (e) {
    var _a, _b;
    var tagName = (_a = e === null || e === void 0 ? void 0 : e.localName) !== null && _a !== void 0 ? _a : '', isAttribute = ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')) || '', isUBE = isAttribute ? true : false, isCE = tagName.indexOf('-') !== -1 ? true : false, ubeSelector = isUBE ? tagName + "[is=\"" + isAttribute + "\"]:defined" : '', ceSelector = isCE ? tagName + ':defined' : '', fileBaseName = isUBE
        ? isAttribute
        : isCE
            ? tagName
            : '', fileName = fileBaseName.length ? '/' + fileBaseName.split('-').join('/') + '.js' : '', selector = isUBE
        ? ubeSelector
        : isCE
            ? ceSelector
            : '';
    //console.log( 'makeLoadData()', tagName, isUBE, isCE, ubeSelector, ceSelector, fileBaseName, ' => ', (isUBE || isCE) ? [ e, selector, fileName ] : false );
    return (isUBE || isCE) ? [e, selector, fileName, fileBaseName] : false;
};
var words = function (classes) {
    var classSet = new Set();
    // read classes into set
    classes.forEach(function (c) {
        c = c.trim();
        c.split(' ').forEach(function (e) {
            if (e !== '') {
                classSet.add(e);
            }
        });
    });
    // now we have a sanitized array
    classes = Array.from(classSet);
    return classes;
};
var getInputName = function (e) {
    var ret = e instanceof HTMLInputElement
        ? 'input'
        : e instanceof HTMLSelectElement
            ? 'select'
            : e instanceof HTMLTextAreaElement
                ? 'textarea'
                : '';
    return ret;
};
// set of currently loading scripts
var loadingScripts = new Set();
var _loadRequirements = function (node) {
    // return Promise.resolve();
    var undefinedSet = new Set(), element = node instanceof HTMLTemplateElement ? node.content : node, head = document.head, allPromises = [], e;
    ;
    if (element instanceof HTMLTemplateElement === false) {
        // if node itself is undefined
        var data = _makeLoadData(element);
        //console.log('data', element, data)
        if (data && !data[0].matches(data[1])) {
            // console.log( '+file:', data );
            undefinedSet.add(data);
        }
    }
    // if some of the childNodes are undefined
    Array.from(element.childNodes).forEach(function (node) {
        var data = _makeLoadData(node);
        //console.log('data', element, data)
        if (data && !data[0].matches(data[1])) {
            // console.log( '+file:', data );
            undefinedSet.add(data);
        }
    });
    if (!!undefinedSet.size) { // there are requirements
        //console.log( 'require set', [ ...undefinedSet ] );
        undefinedSet.forEach(function (data) {
            if (!getCustomElement(data[3]) && !loadingScripts.has(data[2])) {
                loadingScripts.add(data[2]);
                //console.info( '%cloading:', 'color:blue;', data[2], Array.from(loadingScripts) );
                var se = document.createElement('script');
                se.setAttribute('src', data[2]);
                se.setAttribute('blocking', 'render');
                se.setAttribute('type', 'module');
                var s_1 = data[2];
                se.onload = function () {
                    // console.info('loaded', s);
                    loadingScripts["delete"](s_1);
                };
                head.append(se);
                var e_1 = data[0];
                //allPromises.push( customElements.whenDefined( e ) );
            }
            else {
                return;
            }
        });
    }
    return Promise.all(allPromises);
};
var _htmlToElements = function (html) {
    var template = document.createElement('template');
    html = html
        .replace(/<([A-Za-z0-9\-]*)([^>\/]*)(\/>)/gi, "<$1$2></$1>"); // replace XHTML closing tags by full closing tags
    template.innerHTML = html;
    template.content.normalize();
    if (template.content.childNodes.length) {
        _loadRequirements(template);
    }
    //// console.log( '_htmlToElements', isHtml, template.content, ret );
    return Array.from(template.content.childNodes);
};
var _getElementList = function (selector, rootNode) {
    //// console.log('_getElementList(', ...arguments,')');
    var result = new Set();
    ;
    if (!selector) { // no selector given, or a falsy value
        return [];
    }
    else if (typeof selector === 'string') {
        selector = selector
            .replace(/\r/g, '')
            .replace(/\n/g, '')
            .replace(/\t/g, '')
            .replace(/>\s*</g, '><')
            .trim();
        if (!selector.match(/^<.*>$/)) {
            // it is not a HTML string, but a simple string --> it is regarded a CSS selector
            var nodeList = rootNode.querySelectorAll(selector);
            nodeList.forEach(function (node) {
                result.add(node);
            });
            return __spreadArrays(result);
        }
        else { // assume HTML string
            // return html content as a set of nodes
            return _htmlToElements(selector);
        }
    }
    else if (selector instanceof HTMLElement || selector instanceof ShadowRoot) { // selector is a dom node
        result.add(selector);
    }
    else if (selector instanceof DomSelector) { // selector is a $d() result set
        selector.forEach(function (e) {
            result.add(e);
        });
    }
    else if (selector instanceof TbSelector) { // selector is a tb() result set
        selector.forEach(function (e) {
            result.add(e._tg);
        });
    }
    else if (selector instanceof TB) { // selector is a TB instance
        result.add(selector._tg);
    }
    else if (selector instanceof NodeList) {
        selector.forEach(function (n) {
            result.add(n);
        });
    }
    else if (selector instanceof HTMLCollection) {
        Array.from(selector).forEach(function (e) {
            result.add(e);
        });
    }
    else if (selector instanceof Array) {
        selector.forEach(function (e) {
            result.add(e);
        });
    }
    //// console.log( 'result', [ ...result ] );
    return __spreadArrays(result);
};
var _addListener = function (domNode, eventName, handler, capture, once) {
    if (capture === void 0) { capture = false; }
    if (once === void 0) { once = false; }
    var options = {};
    if (capture) {
        options.capture = capture;
    }
    if (once) {
        options.once = once;
    }
    domNode.addEventListener(eventName, handler, options);
};
var _removeListener = function (domNode, eventName, handler) {
    domNode.removeEventListener(eventName, handler);
};
var nativeEventNames = Object.keys(HTMLElement.prototype).filter(function (key) { return /^on/.test(key); });
var _autoAttachListeners = function (target, instance) {
    //console.log( 'autoAttachListeners', target, instance );
    Object
        .getOwnPropertyNames(instance.__proto__)
        .filter(function (key) { return /^on[A-Z]|one[A-Z]/.test(key); })
        .forEach(function (key) {
        var once = /^one/.test(key), withoutOnOne = key.replace(/^on[e]{0,1}/, ''), nativeEventName = 'on' + withoutOnOne.toLowerCase(), listener = Object.getPrototypeOf(instance)[key].bind(instance), isNative = nativeEventNames.indexOf(nativeEventName) > -1;
        //console.log( '_addListener', isNative, withoutOnOne.toLowerCase(), 'or', '[TB]' + key );
        if (isNative) { // native event present
            //console.log( '_addListener native', target, withoutOnOne.toLowerCase(), listener, false, once );
            _addListener(target, withoutOnOne.toLowerCase(), listener, false, once);
        }
        else { // an event handler, but not native -> assumed to be a twobirds event
            //console.log( '_addListener [TB]', target, '[TB]' + key, listener, true, once );
            _addListener(target, '[TB]' + key, listener, true, once);
        }
    });
};
var getId = function () {
    var i;
    if (i = crypto === null || crypto === void 0 ? void 0 : crypto.randomUUID()) {
        return i;
    }
    return ('id-' +
        new Date().getTime() +
        '-' +
        Math.random().toString().replace(/\./, ''));
};
var InputBase = /** @class */ (function () {
    function InputBase(e) {
    }
    return InputBase;
}());
var getInputHandlers = function (e) {
    var 
    //inputList = ( Array.from( e.querySelectorAll('*') ) as HTMLElement[] ).filter( i => isInputElement(i) ),
    inputList = exports.$d('input,select,textarea', e).array, handlers = {}, h;
    inputList.forEach(function (i) {
        var name = i.getAttribute('name') || e.tagName;
        if (name) {
            var h_1;
            switch (getInputName(i)) {
                case 'input':
                    //console.log( i, 'is HTMLInputElement');
                    h_1 = new InputHandler(i, e); // e = element to search radios in = radioBase
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: function () { return h_1.val; },
                        set: function (value) { h_1.val = value; }
                    });
                    break;
                case 'select':
                    //console.log( i, 'is HTMLSelectElement');
                    h_1 = new SelectHandler(i);
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: function () { return h_1.val; },
                        set: function (value) { h_1.val = value; }
                    });
                    break;
                case 'textarea':
                    //console.log( i, 'is HTMLTextAreaElement');
                    h_1 = new TextAreaHandler(i);
                    Object.defineProperty(handlers, name, {
                        enumerable: true,
                        configurable: true,
                        get: function () { return h_1.val; },
                        set: function (value) { h_1.val = value; }
                    });
                    break;
                default:
                    console.warn('handler missing:', name, 'for', i);
                    break;
            }
        }
    });
    return handlers;
};
var InputHandler = /** @class */ (function (_super) {
    __extends(InputHandler, _super);
    function InputHandler(e, radioBase) {
        var _this = _super.call(this, e) || this;
        _e.set(_this, void 0);
        _type.set(_this, void 0);
        _radioBase.set(_this, void 0);
        __classPrivateFieldSet(_this, _e, e);
        __classPrivateFieldSet(_this, _type, e.type);
        __classPrivateFieldSet(_this, _radioBase, radioBase);
        return _this;
    }
    Object.defineProperty(InputHandler.prototype, "val", {
        get: function () {
            var that = this, type = __classPrivateFieldGet(that, _type), ret;
            switch (type) {
                case 'radio':
                    var check = exports.$d("input[type=\"radio\"][name=\"" + __classPrivateFieldGet(this, _e).getAttribute('name') + "\"]", __classPrivateFieldGet(this, _radioBase))
                        .filter(function (e) { return e.checked; });
                    if (!check.length)
                        return '';
                    ret = check.array[0];
                    return ret.value;
                case 'checkbox':
                    return __classPrivateFieldGet(that, _e).checked;
                default:
                    return __classPrivateFieldGet(that, _e).value;
                    break;
            }
            return '';
        },
        set: function (value) {
            var that = this, type = __classPrivateFieldGet(that, _type), ret;
            switch (type) {
                case 'radio':
                    var selection = "input[type=\"radio\"][name=\"" + __classPrivateFieldGet(this, _e).getAttribute('name') + "\"]", radios = exports.$d(selection, __classPrivateFieldGet(this, _radioBase));
                    // console.log('set radios', selection, this.#radioBase, radios.array );
                    if (!radios.length)
                        return;
                    if (value === '') { // uncheck all
                        radios.forEach(function (r) { if (r.checked)
                            r.checked = false; });
                        //console.log('unset all radios', radios.array );
                        return;
                    }
                    var setRadioElement = radios.filter(function (r) { return r.getAttribute('value') === value; });
                    if (!setRadioElement.length)
                        return;
                    // console.log('set this radio', setRadioElement.array );
                    setRadioElement.array[0].checked = true;
                    // console.log('done', setRadioElement.array );
                    return;
                    break;
                case 'checkbox':
                    __classPrivateFieldGet(that, _e).checked = value;
                    break;
                default:
                    __classPrivateFieldGet(that, _e).value = value;
                    break;
            }
            return;
        },
        enumerable: false,
        configurable: true
    });
    return InputHandler;
}(InputBase));
_e = new WeakMap(), _type = new WeakMap(), _radioBase = new WeakMap();
var SelectHandler = /** @class */ (function (_super) {
    __extends(SelectHandler, _super);
    function SelectHandler(e) {
        var _this = _super.call(this, e) || this;
        _e_1.set(_this, void 0);
        _multi.set(_this, false);
        __classPrivateFieldSet(_this, _e_1, e);
        __classPrivateFieldSet(_this, _multi, e.type === 'select-multiple');
        return _this;
    }
    Object.defineProperty(SelectHandler.prototype, "val", {
        get: function () {
            var that = this, ret = [];
            // get all options
            var options = exports.$d(__classPrivateFieldGet(that, _e_1).querySelectorAll('option'))
                .filter(function (o) { return o.selected; })
                .forEach(function (o) { if (o.selected)
                ret.push(o.value); });
            if (__classPrivateFieldGet(this, _multi))
                return ret;
            return ret.length ? ret[0] : '';
        },
        set: function (value) {
            var that = this;
            value = typeof value === 'string' ? [value] : value;
            //console.log('set values', value );
            // set all options
            var options = exports.$d(__classPrivateFieldGet(that, _e_1).querySelectorAll('option'))
                .forEach(function (o) {
                if (value.indexOf(o.value) > -1) {
                    o.setAttribute('selected', true);
                    o.selected = true;
                }
                else {
                    o.removeAttribute('selected');
                    o.selected = false;
                }
            });
            /*
            if (pOption.selected) {
                if (
                    !pOption.disabled &&
                    (!pOption.parentNode.disabled ||
                        pOption.parentNode.nodeName !==
                            'optgroup')
                ) {
                    var value = pOption.value;
    
                    ret.push(value);
                }
            }
            */
            return;
        },
        enumerable: false,
        configurable: true
    });
    return SelectHandler;
}(InputBase));
_e_1 = new WeakMap(), _multi = new WeakMap();
var TextAreaHandler = /** @class */ (function (_super) {
    __extends(TextAreaHandler, _super);
    function TextAreaHandler(e) {
        var _this = _super.call(this, e) || this;
        _e_2.set(_this, void 0);
        __classPrivateFieldSet(_this, _e_2, e);
        return _this;
    }
    Object.defineProperty(TextAreaHandler.prototype, "val", {
        get: function () {
            return __classPrivateFieldGet(this, _e_2).value;
        },
        set: function (value) {
            __classPrivateFieldGet(this, _e_2).value = value;
            return;
        },
        enumerable: false,
        configurable: true
    });
    return TextAreaHandler;
}(InputBase));
_e_2 = new WeakMap();
/*
selector classes
*/
var DomSelector = /** @class */ (function () {
    function DomSelector(selection, element) {
        if (element === void 0) { element = document.body; }
        _set.set(this, new Set());
        var that = this, undefinedTags = new Set();
        // // console.log('DomSelector constructor', ...arguments);
        if (!selection) {
            return that;
        }
        if (selection instanceof Array) {
            selection.forEach(function (select) {
                _getElementList(select, element).forEach(function (e) {
                    var _a, _b;
                    if (((_a = e.localName) === null || _a === void 0 ? void 0 : _a.indexOf('-')) !== -1 || ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')))
                        _loadRequirements(e);
                    __classPrivateFieldGet(that, _set).add(e);
                });
            });
        }
        else {
            _getElementList(selection, element).forEach(function (e) {
                var _a, _b;
                if (((_a = e.localName) === null || _a === void 0 ? void 0 : _a.indexOf('-')) !== -1 || ((_b = e.getAttribute) === null || _b === void 0 ? void 0 : _b.call(e, 'is')))
                    _loadRequirements(e);
                __classPrivateFieldGet(that, _set).add(e);
            });
        }
    }
    Object.defineProperty(DomSelector.prototype, "set", {
        get: function () {
            return __classPrivateFieldGet(this, _set);
        },
        set: function (set) {
            __classPrivateFieldSet(this, _set, set);
            return;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DomSelector.prototype, "array", {
        get: function () {
            return Array.from(__classPrivateFieldGet(this, _set));
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(DomSelector.prototype, "length", {
        get: function () {
            return __classPrivateFieldGet(this, _set).size;
        },
        enumerable: false,
        configurable: true
    });
    DomSelector.prototype.$t = function (nameSpace) {
        var result = new TbSelector();
        __classPrivateFieldGet(this, _set).forEach(function (e) {
            if (e._tb) {
                Object.values(e._tb).forEach(function (tb) {
                    result.set.add(tb);
                });
            }
        });
        if (nameSpace) {
            result.filter(function (tb) { return tb.nameSpace === nameSpace; });
        }
        return result;
    };
    DomSelector.prototype.add = function (selection, target) {
        var that = this;
        exports.$d(selection, target).forEach(function (element) {
            __classPrivateFieldGet(that, _set).add(element);
        });
        return that;
    };
    DomSelector.prototype.addClass = function () {
        var classes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            classes[_i] = arguments[_i];
        }
        // sanitize classes
        classes = words(classes);
        // now set those classes
        this.forEach(function (element) {
            var _a;
            (_a = element.classList).add.apply(_a, classes);
        });
        return this;
    };
    DomSelector.prototype.after = function (selection, target) {
        var that = this, after = exports.$d(selection, target);
        ;
        if (!after.length)
            return that;
        after
            .first()
            .forEach(function (element) {
            that.array.reverse().forEach(function (e) {
                e.after(element);
            });
        });
        return that;
    };
    DomSelector.prototype.append = function (selection, target) {
        var that = this, addTo = that.array['0'], append = exports.$d(selection, target);
        if (addTo) {
            append.forEach(function (e) {
                addTo.append(e);
            });
        }
        return that;
    };
    DomSelector.prototype.appendTo = function (selection, target) {
        var that = this, appendTo = exports.$d(selection, target).array['0'];
        if (appendTo) {
            that.forEach(function (e) {
                appendTo.append(e);
            });
        }
        return that;
    };
    DomSelector.prototype.attr = function (first, second) {
        var that = this, attributes = {};
        // if no elements in set -> skip
        if (!that.length)
            return that;
        // if no arguments, return attributes of first in list as hash object
        if (!arguments.length) {
            Array.from(__spreadArrays(that.set)[0].attributes).forEach(function (attribute) {
                attributes[attribute.name] = attribute.value;
            });
            return attributes;
        }
        // if first is a string
        //   if no value (second) is given -> return attribute value of first element in node selection
        //   if value (second) is null -> remove attribute and return this
        //   else set attribute values and return this
        if (typeof first === 'string') {
            // return attributes
            if (second === undefined) {
                return __spreadArrays(that.set)[0].getAttribute(first);
            }
            // remove attribute
            if (second === null) {
                __spreadArrays(that.set).forEach(function (e) {
                    e.removeAttribute(first);
                });
                return that;
            }
            // if attribute value is an object -> convert to json string
            if (typeof second === 'object') {
                second = JSON.stringify(second);
            }
            // set attribute
            __spreadArrays(that.set).forEach(function (e) {
                e.setAttribute(first, second);
            });
            return that;
        }
        // if first is an object set attributes to all nodes
        if (typeof first === 'object') {
            __spreadArrays(that.set).forEach(function (e) {
                Object.keys(first).forEach(function (key) {
                    exports.$d(e).attr(key, first.key);
                });
            });
            return that;
        }
        return that;
    };
    DomSelector.prototype.before = function (selection, target) {
        var that = this, before = exports.$d(selection, target);
        ;
        if (!before.length)
            return that;
        before
            .first()
            .forEach(function (element) {
            that.forEach(function (e) {
                e.before(element);
            });
        });
        return that;
    };
    DomSelector.prototype.children = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            Array.from(e.children).forEach(function (c) {
                set.add(c);
            });
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_1 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_1.has(e); });
        }
        return that;
    };
    DomSelector.prototype.descendants = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            e.querySelectorAll('*').forEach(function (h) {
                set.add(h);
            });
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_2 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_2.has(e); });
        }
        return that;
    };
    DomSelector.prototype.empty = function () {
        return this.filter(function (e) { return false; });
    };
    DomSelector.prototype.first = function (selection, rootNode) {
        var that = this, set = new Set(), compare = false;
        ;
        // get compare selection if specified
        if (selection) {
            compare = exports.$d(selection, rootNode);
        }
        // get compare selection if specified
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            if (e.parentNode) {
                set.add(e.parentNode.children[0]);
            }
        });
        // make result
        that = exports.$d(__spreadArrays(set));
        // compare if necessay
        if (selection) {
            that.filter(function (e) { return compare.has(e); });
        }
        return that;
    };
    DomSelector.prototype.has = function (element) {
        return __classPrivateFieldGet(this, _set).has(element);
    };
    DomSelector.prototype.hasClass = function () {
        var classes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            classes[_i] = arguments[_i];
        }
        // sanitize classes
        classes = words(classes);
        // now test those classes
        return this.every(function (e) {
            return Array.from(e.classList).every(function (c) { return e.contains(c); });
        });
    };
    DomSelector.prototype.html = function (html) {
        var _this = this;
        var that = this;
        if (html) {
            that.forEach(function (e) {
                !e.parentNode
                    ? (function () {
                        console.warn('Cannot access the .innerHTML of a  HTMLElement that hasnt been inserted yet', e);
                        console.trace(e, _this);
                        console.info('If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;');
                    })()
                    : e.innerHTML = html;
            });
            return that;
        }
        return that.array[0].innerHTML;
    };
    DomSelector.prototype.last = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            var _a;
            var siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                set.add(Array.from(siblings).at(-1));
            }
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_3 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_3.has(e); });
        }
        return that;
    };
    DomSelector.prototype.load = function () {
        var that = this;
        if (!that.length) {
            that.add(document.body);
        }
        that.forEach(function (e) { return _loadRequirements(e); });
        return that;
    };
    DomSelector.prototype.next = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            var _a;
            var siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                var arr = Array.from(siblings), pos = arr.indexOf(e) + 1;
                if (pos < arr.length) {
                    set.add(arr.at(pos));
                }
            }
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_4 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_4.has(e); });
        }
        return that;
    };
    DomSelector.prototype.normalize = function () {
        var that = this;
        __classPrivateFieldGet(that, _set).forEach(function (e) { e.normalize(); });
        return that;
    };
    DomSelector.prototype.off = function (eventName, cb) {
        var that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ')
            : [eventName];
        // remove handler
        that.array.forEach(function (e) {
            eventNames.forEach(function (n) {
                _removeListener(e, n, cb);
            });
        });
        return that;
    };
    DomSelector.prototype.on = function (eventName, cb, capture, once) {
        if (capture === void 0) { capture = false; }
        if (once === void 0) { once = false; }
        var that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ').filter(function (s) { return s !== ''; })
            : [eventName];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        // attach handler
        that.forEach(function (e) {
            eventNames.forEach(function (n) {
                _addListener(e, n, wrapper, capture, once);
            });
        });
        return wrapper;
    };
    DomSelector.prototype.one = function (eventName, cb, capture) {
        if (capture === void 0) { capture = false; }
        var that = this, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ').filter(function (s) { return s !== ''; })
            : [eventName];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        // attach handler
        that.forEach(function (e) {
            eventNames.forEach(function (n) {
                _addListener(e, n, wrapper, capture, true);
            });
        });
        return wrapper;
    };
    DomSelector.prototype.parent = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            if (e.parentNode && e.parentNode.tagName !== 'HTML') {
                set.add(e.parentNode);
            }
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_5 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_5.has(e); });
        }
        return that;
    };
    DomSelector.prototype.parents = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            var current = e;
            while (current.parentNode && current.parentNode.tagName !== 'HTML') {
                current = current.parentNode;
                set.add(current);
            }
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_6 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_6.has(e); });
        }
        return that;
    };
    DomSelector.prototype.prev = function (selection, rootNode) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            var _a;
            var siblings = (_a = e.parentNode) === null || _a === void 0 ? void 0 : _a.children;
            if (siblings) {
                var arr = Array.from(siblings), pos = arr.indexOf(e) - 1;
                if (pos > -1) {
                    set.add(arr.at(pos));
                }
            }
        });
        that = exports.$d(__spreadArrays(set));
        if (selection) {
            var compare_7 = exports.$d(selection, rootNode);
            that.filter(function (e) { return compare_7.has(e); });
        }
        return that;
    };
    DomSelector.prototype.removeClass = function () {
        var classes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            classes[_i] = arguments[_i];
        }
        // sanitize classes
        classes = words(classes);
        // now set those classes
        this.forEach(function (element) {
            var _a;
            (_a = element.classList).remove.apply(_a, classes);
        });
        return this;
    };
    DomSelector.prototype.text = function (text) {
        var _this = this;
        var that = this;
        if (text) {
            that.forEach(function (e) {
                !e.parentNode
                    ? (function () {
                        console.warn('Cannot access the .textContent of a  HTMLElement that hasnt been inserted yet', e);
                        console.trace(e, _this);
                        console.info('If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;');
                    })()
                    : e.innerText = text;
            });
            return that;
        }
        var t = that.array[0].innerText;
        return t !== null && t !== void 0 ? t : '';
    };
    DomSelector.prototype.toggleClass = function () {
        var classes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            classes[_i] = arguments[_i];
        }
        // sanitize classes
        classes = words(classes);
        // now set those classes
        this.forEach(function (element) {
            var _a;
            (_a = element.classList).toggle.apply(_a, classes);
        });
        return this;
    };
    DomSelector.prototype.trigger = function (eventName, data, bubbles, cancelable, composed) {
        if (data === void 0) { data = {}; }
        if (bubbles === void 0) { bubbles = false; }
        if (cancelable === void 0) { cancelable = false; }
        if (composed === void 0) { composed = false; }
        var that = this, ev, options = { bubbles: bubbles, cancelable: cancelable, composed: composed }, eventNames = eventName.indexOf(' ') > -1
            ? eventName.split(' ')
            : [eventName];
        __classPrivateFieldGet(that, _set).forEach(function (e) {
            eventNames.forEach(function (n) {
                // console.log()
                ev = new Event(n, options);
                ev.data = data;
                e.dispatchEvent(ev);
            });
        });
        return that;
    };
    // INFO:
    // val(): LooseObject; // no param: get all as LooseObject
    // val( p1: string ): Array<string> | number | string; // get from input name
    // val( p1: object ): DomSelector; // set from object
    // val( p1: string, p2: any ): DomSelector; // set from name and value
    DomSelector.prototype.val = function (p1, p2) {
        if (!this.length)
            return this;
        var that = this;
        var result, handlers = getInputHandlers(that.array[0]);
        // work with it
        if (!p1) { // get all inputs as LooseObject
            result = Object.assign({}, handlers);
            // Object.keys( handlers ).forEach( ( key ) => result[key] = handlers.hasOwnProperty(key) ? handlers[key].val : undefined );
        }
        else {
            if (isObject(p1)) { // set inputs from Object
                Object.assign(handlers, p1);
                // Object.keys( p1 ).forEach( ( key ) => !!handlers[key] ? handlers[key].val = p1[key] : false );
                result = that;
            }
            else if (typeof p1 === 'string') {
                if (p2 === undefined) { // get a single input
                    result = handlers[p1];
                }
                else { // set a single input
                    handlers[p1] = p2;
                    result = that;
                }
            }
        }
        return result;
    };
    DomSelector.prototype.values = function (values) {
        var that = this, holder = {};
        var data = {};
        //console.log( '$d().values()', values );
        if (!that.length) {
            console.warn('no dom selection to search inputs in');
            return {};
        }
        var handlers = getInputHandlers(that.array[0]);
        //console.log( 'handlers', Object.keys(handlers), handlers['cb1'] );
        // let values = that.val();
        // console.log( 'values', values );
        Object.keys(handlers).forEach(function (key) {
            //console.log(key);
            data[key] = handlers[key];
        });
        // data = Object.assign( {}, that.val() )
        //console.log( 'data', that.array[0], data, Object.keys( data ) );
        if (Object.keys(data).length === 0) {
            console.warn('no inputs in this dom selection', that.array[0], data);
        }
        //console.log( '$d val()', data );
        if (values) {
            Object
                .keys(values)
                .forEach(function (key) {
                if (handlers.hasOwnProperty(key)) {
                    console.log('key found', key);
                    handlers[key] = values[key];
                }
            });
            // .forEach( key => handlers[key] = values[key] )
        }
        return structuredClone(Object.assign(handlers));
    };
    /*
    array method mapping
    */
    DomSelector.prototype.at = function (index) {
        return exports.$d(this.array.at(index));
    };
    DomSelector.prototype.concat = function (items) {
        var that = this, arr = items instanceof DomSelector
            ? items.array
            : items;
        arr.forEach(function (e) {
            __classPrivateFieldGet(that, _set).add(e);
        });
        return that;
    };
    DomSelector.prototype.entries = function () {
        return this.array.entries();
    };
    DomSelector.prototype.every = function (predicate, thisArg) {
        return this.array.every(predicate, thisArg);
    };
    DomSelector.prototype.filter = function (predicate, thisArg) {
        __classPrivateFieldSet(this, _set, new Set(this.array.filter(predicate, thisArg)));
        return this;
    };
    DomSelector.prototype.forEach = function (predicate, thisArg) {
        this.array.forEach(predicate, thisArg);
        return this;
    };
    DomSelector.prototype.includes = function (item) {
        return this.array.indexOf(item, 0) !== -1;
    };
    DomSelector.prototype.indexOf = function (item, start) {
        return this.array.indexOf(item, start);
    };
    DomSelector.prototype.map = function (predicate, thisArg) {
        return this.array.map(predicate, thisArg);
    };
    DomSelector.prototype.pop = function () {
        return this.array.pop();
    };
    DomSelector.prototype.push = function () {
        var _this = this;
        var items = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            items[_i] = arguments[_i];
        }
        items.forEach(function (item) {
            __classPrivateFieldGet(_this, _set).add(item);
        });
        return this;
    };
    DomSelector.prototype.shift = function () {
        var that = this, element = __classPrivateFieldGet(that, _set).size ? that.array[0] : undefined;
        if (element) {
            __classPrivateFieldGet(that, _set)["delete"](element);
        }
        return element;
    };
    DomSelector.prototype.slice = function (start, end) {
        return exports.$d(this.array.slice(start, end));
    };
    DomSelector.prototype.some = function (predicate, thisArg) {
        return this.array.some(predicate, thisArg);
    };
    DomSelector.prototype.splice = function (start, deleteCount) {
        var items = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            items[_i - 2] = arguments[_i];
        }
        var that = this, arr = that.array;
        arr.splice.apply(arr, __spreadArrays([start, deleteCount], items));
        return exports.$d(arr);
    };
    return DomSelector;
}());
exports.DomSelector = DomSelector;
_set = new WeakMap();
var TbSelector = /** @class */ (function () {
    function TbSelector(selection, element) {
        var _this = this;
        if (element === void 0) { element = document.body; }
        _set_1.set(this, new Set());
        if (!selection) {
            return this;
        }
        exports.$d(selection, element).forEach(function (e) {
            exports.$t(e).forEach(function (t) {
                __classPrivateFieldGet(_this, _set_1).add(t);
            });
        });
    }
    Object.defineProperty(TbSelector.prototype, "set", {
        get: function () {
            return __classPrivateFieldGet(this, _set_1);
        },
        set: function (set) {
            __classPrivateFieldSet(this, _set_1, set);
            return;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TbSelector.prototype, "array", {
        get: function () {
            return Array.from(__classPrivateFieldGet(this, _set_1));
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(TbSelector.prototype, "length", {
        get: function () {
            return __classPrivateFieldGet(this, _set_1).size;
        },
        set: function (value) {
            //ignore
        },
        enumerable: false,
        configurable: true
    });
    TbSelector.prototype.$d = function (selection, element) {
        if (element === void 0) { element = document.body; }
        var result = new DomSelector(), set = new Set(), compare = !!selection || false, compareSelection = new Set();
        if (compare) {
            compareSelection = exports.$d(selection, element).set;
        }
        __classPrivateFieldGet(this, _set_1).forEach(function (e) {
            if (!compare || compareSelection.has(e._tg)) {
                set.add(e._tg);
            }
        });
        result.set = set;
        return result;
    };
    TbSelector.prototype.children = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e.children(nameSpace).forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        return that;
    };
    TbSelector.prototype.descendants = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e.descendants(nameSpace).forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        return that;
    };
    TbSelector.prototype.first = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e
                .parent()
                .children()
                .$d()
                .first()
                .$t()
                .forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        if (nameSpace) {
            that.filter(function (tb) { return tb.nameSpace = nameSpace; });
        }
        return that;
    };
    TbSelector.prototype.has = function (element) {
        return __classPrivateFieldGet(this, _set_1).has(element);
    };
    TbSelector.prototype.last = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e
                .parent()
                .children()
                .$d()
                .last()
                .$t()
                .forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        if (nameSpace) {
            that.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return that;
    };
    TbSelector.prototype.next = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            var elements = new Set(), element;
            element = e._tg;
            while (element.nextElementSibling !== null) {
                element = element.nextElementSibling;
                elements.add(element);
            }
            exports.$t(__spreadArrays(elements))
                .$d()
                .first()
                .$t()
                .forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        if (nameSpace) {
            that.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return that;
    };
    TbSelector.prototype.ns = function (nameSpace) {
        return this.filter(function (tb) { return tb.nameSpace === nameSpace; });
    };
    TbSelector.prototype.off = function (name, cb) {
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter(function (s) { return s !== ''; })
            : [name];
        // remove handlers
        that.$d().array.forEach(function (e) {
            eventNames.forEach(function (n) {
                // console.log('Tb Selector remove event', `[TB]${n}`, e, cb );
                _removeListener(e, "[TB]" + n, cb);
            });
        });
        return that;
    };
    TbSelector.prototype.on = function (name, cb, once) {
        if (once === void 0) { once = false; }
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter(function (s) { return s !== ''; })
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        // attach handler
        that.forEach(function (tb) {
            eventNames.forEach(function (n) {
                _addListener(tb._tg, "[TB]" + n, wrapper, false, once);
            });
        });
        return wrapper;
    };
    TbSelector.prototype.one = function (name, cb) {
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ').filter(function (s) { return s !== ''; })
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        // attach handler
        that.forEach(function (tb) {
            eventNames.forEach(function (n) {
                _addListener(tb._tg, "[TB]" + n, wrapper, false, true);
            });
        });
        return wrapper;
    };
    TbSelector.prototype.parent = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e.parent(nameSpace).forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        return that;
    };
    TbSelector.prototype.parents = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            e.parents(nameSpace).forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        return that;
    };
    TbSelector.prototype.prev = function (nameSpace) {
        var that = this, set = new Set();
        __classPrivateFieldGet(that, _set_1).forEach(function (e) {
            var elements = new Set(), element;
            element = e._tg;
            while (element.previousElementSibling !== null) {
                element = element.previousElementSibling;
                elements.add(element);
            }
            exports.$t(__spreadArrays(elements))
                .$d()
                .last()
                .$t()
                .forEach(function (tb) {
                set.add(tb);
            });
        });
        __classPrivateFieldSet(that, _set_1, set);
        if (nameSpace) {
            that.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return that;
    };
    TbSelector.prototype.trigger = function (ev, data, bubble) {
        if (data === void 0) { data = {}; }
        if (bubble === void 0) { bubble = 'l'; }
        var that = this;
        that.forEach(function (tb) {
            tb.trigger(ev, data, bubble);
        });
        return that;
    };
    /*
    array method mapping
    */
    TbSelector.prototype.at = function (index) {
        return exports.$t(this.array.at(index));
    };
    TbSelector.prototype.concat = function (items) {
        var that = this;
        items.forEach(function (tb) {
            __classPrivateFieldGet(that, _set_1).add(tb);
        });
        return that;
    };
    TbSelector.prototype.entries = function () {
        return this.array.entries();
    };
    TbSelector.prototype.every = function (predicate, thisArg) {
        return this.array.every(predicate, thisArg);
    };
    TbSelector.prototype.filter = function (predicate, thisArg) {
        return exports.$t(this.array.filter(predicate, thisArg));
    };
    TbSelector.prototype.forEach = function (predicate, thisArg) {
        var that = this;
        that.array.forEach(predicate, thisArg);
        return that;
    };
    TbSelector.prototype.includes = function (item) {
        return this.array.indexOf(item) !== -1;
    };
    TbSelector.prototype.indexOf = function (item, start) {
        return this.array.indexOf(item, start);
    };
    TbSelector.prototype.map = function (predicate, thisArg) {
        return this.array.map(predicate, thisArg);
    };
    TbSelector.prototype.pop = function () {
        var that = this, arr = that.array, result = arr.pop();
        __classPrivateFieldSet(that, _set_1, new Set(arr));
        return result;
    };
    TbSelector.prototype.push = function (item) {
        var that = this;
        __classPrivateFieldGet(that, _set_1).add(item);
        return that;
    };
    TbSelector.prototype.shift = function () {
        var that = this, first = this.array.shift();
        __classPrivateFieldGet(that, _set_1)["delete"](first);
        return first;
    };
    TbSelector.prototype.slice = function (start, end) {
        return exports.$t(this.array.slice(start, end));
    };
    TbSelector.prototype.some = function (predicate, thisArg) {
        return this.array.some(predicate, thisArg);
    };
    TbSelector.prototype.splice = function (start, deleteCount) {
        var items = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            items[_i - 2] = arguments[_i];
        }
        var that = this, arr = that.array, result = arr.splice.apply(arr, __spreadArrays([start, deleteCount], items));
        return exports.$t(result);
    };
    return TbSelector;
}());
exports.TbSelector = TbSelector;
_set_1 = new WeakMap();
var TbEvent = /** @class */ (function (_super) {
    __extends(TbEvent, _super);
    function TbEvent(type, data, bubble) {
        if (data === void 0) { data = {}; }
        if (bubble === void 0) { bubble = 'l'; }
        var _this = _super.call(this, type) || this;
        _this.data = data;
        _this.bubble = bubble;
        return _this;
    }
    return TbEvent;
}(Event));
exports.TbEvent = TbEvent;
/*
exported helpers
*/
exports.debounce = function (func, milliseconds) {
    var timeout;
    return function () {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            func.apply(void 0, arguments);
        }, milliseconds);
    };
};
exports.nameSpace = function (ns, obj, val, orig) {
    if (val === void 0) { val = null; }
    var leftOver = ns.split('.');
    if (!orig) {
        orig = obj;
    }
    // console.log( 'start', ns, obj, val, leftOver );
    var last = leftOver.shift(), // this property name
    lns = leftOver.join('.') // the leftover namespace
    ;
    if (val !== null && last.length && obj[last] === undefined) { // extend if val given and ns leftover...
        // console.log( 'add prop', last );
        obj[last] = {};
    }
    var lob = obj[last] // the leftover value or obj
    ;
    // console.log( 'lob', lob, 'last:', last, 'lns:', lns, 'obj:', obj );
    if (lns.length === 0) { // this should be the value to set or get
        if (val !== null) {
            obj[last] = val;
            return orig;
        }
        return obj[last];
    }
    if (isObject(obj[last])) {
        return exports.nameSpace(lns, obj[last], val, orig);
    }
    else {
        if (val) {
            return orig;
        }
        return lob[last];
    }
};
exports.parse = function (what, parseThis) {
    var args = Array.from(arguments);
    if (!args.length) {
        console.error('no arguments give to parse');
        return;
    }
    if (args.length === 1) {
        return args[1];
    }
    else if (args.length > 2) {
        while (args.length > 1) {
            args[0] = exports.parse(args[0], args[1]);
            args.splice(1, 1);
        }
        return args[0];
    }
    // implicit else: exactly 2 arguments
    if (typeof what === 'string') {
        var vars = what.match(/\{[^\{\}]*\}/g);
        if (!!vars) {
            vars
                .forEach(function (pPropname) {
                var propname = pPropname.substr(1, pPropname.length - 2), value = exports.nameSpace(propname, parseThis);
                if (typeof value !== 'undefined') {
                    what = what.replace(pPropname, value);
                }
            });
        }
    }
    else if (isObject(what)) {
        switch (what.constructor) {
            case Object:
                Object
                    .keys(what)
                    .forEach(function (pKey) {
                    if (what.hasOwnProperty(pKey)) {
                        what[pKey] = exports.parse(what[pKey], parseThis);
                    }
                });
                break;
            case Array:
                what
                    .forEach(function (pValue, pKey, original) {
                    original[pKey] = exports.parse(what[pKey], parseThis);
                });
                break;
        }
    }
    return what;
};
var TB = /** @class */ (function () {
    function TB(target, data) {
        var that = this;
        Object.defineProperty(that, '_id', { enumerable: false, writable: false, configurable: false, value: getId() });
        Object.defineProperty(that, '_tg', { get: function () { return target; } });
        Object.defineProperty(that, '_in', { value: data, writable: false, configurable: false });
    }
    TB.prototype.children = function (nameSpace) {
        var that = this, id = getId(), elements = exports.$d(that._tg);
        ;
        // generate temp attribute
        elements.attr('data-tempid', id);
        var selector = exports.$d(that._tg)
            .descendants()
            .$t()
            .filter(function (e) { return exports.$t(e).parent().$d().attr('data-tempid') === id; });
        // remove temporary attribute
        elements.attr('data-tempid', null);
        if (nameSpace) {
            selector.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return selector;
    };
    TB.prototype.descendants = function (nameSpace) {
        var that = this, id = getId(), elements = exports.$d(that._tg);
        ;
        // generate temp attribute
        elements.attr('data-tempid', id);
        var selector = exports.$d(that._tg)
            .descendants()
            .$t();
        // remove temporary attribute
        elements.attr('data-tempid', null);
        if (nameSpace) {
            selector.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return selector;
    };
    TB.prototype.off = function (name, cb) {
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        eventNames.forEach(function (name) {
            exports.$d(that._tg).off("[TB]" + name, cb);
        });
        return this;
    };
    TB.prototype.on = function (name, cb, once) {
        if (once === void 0) { once = false; }
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        eventNames.forEach(function (n) {
            _addListener(that._tg, "[TB]" + name, wrapper, false, once);
        });
        return wrapper;
    };
    TB.prototype.one = function (name, cb) {
        var that = this, eventNames = name.indexOf(' ') > -1
            ? name.split(' ')
            : [name];
        function wrapper() { wrapper.cb.apply(that, Array.from(arguments)); }
        ;
        wrapper.cb = cb;
        eventNames.forEach(function (n) {
            _addListener(that._tg, "[TB]" + name, wrapper, false, true);
        });
        return wrapper;
    };
    TB.prototype.parent = function (nameSpace) {
        var that = this, result;
        result = exports.$d(that._tg)
            .parents()
            .filter(function (e) { return e.tb().length; })
            .first()
            .$t();
        if (nameSpace) {
            result.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return result;
    };
    TB.prototype.parents = function (nameSpace) {
        var that = this, result;
        result = exports.$d(that._tg)
            .parents()
            .filter(function (e) { return e.tb().length; })
            .$t();
        if (nameSpace) {
            result.filter(function (e) { return e.nameSpace = nameSpace; });
        }
        return result;
    };
    TB.prototype.shadow = function (style) {
        var that = this;
        if (!that._sh) {
            that._sh = that._tg.attachShadow({ mode: "open" });
            that._sh.close = function () {
                that._sh = that._tg.attachShadow({ mode: "closed" });
            };
        }
        else {
            console.warn('IGNORED: shadow DOM already created! You may modify this._sh, however...');
        }
        if (style) {
            if (style instanceof HTMLStyleElement) {
                that._sh.appendChild(style);
            }
            else { // a style string
                var styleElement = document.createElement('style');
                styleElement.textContent = style.replace(/[\s]/g, ' ').replace(/  /g, ' '); // shorten it
                that._sh.appendChild(styleElement);
            }
        }
        return that._sh;
    };
    TB.prototype.siblings = function (nameSpace) {
        var that = this, target = that._tg;
        if (nameSpace !== undefined) {
            var i = target._tb[nameSpace];
            return i;
        }
        return target._tb;
    };
    TB.prototype.values = function (name, initial) {
        if (initial === void 0) { initial = {}; }
        //console.log( '[TB].values(', name, ',', initial, ')', this );
        this[name] = exports.values(this._tg, initial, this, name);
    };
    TB.prototype.trigger = function (ev, data, bubble) {
        if (data === void 0) { data = {}; }
        if (bubble === void 0) { bubble = 'l'; }
        var that = this, tbEvent = ev instanceof TbEvent ? ev : new TbEvent(ev, data, bubble);
        ;
        // if event __stopped__ , handling is cancelled
        // if ( (ev as TbEvent).__stopped__ || (ev as TbEvent).__immediateStopped__ ) {
        // 	return that;
        // }
        exports.$d(that._tg).trigger(tbEvent.type, data);
        // if event __stopped__ , handling is cancelled
        // if ( !!tbEvent.__stopped__  ) {
        // 	return that;
        // }
        setTimeout(function () {
            // bubble up
            if (tbEvent.bubble.indexOf('u') > -1) {
                that
                    .parent()
                    .trigger(new TbEvent(tbEvent.type, tbEvent.data, 'lu'));
            }
            // bubble down
            if (tbEvent.bubble.indexOf('d') > -1) {
                that
                    .children()
                    .forEach(function (tbInstance) {
                    tbInstance.trigger(new TbEvent(tbEvent.type, tbEvent.data, 'ld'));
                });
            }
        }, 0);
        return that;
    };
    return TB;
}());
exports.TB = TB;
/*
functions
*/
exports.values = function (target, values, tb, observable) {
    //console.log( 'values(', target, ',', values, ',', observable, ')' );
    var _a;
    if (values === void 0) { values = {}; }
    //console.log( 'values()', values );
    var handlers = getInputHandlers(target);
    function updateForm(values) {
        Object
            .keys(values)
            .forEach(function (key) {
            if (handlers.hasOwnProperty(key) && values.hasOwnProperty(key)) {
                //console.log( 'set value', key, values[key] );
                handlers[key] = values[key];
            }
        });
    }
    if (Object.keys(values).length > 0) {
        updateForm(values);
    }
    //console.log( 'set observable on form change', observable );
    if (observable !== undefined) {
        var obs = tb[observable];
        if (!((_a = obs.constructor) === null || _a === void 0 ? void 0 : _a.hasFormHandler)) {
            obs.constructor.hasFormHandler = true;
            obs.observe(updateForm);
        }
        tb[observable].observe(function () { });
        tb[observable].observe(function () { });
        exports.$d('input, textarea', target).on('keyup change', function () { tb[observable] = structuredClone(Object.assign(handlers)); });
        exports.$d('select', target).on('keyup select change', function () { tb[observable] = structuredClone(Object.assign(handlers)); });
    }
    return observable ? tb[observable] : Object.assign({}, handlers);
};
exports.load = function (fileName) {
    var promise;
    // special handling for .css and .js files
    if (/\.css$/.test(fileName)) {
        // css file
        //console.log( `css file: ${fileName}` );
        promise = new Promise(function (resolve, reject) {
            var link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', fileName);
            link.onload = function () {
                loadingScripts["delete"](fileName);
                resolve('ccs loaded ' + fileName);
            };
            link.onerror = function () {
                loadingScripts["delete"](fileName);
                console.warn('could not load ' + fileName);
                resolve('ccs NOT loaded!');
            };
            exports.$d(document.head).append(link);
        });
    }
    else if (/\.js$/.test(fileName)) {
        // js file, assumed to be a module
        //console.log( `js file: ${fileName}` );
        promise = new Promise(function (resolve, reject) {
            var se = document.createElement('script');
            se.setAttribute('src', fileName);
            se.setAttribute('blocking', 'render');
            se.setAttribute('type', 'module');
            loadingScripts.add(fileName);
            se.onload = function () {
                loadingScripts["delete"](fileName);
                resolve('js loaded ' + fileName);
            };
            se.onerror = function () {
                loadingScripts["delete"](fileName);
                console.warn('could not load ' + fileName);
                resolve('js NOT loaded!');
            };
            document.head.append(se);
        });
    }
    else {
        // other file
        // console.log( `other file: ${fileName}` );
        promise = fetch(fileName).then(function (p) { return p.text(); });
    }
    //console.log( promiseArray );
    return promise;
};
exports.loadAll = function () {
    var files = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        files[_i] = arguments[_i];
    }
    var promiseArray = [];
    // sanitze input ( flatten input array using whitespaces )
    files = words(files);
    // load files
    // special handling for .css and .js files
    files.forEach(function (fileName) {
        promiseArray.push(exports.load(fileName));
    });
    return Promise.all(promiseArray);
};
exports.$d = function (selection, element) {
    if (element === void 0) { element = document; }
    var set = new Set();
    if (selection instanceof Array) {
        selection.forEach(function (value) {
            exports.$d(value, element).forEach(function (n) {
                set.add(n);
            });
        });
        return new DomSelector(__spreadArrays(set));
    }
    else {
        return new DomSelector(selection, element);
    }
};
exports.$t = function (selection, element) {
    if (element === void 0) { element = document; }
    return exports.$d(selection, element).$t();
};
var _convertToObservables = function (target, propertyName) {
    Object.keys(target).forEach(function (key) {
        if (key[0] === '_')
            return;
        var val = target[key], Constructor;
        if (!val.prototype) {
            switch (typeof val) {
                case "string":
                    Constructor = String;
                    break;
                case "number":
                    Constructor = Number;
                    break;
                case "boolean":
                    Constructor = Boolean;
                    break;
                case "object":
                    Constructor = Object;
                    break;
                default:
                    return;
            }
        }
        var Observable = /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1(val) {
                var _this = _super.call(this, val) || this;
                _this.constructor.callbacks = [];
                if (isObject(val)) { // you cannot bind native values to the dom
                    _this.constructor.bound = [];
                    Object.getOwnPropertyNames(val).forEach(function (key) {
                        _this[key] = val[key];
                    });
                }
                return _this;
            }
            class_1.prototype.observe = function (f) {
                //console.log('observe');
                this.constructor.callbacks.push(f);
                return this;
            };
            class_1.prototype.bind = function (target) {
                var that = this, iso = isObject(that.valueOf()), bound = that.constructor.bound;
                //console.log('bind');
                if (iso) {
                    // console.log('bind...', that.valueOf(), target );
                    function walk(element) {
                        // console.log('-walk:', element);
                        if (!!element['nodeType'] && element.nodeType === 3) { // text node
                            // console.log('--type: textNode(3)');
                            var placeholders = Array.from(element.nodeValue && element.nodeValue.match(/\{[^\{\}]*\}/g) || []);
                            if (placeholders.length > 0) {
                                //console.log('--placeholders:', placeholders);
                                var f = (function (template) {
                                    //console.log('template', template);
                                    return function (values) {
                                        var t, changed = false;
                                        //console.log( 'placeholders:', placeholders, 'values:', values);
                                        if (placeholders) {
                                            if (iso) {
                                                placeholders.forEach(function (placeholder) {
                                                    if (f.values[placeholder] !== values[placeholder]) {
                                                        f.values[placeholder] = values[placeholder];
                                                        changed = true;
                                                    }
                                                });
                                            }
                                            else { // it is a simple value
                                                f.values = values;
                                                changed = true;
                                            }
                                            ;
                                        }
                                        if (changed) { // only reflow if changed
                                            t = template;
                                            //console.log('--template parse', t, (f as LooseObject).values );
                                            if (iso) {
                                                element.nodeValue = exports.parse(t, f.values);
                                            }
                                            else { // it is a simple value -> replace every placeholder whatever the name wi
                                                element.nodeValue = t.replace(/\{.*\}/g, f.values);
                                            }
                                            ;
                                        }
                                        return values;
                                    };
                                })(element.nodeValue || '');
                                f.values = iso ? {} : undefined;
                                placeholders = Array.from(placeholders).map(function (pKey) { return pKey.replace(/[\{\}]/g, '').trim(); });
                                var initial_1 = iso ? {} : undefined;
                                placeholders.forEach(function (pKey) {
                                    if (iso) {
                                        initial_1[pKey] = "";
                                    }
                                });
                                bound.push(f);
                                // console.log( `--bound textNode`, placeholders, f );
                                //(observableFunction as Function)(initial);
                            }
                            //console.log('--childNodes:', element.childNodes );
                            Array.from(element.childNodes)
                                .forEach(function (childNode) {
                                walk(childNode);
                            });
                            that.notify();
                        }
                        if (!!element['nodeType'] && element.nodeType === 1) { // HTML element
                            //console.log('--type: HTMLElement(1)');
                            Array.from(element.attributes)
                                .forEach(function (attributeNode) {
                                var placeholders = Array.from(attributeNode.value && attributeNode.value.match(/\{[^\{\}]*\}/g) || []);
                                if (placeholders.length > 0) {
                                    //console.log( `--[${attributeNode.value}] placeholders:`, placeholders );
                                    var f = (function (template) {
                                        return function (values) {
                                            var t, changed = false;
                                            //console.log( 'placeholders:', placeholders, 'values:', values);
                                            if (placeholders) {
                                                placeholders.forEach(function (pKey) {
                                                    if (f.values[pKey] !== values[pKey]) {
                                                        f.values[pKey] = values[pKey];
                                                        changed = true;
                                                    }
                                                });
                                            }
                                            if (changed) { // only reflow if changed
                                                t = template;
                                                element.nodeValue = exports.parse(t, f.values);
                                            }
                                            return values;
                                        };
                                    })(attributeNode.value || '');
                                    f.values = {};
                                    placeholders = Array.from(placeholders).map(function (pKey) { return pKey.replace(/[\{\}]/g, '').trim(); });
                                    var initial_2 = {};
                                    placeholders.forEach(function (pKey) {
                                        initial_2[pKey] = "";
                                    });
                                    bound.push(f);
                                    // console.log( `--bound element attribute`, placeholders, f );
                                    // console.log( `--bound`, observableFunction.bound );
                                    //(observableFunction as Function)(initial);
                                }
                            });
                            // console.log('--childNodes:', element.childNodes );
                            Array.from(element.childNodes)
                                .forEach(function (childNode) {
                                walk(childNode);
                            });
                            that.notify();
                        }
                    }
                    walk(target);
                    if (bound.length > 0) {
                        that.notify();
                    }
                }
                else {
                    console.warn('IGNORED - cannot bind a non-Object to the dom, IS:', typeof that.valueOf());
                }
                return this;
            };
            class_1.prototype.notify = function () {
                var that = this, data = isObject(that.valueOf()) ? structuredClone(Object.assign({}, that)) : that.valueOf();
                // console.log('notify', that.constructor, (that.constructor as any).callbacks.length, (that.constructor as any)?.bound?.length, data );
                that.constructor.callbacks.forEach(function (f) {
                    // console.log( 'callback', f, data);
                    f(data);
                });
                if (isObject(that.valueOf())) {
                    // console.log('bound', this.constructor);
                    that.constructor.bound.forEach(function (f) {
                        // console.log( 'bound', f, data);
                        f(data);
                    });
                }
                return this;
            };
            return class_1;
        }(Constructor));
        //(Observable.constructor as LooseObject).callbacks = [];
        var o = new Observable(val);
        Object.defineProperty(target, key, {
            enumerable: true,
            get: function () {
                if (isObject(o.valueOf())) {
                    var check_1 = structuredClone(Object.assign({}, o));
                    setTimeout(exports.debounce(function checkChanges() {
                        var state = structuredClone(Object.assign({}, o));
                        //console.log( 'check changes', state, check );
                        if (!deepEqual(state, check_1)) {
                            //console.log( '-> not equal!' );
                            o.notify();
                        }
                    }, 0), 0);
                }
                return o;
            },
            set: function (val) {
                var _a, _b;
                //console.log( 'SET:', isObject( val.valueOf() ), 'set:', val, 'to', isObject( o.valueOf() ), o, );
                if (typeof val === typeof o.valueOf()) {
                    var iso = isObject(o.valueOf());
                    //let iso = o.constructor.prototype.__proto__.constructor === Object;
                    //console.log('target iso:', iso);
                    if (iso) {
                        var old_1 = structuredClone(Object.assign({}, o));
                        val = structuredClone(Object.assign({}, val));
                        var hasFormHandler = (_b = (_a = o.constructor) === null || _a === void 0 ? void 0 : _a.hasFormHandler) !== null && _b !== void 0 ? _b : false, callbacks = o.constructor.callbacks, bound = o.constructor.bound;
                        o = new Observable(structuredClone(Object.assign({}, val)));
                        //console.log('after set o=', o, 'check', old);
                        o.constructor.callbacks = hasFormHandler;
                        o.constructor.callbacks = callbacks;
                        o.constructor.bound = bound;
                        setTimeout(// check for changes
                        exports.debounce(function checkChanges() {
                            var state = structuredClone(Object.assign({}, o));
                            //console.log( 'check changes', state, check );
                            if (!deepEqual(state, old_1)) {
                                //console.log( '-> not equal!' );
                                o.notify();
                            }
                        }, 0), 0);
                    }
                    else { // simple native var
                        var old = o.valueOf(), callbacks = o.constructor.callbacks;
                        o = new Observable(val);
                        o.constructor.callbacks = callbacks;
                        if (val !== old) {
                            o.notify();
                        }
                    }
                }
                else {
                    console.warn('IGNORED - cannot change observable types, IS:', typeof o.valueOf(), '- CANNOT BECOME:', typeof val);
                }
            }
        });
        target[key] = val;
    });
};
exports.createTb = function (target, definitionClass, data) {
    //console.log( 'createTbInstance', target, definitionClass, data );
    if (data === void 0) { data = {}; }
    if (!target) {
        throw new Error('no target HTML element given');
    }
    if (!definitionClass) {
        throw new Error('no definition class given');
    }
    var instance = new definitionClass(target, data);
    _autoAttachListeners(target, instance); // auto-attaches all native and tb event listeners
    target._tb = target._tb || {};
    target._tb[definitionClass.name] = instance;
    //console.log( 'in cTb', target, data )
    _convertToObservables(instance);
    var nameSpace = definitionClass.name || target.tagName.toLowerCase().split('-').join('.');
    Object.defineProperty(instance, '_ns', { value: nameSpace, writable: false, configurable: false });
    return instance;
};
var createCustomElement = function (tagName, definitionClass, observedAttributes, userDefinedBuiltinObject) {
    if (observedAttributes === void 0) { observedAttributes = []; }
    if (userDefinedBuiltinObject === void 0) { userDefinedBuiltinObject = 'x'; }
    //console.log( 'createCustomElement', tagName, definitionClass, observedAttributes, userDefinedBuiltinObject );
    // create custon element
    if (customElements.get(tagName) === undefined) {
        var wantUBE = userDefinedBuiltinObject === 'x' ? false : true, extendClass = HTMLElement;
        //console.log( 'wantUBE', wantUBE );
        //console.log( 'create custom element' );
        // parameter sanitation
        if (!userDefinedBuiltinObject) {
            if (tagName.indexOf('-') === -1) {
                throw ('createCustomElement: pTagName must be a string containing at least one hyphen: "-" ');
            }
        }
        else {
            var e = document.createElement(userDefinedBuiltinObject);
            if (e instanceof HTMLUnknownElement) {
                //console.log(`[${userDefinedBuiltinObject}] is an unknown HTML element: default to HTMLElement`);
            }
            else {
                extendClass = window[document.createElement(userDefinedBuiltinObject).constructor.name];
            }
        }
        // auto-define autonomous custom element
        customElements.define(tagName, /** @class */ (function (_super) {
            __extends(class_2, _super);
            function class_2(data) {
                var _this = _super.call(this) || this;
                exports.createTb(_this, definitionClass, data);
                return _this;
            }
            Object.defineProperty(class_2, "observedAttributes", {
                get: function () {
                    return observedAttributes;
                },
                enumerable: false,
                configurable: true
            });
            class_2.prototype.connectedCallback = function () {
                var that = this;
                //console.log( 'createCustomElement trigger [TB]oneConnected' );
                that.dispatchEvent(new Event('[TB]oneConnected'));
                //console.log( 'createCustomElement trigger [TB]onConnected' );
                that.dispatchEvent(new Event('[TB]onConnected'));
            };
            class_2.prototype.disconnectedCallback = function () {
                var that = this;
                //console.log( 'createCustomElement trigger [TB]oneDisconnected' );
                that.dispatchEvent(new Event('[TB]oneDisconnected'));
                //console.log( 'createCustomElement trigger [TB]onDisconnected' );
                that.dispatchEvent(new Event('[TB]onDisconnected'));
            };
            class_2.prototype.adoptedCallback = function () {
                var that = this;
                //console.log( 'createCustomElement trigger [TB]oneAdopted' );
                that.dispatchEvent(new Event('[TB]oneAdopted'));
                //console.log( 'createCustomElement trigger [TB]onAdopted' );
                that.dispatchEvent(new Event('[TB]onAdopted'));
            };
            class_2.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
                var that = this, ev, data = { name: name, oldValue: oldValue, newValue: newValue };
                //console.log( 'createCustomElement trigger [TB]oneAttributechanged' );
                ev = new Event('[TB]oneAttributeChanged');
                ev.data = data;
                that.dispatchEvent(ev);
                //console.log( 'createCustomElement trigger [TB]onAttributechanged' );
                ev = new Event('[TB]onAttributeChanged');
                ev.data = data;
                that.dispatchEvent(ev);
            };
            return class_2;
        }(extendClass)), wantUBE
            ? { "extends": userDefinedBuiltinObject.toLocaleLowerCase() }
            : undefined);
    }
};
var getCustomElement = function (tagName) {
    return customElements.get(tagName);
};
var CEconstructors = {};
var CEinventory = new Set();
var CE = /** @class */ (function () {
    function CE(tagName, definitionClass, observedAttributes) {
        if (observedAttributes === void 0) { observedAttributes = []; }
        if (!CEinventory.has(tagName)) {
            createCustomElement(tagName, definitionClass, observedAttributes);
            CEinventory.add(tagName);
            CEconstructors[definitionClass.name] = getCustomElement(tagName);
        }
        else {
            console.warn('IGNORED: new CE(...) - custom element', tagName, 'is already defined!', __spreadArrays(CEinventory));
        }
    }
    CE.get = function (tagName) {
        if (tagName) {
            var constructor = getCustomElement(tagName);
            return constructor;
        }
        return CEinventory;
    };
    return CE;
}());
exports.CE = CE;
var UBEconstructors = {};
var UBEinventory = new Set();
var UBE = /** @class */ (function () {
    function UBE(tagName, definitionClass, observedAttributes, userDefinedBuiltinObject) {
        if (observedAttributes === void 0) { observedAttributes = []; }
        if (userDefinedBuiltinObject === void 0) { userDefinedBuiltinObject = 'x'; }
        if (!UBEinventory.has(tagName)) {
            createCustomElement(tagName, definitionClass, observedAttributes, userDefinedBuiltinObject);
            UBEinventory.add(tagName);
            UBEconstructors[definitionClass.name] = getCustomElement(tagName);
        }
        else {
            console.warn('IGNORED: new UBE(...) - user defined built-in element', userDefinedBuiltinObject, 'is already defined!', __spreadArrays(UBEinventory));
        }
    }
    UBE.get = function (tagName) {
        if (tagName) {
            var constructor = getCustomElement(tagName);
            return constructor;
        }
        return UBEinventory;
    };
    return UBE;
}());
exports.UBE = UBE;
