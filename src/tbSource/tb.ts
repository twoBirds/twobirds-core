/*
ts types
*/
export type singleSelector = string | ShadowRoot | HTMLElement | HTMLElement[] | HTMLCollection | NodeList | DSelector | TB | TSelector | undefined;

export type selection = singleSelector | singleSelector[];

/*
internal helpers
*/

function isObject( value: any ): boolean {
  return value != null && typeof value === 'object';
}

const deepEqual = ( o1: any, o2:any): boolean => {
	const k1 = Object.keys(o1);
	const k2 = Object.keys(o2);
	if (k1.length !== k2.length) {
		return false;
	}
	for (const key of k1) {
		const
			v1 = o1[key],
			v2 = o2[key],
			recurse = isObject(v1) && isObject(v2);
		;
		if (
			( recurse && !deepEqual(v1, v2) )
			|| (!recurse && v1 !== v2)
		) {
			return false;
		}
	}
	return true;
}

const _makeLoadData = ( e: HTMLElement ): Array<any> | false => {

		let
			tagName = e?.localName ?? '',
			isAttribute = e.getAttribute?.('is') || '',
			isUBE = isAttribute ? true : false,
			isCE = tagName.indexOf('-') !== -1 ? true : false,
			ubeSelector = isUBE ? `${tagName}[is=\"${isAttribute}\"]:defined` : '',
			ceSelector = isCE ? tagName + ':defined' : '',
			fileBaseName = isUBE
				? isAttribute
				: isCE
					? tagName
					: '',
			fileName = fileBaseName.length ? '/' + fileBaseName.split('-').join('/') + '.js' : '',
			selector = isUBE
				? ubeSelector
				: isCE
					? ceSelector
					: ''
		;

		//console.log( 'makeLoadData()', tagName, isUBE, isCE, ubeSelector, ceSelector, fileBaseName, ' => ', (isUBE || isCE) ? [ e, selector, fileName ] : false );
		return (isUBE || isCE) ? [ e, selector, fileName, fileBaseName ] : false;
}

const words = ( classes: Array<string> ): Array<string> => {

	let classSet: Set<string> = new Set();

	// read classes into set
	classes.forEach( c => {
		c = c.trim();
		c.split(' ').forEach( e => {
			if ( e !== '' ){
				classSet.add(e);
			}
		});
	});

	// now we have a sanitized array
	classes = Array.from( classSet );

	return classes;
}

const getInputName = ( e: HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement ): string => {

	let ret = e instanceof HTMLInputElement
		? 'input'
		: e instanceof HTMLSelectElement
			? 'select'
			: e instanceof HTMLTextAreaElement
				? 'textarea'
				: ''
		;

	return ret;
}

// set of currently loading scripts
const loadingScripts: Set<string> = new Set();

const _loadRequirements = ( node: HTMLTemplateElement | HTMLElement ): Promise<any> => {

	// return Promise.resolve();
	let
		undefinedSet: Set<any> = new Set(),
		element: any = node instanceof HTMLTemplateElement ? (node as HTMLTemplateElement).content : node,
		head = document.head,
		allPromises: Array<any> = [],
		e: HTMLElement;
	;

	if ( element instanceof HTMLTemplateElement === false ){
		// if node itself is undefined
		let data: any = _makeLoadData(element);

		//console.log('data', element, data)
		if ( data && !data[0].matches( data[1] ) ){
			// console.log( '+file:', data );
			undefinedSet.add( data );
		}
	}

	// if some of the childNodes are undefined
	Array.from( element.childNodes ).forEach(( node )=>{
		let data: any = _makeLoadData(node as HTMLElement);

		//console.log('data', element, data)
		if ( data && !data[0].matches( data[1] ) ){
			// console.log( '+file:', data );
			undefinedSet.add( data );
		}
	});

	if ( !!undefinedSet.size ){ // there are requirements

		//console.log( 'require set', [ ...undefinedSet ] );
		undefinedSet.forEach( (data) => {
			if ( !getCustomElement( data[3] ) && !loadingScripts.has( data[2]) ){

				loadingScripts.add( data[2] );

				//console.info( '%cloading:', 'color:blue;', data[2], Array.from(loadingScripts) );

				const se: HTMLScriptElement = document.createElement('script');

				se.setAttribute( 'src', data[2] );
				se.setAttribute( 'blocking', 'render' );
				se.setAttribute( 'type', 'module' );
				let s = (data as any)[2];
				se.onload = () => {
					// console.info('loaded', s);
					loadingScripts.delete( s );
				};

				head.append( se );

				let e = (data as Array<any>)[0];
				//allPromises.push( customElements.whenDefined( e ) );
			} else {
				return ;
			}
		});

	}

	return Promise.all( allPromises );
}

const _htmlToElements = ( html: string ): HTMLElement[] => {
	let template = document.createElement('template');

	html = html
		.replace( /<([A-Za-z0-9\-]*)([^>\/]*)(\/>)/gi, "<$1$2></$1>"); // replace XHTML closing tags by full closing tags

	template.innerHTML = html;

	template.content.normalize();

	if ( template.content.childNodes.length ){
		_loadRequirements(template);
	}

	//// console.log( '_htmlToElements', isHtml, template.content, ret );

	return ( Array.from(template.content.childNodes) as HTMLElement[] );
}

const _getElementList = ( selector: singleSelector, rootNode: ShadowRoot | HTMLElement | Document ): HTMLElement[] => {

	//// console.log('_getElementList(', ...arguments,')');

	let
		result: Set<HTMLElement> = new Set();
	;

	if (!selector) { // no selector given, or a falsy value
		return [];
	} else if (typeof selector === 'string') {

		selector = selector
			.replace( /\r/g,'')
			.replace( /\n/g,'')
			.replace( /\t/g,'')
			.replace( />\s*</g,'><')
			.trim();

		if ( !selector.match( /^<.*>$/ ) ){
			// it is not a HTML string, but a simple string --> it is regarded a CSS selector
			const nodeList: NodeList = rootNode.querySelectorAll( selector );
			nodeList.forEach( (node) => {
				result.add( node as HTMLElement );
			});
			return [ ...result ] as HTMLElement[];
		} else { // assume HTML string
			// return html content as a set of nodes
			return _htmlToElements(
				selector
			);
		}
	} else if ( selector instanceof HTMLElement || selector instanceof ShadowRoot ) { // selector is a dom node
		result.add( selector as HTMLElement );
	} else if ( selector instanceof DomSelector ) { // selector is a $d() result set
		selector.forEach( e => {
			result.add( e );
		});
	} else if ( selector instanceof TbSelector ) { // selector is a tb() result set
		selector.forEach( e => {
			result.add( e._tg );
		});
	} else if ( selector instanceof TB ) { // selector is a TB instance
		result.add( (selector as any)._tg );
	} else if ( selector instanceof NodeList ){
		selector.forEach( n => {
			result.add( n as HTMLElement );
		});
	} else if ( selector instanceof HTMLCollection ){
		Array.from(selector).forEach( e => {
			result.add( e as HTMLElement );
		});
	} else if ( selector instanceof Array ) {
		selector.forEach( e => {
			result.add( e as HTMLElement );
		});
	}

	//// console.log( 'result', [ ...result ] );

	return [ ...result ] as HTMLElement[];
}

const _addListener = ( domNode: Node, eventName: string, handler: EventListenerOrEventListenerObject | null, capture: boolean = false, once: boolean = false ) => {
	let
		options: LooseObject = {}
	;

	if ( capture ){
		options.capture = capture;
	}

	if ( once ){
		options.once = once;
	}

	domNode.addEventListener( eventName, handler, options );
}

const _removeListener = ( domNode: Node, eventName: string, handler: EventListenerOrEventListenerObject | null ) => {
	domNode.removeEventListener( eventName, handler );
}

const nativeEventNames: Array<string> = Object.keys( HTMLElement.prototype ).filter( key => /^on/.test(key) );

const _autoAttachListeners = ( target: HTMLElement, instance: Object ) => {

	//console.log( 'autoAttachListeners', target, instance );

	Object
		.getOwnPropertyNames((instance as any).__proto__)
		.filter( key => /^on[A-Z]|one[A-Z]/.test(key) )
		.forEach( key => {
			let
				once = /^one/.test(key),
				withoutOnOne = key.replace( /^on[e]{0,1}/, '' ),
				nativeEventName = 'on' + withoutOnOne.toLowerCase(),
				listener = Object.getPrototypeOf( instance )[key].bind(instance),
				isNative = nativeEventNames.indexOf( nativeEventName ) > -1;

			//console.log( '_addListener', isNative, withoutOnOne.toLowerCase(), 'or', '[TB]' + key );

			if ( isNative ){ // native event present
				//console.log( '_addListener native', target, withoutOnOne.toLowerCase(), listener, false, once );
				_addListener( target, withoutOnOne.toLowerCase(), listener, false, once );
			} else { // an event handler, but not native -> assumed to be a twobirds event
				//console.log( '_addListener [TB]', target, '[TB]' + key, listener, true, once );
				_addListener( target, '[TB]' + key, listener, true, once );
			}
		});

}

const getId = (): string => {
	let i: string;
	if ( i = crypto?.randomUUID() ){
		return i;
	}
	return (
		'id-' +
		new Date().getTime() +
		'-' +
		Math.random().toString().replace(/\./, '')
	);
}

type inputTypes = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement;

abstract class InputBase{
	constructor( e: any ){
	}

	abstract get val(): any;

	abstract set val( value: any );

}

const getInputHandlers = ( e: HTMLElement | ShadowRoot ): LooseObject => {

		let
			//inputList = ( Array.from( e.querySelectorAll('*') ) as HTMLElement[] ).filter( i => isInputElement(i) ),
			inputList = $d('input,select,textarea', e).array,
			handlers: LooseObject = {};

		inputList.forEach( (i) => {
			const
				name: string = ( i as inputTypes ).getAttribute('name') || 'undefined';

			if ( name ){
				let h: InputBase;
				switch ( getInputName( i as inputTypes ) ){
					case 'input':
						//console.log( i, 'is HTMLInputElement');
						h = new InputHandler( i as HTMLInputElement, e ); // e = element to search radios in = radioBase
						Object.defineProperty( handlers, name, {
							enumerable: true,
							configurable: true,
							get: (): any => { return h.val },
							set: ( value: any ) => { h.val = value }
						});
						break;
					case 'select':
						//console.log( i, 'is HTMLSelectElement');
						h = new SelectHandler( i as HTMLSelectElement );
						Object.defineProperty( handlers, name, {
							enumerable: true,
							configurable: true,
							get: (): any => { return h.val },
							set: ( value: any ) => { h.val = value }
						});
						break;
					case 'textarea':
						//console.log( i, 'is HTMLTextAreaElement');
						h = new TextAreaHandler( i as HTMLTextAreaElement );
						Object.defineProperty( handlers, name, {
							enumerable: true,
							configurable: true,
							get: (): any => { return h.val },
							set: ( value: any ) => { h.val = value }
						});
						break;
					default:
						console.warn('handler missing:', name, 'for', i);
						break;
				}
			}
		});

		return handlers;
}

class InputHandler extends InputBase {

	#e: HTMLInputElement;
	#type: string;
	#radioBase: any;

	constructor( e: HTMLInputElement, radioBase?: HTMLElement | ShadowRoot ){
		super( e );
		this.#e = e;
		this.#type = e.type;
		this.#radioBase = radioBase;
	}

	get val(): any{
		let that = this,
			type = that.#type,
			ret: any;

		switch (type){
			case 'radio':
				const check = $d( `input[type="radio"][name="${ this.#e.getAttribute('name') }"]`, this.#radioBase )
					.filter( e => e.checked );
				if ( !check.length ) return '';
				ret = check.array[0];
				return ret.value;
			case 'checkbox':
				return that.#e.checked;
			default:
				return that.#e.value;
				break;
		}

		return '';
	}

	set val( value: any ){
		let that = this,
			type = that.#type,
			ret: any;

		switch (type){
			case 'radio':
				const
					selection = `input[type="radio"][name="${ this.#e.getAttribute('name') }"]`,
					radios = $d( selection, this.#radioBase );

				// console.log('set radios', selection, this.#radioBase, radios.array );

				if ( !radios.length ) return;

				if ( value === '' ){ // uncheck all
					radios.forEach( r => { if (r.checked) r.checked = false } );
					//console.log('unset all radios', radios.array );
					return;
				}

				const setRadioElement = radios.filter( r => r.getAttribute('value') === value );

				if ( !setRadioElement.length ) return;

				// console.log('set this radio', setRadioElement.array );

				(setRadioElement.array[0] as HTMLInputElement).checked = true;

				// console.log('done', setRadioElement.array );
				return;
				break;
			case 'checkbox':
				that.#e.checked = value;
				break;
			default:
				that.#e.value = value;
				break;
		}

		return;
	}

}

class SelectHandler extends InputBase {

	#e: HTMLSelectElement;
	#multi: boolean = false;

	constructor( e: HTMLSelectElement ){
		super( e );
		this.#e = e;
		this.#multi = e.type === 'select-multiple';
	}

	get val(): string[] | string{
		const
			that = this,
			ret: string[] = [];

		// get all options
		const options = $d( that.#e.querySelectorAll('option') )
			.filter( o => o.selected )
			.forEach( o => { if ( o.selected ) ret.push( o.value ) } );

		if ( this.#multi ) return ret;
		return ret.length ? ret[0] : '';
	}

	set val( value: string[] | string ){
		const
			that = this;

		value = typeof value === 'string' ? [ value ] : value;

		//console.log('set values', value );

		// set all options
		const options = $d( that.#e.querySelectorAll('option') )
			.forEach( o => {
				if ( value.indexOf( o.value ) > -1 ) {
					o.setAttribute( 'selected', true )
					o.selected = true;
				} else {
					o.removeAttribute( 'selected' )
					o.selected = false;
				}
			});

		/*
		if (pOption.selected) {
			if (
				!pOption.disabled &&
				(!pOption.parentNode.disabled ||
					pOption.parentNode.nodeName !==
						'optgroup')
			) {
				var value = pOption.value;

				ret.push(value);
			}
		}
		*/
		return;
	}

}

class TextAreaHandler extends InputBase {
	#e: HTMLTextAreaElement
	constructor( e: HTMLTextAreaElement ){
		super(e);
		this.#e = e;
	}

	get val(): string{
		return this.#e.value;
	}

	set val( value: string ){
		this.#e.value = value;
		return;
	}

}

/*
selector classes
*/

class DomSelector{

	#set: Set<HTMLElement> = new Set();

	public get set(): Set<HTMLElement> {
		return this.#set;
	}

	public get array(): HTMLElement[] {
		return Array.from( this.#set );
	}

	public set set( set: Set<HTMLElement> ) {
		this.#set = set;
		return;
	}

	public get length(): number {
		return this.#set.size;
	}

	constructor( selection?: selection, element: any = document.body ){

		let
			that = this,
			undefinedTags: Set<any> = new Set();

		// // console.log('DomSelector constructor', ...arguments);

		if ( !selection ) {
			return that;
		}


		if ( selection instanceof Array ){
			selection.forEach( (select) => {
				_getElementList( select, element ).forEach( (e) => {
					if ( e.localName?.indexOf('-') !== -1 || e.getAttribute?.('is') ) _loadRequirements( e );
					that.#set.add( e );
				});
			});
		} else {
			_getElementList( selection, element ).forEach( (e) => {
				if ( e.localName?.indexOf('-') !== -1 || e.getAttribute?.('is') ) _loadRequirements( e );
				that.#set.add( e );
			});
		}

	}

	$t( nameSpace?: string ): TSelector {
		let
			result = new TbSelector()
		;

		this.#set.forEach( (e: HTMLElement) => {
			if ( (e as THTMLElement)._tb ){
				Object.values( (e as THTMLElement)._tb ).forEach( (tb: TB) => {
					result.set.add( tb );
				});
			}
		});

		if ( nameSpace ) {
			result.filter( tb => (tb as LooseObject).nameSpace === nameSpace );
		}

		return result;
	}

	add( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this
		;

		$d( selection, target ).forEach( element => {
			that.#set.add( element );
		});

		return that;
	}

	addClass( ...classes: string[] ): DSelector  {

		// sanitize classes
		classes = words( classes );

		// now set those classes
		this.forEach( element => {
			element.classList.add( ...classes );
		});

		return this;
	}

	after( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this,
			after = $d( selection, target );
		;

		if ( !after.length ) return that;

		after
			.first()
			.forEach( element => {
				that.array.reverse().forEach( (e: HTMLElement) => {
					e.after( element );
				});
			});

		return that;
	}

	append( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this,
			addTo = that.array['0'],
			append = $d( selection, target )
		;

		if ( addTo ){
			append.forEach( (e: HTMLElement) => {
				addTo.append( e );
			});
		}

		return that;
	}

	appendTo( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this,
			appendTo = $d( selection, target ).array['0']
		;

		if ( appendTo ){
			that.forEach( (e: HTMLElement) => {
				appendTo.append( e );
			});
		}

		return that;
	}

	attr(): LooseObject;
	attr( obj: LooseObject ): DSelector;
	attr( name: string ): string | null;
	attr( first?: string | LooseObject, second?: string | object | null ): DSelector;
	attr( first?: unknown, second?: unknown ): DSelector | LooseObject | string | null {
		let
			that = this,
			attributes: LooseObject = {}
		;

		// if no elements in set -> skip
		if ( !that.length ) return that;

		// if no arguments, return attributes of first in list as hash object
		if (!arguments.length) {
			Array.from( [ ...that.set ][0].attributes ).forEach( (attribute: LooseObject) => {
				attributes[attribute.name] = attribute.value;
			});
			return attributes;
		}

		// if first is a string
		//   if no value (second) is given -> return attribute value of first element in node selection
		//   if value (second) is null -> remove attribute and return this
		//   else set attribute values and return this
		if ( typeof first === 'string' ) {

			// return attributes
			if ( second === undefined ){
				return [ ...that.set ][0].getAttribute( first );
			}

			// remove attribute
			if ( second === null ){
				[ ...that.set ].forEach( (e: HTMLElement) =>{
					e.removeAttribute( first );
				});
				return that;
			}

			// if attribute value is an object -> convert to json string
			if ( typeof second === 'object' ){
				second = JSON.stringify( second );
			}

			// set attribute
			[ ...that.set ].forEach( (e: HTMLElement) =>{
				e.setAttribute( first, second as string );
			});

			return that;
		}

		// if first is an object set attributes to all nodes
		if ( typeof first === 'object' ) {
			[ ...that.set ].forEach( (e: HTMLElement) =>{
				Object.keys( first as LooseObject ).forEach( (key: string) => {
					$d(e).attr( key, ( first as LooseObject ).key );
				});
			});
			return that;
		}

		return that;
	}

	before( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this,
			before = $d( selection, target );
		;

		if ( !before.length ) return that;

		before
			.first()
			.forEach( element => {
				that.forEach( (e: HTMLElement) => {
					e.before( element );
				});
			});

		return that;
	}

	children( selection?: selection, rootNode?: HTMLElement ): DSelector{

		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			Array.from( e.children ).forEach( ( c ) => {
				set.add( c as HTMLElement );
			});
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	descendants( selection?: selection, rootNode?: HTMLElement ): DSelector{
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			e.querySelectorAll('*').forEach( ( h ) => {
				set.add( h as HTMLElement );
			});
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	empty(): DSelector {
		return this.filter( (e: any)=>false );
	}

	first( selection?: selection, rootNode?: HTMLElement ): DSelector{
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set(),
			compare: any = false;
		;

		// get compare selection if specified
		if ( selection ){
			compare = $d( selection, rootNode );
		}

		// get compare selection if specified
		that.#set.forEach( (e: HTMLElement) => {
			if ( e.parentNode ){
				set.add( e.parentNode.children[0] as HTMLElement );
			}
		});

		// make result
		that = $d( [ ...set ] );

		// compare if necessay
		if ( selection ){
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	has( element: HTMLElement ): boolean{
		return this.#set.has( element );
	}

	hasClass( ...classes: string[] ): boolean  {

		// sanitize classes
		classes = words( classes );

		// now test those classes
		return this.every( e => {
			return Array.from( e.classList ).every( c => e.contains( c ) );
		});

	}


	html( html?: string ): string | DSelector{
		let that = this;

		if ( html ){
			that.forEach( (e: HTMLElement) => {
				!e.parentNode
					? (()=>{
						console.warn('Cannot access the .innerHTML of a  HTMLElement that hasnt been inserted yet', e );
						console.trace( e, this );
						console.info( 'If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;' )
					})()
					: e.innerHTML = html;
			});
			return that;
		}

		return that.array[0].innerHTML;
	}

	last( selection?: selection, rootNode?: HTMLElement ): DSelector{
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			let
				siblings = e.parentNode?.children as HTMLCollection
			;
			if ( siblings ) {
				set.add( Array.from(siblings).at(-1) as HTMLElement );
			}
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	load(): DSelector{
		let that = this;

		if( !that.length ){
			that.add( document.body );
		}

		that.forEach( e => _loadRequirements( e ) );

		return that;
	}

	next( selection?: selection, rootNode?: HTMLElement ): DSelector{
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			let
				siblings = e.parentNode?.children as HTMLCollection
			;
			if ( siblings ) {
				let
					arr = Array.from(siblings),
					pos = arr.indexOf(e) + 1
				;
				if ( pos < arr.length ){
					set.add( arr.at(pos) as HTMLElement );
				}
			}
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	normalize(): DSelector {
		let that = this;
		that.#set.forEach( (e: HTMLElement) => { e.normalize() });
		return that;
	}

	off( eventName: string, cb: Function ): DSelector{
		let
			that = this,
			eventNames =
				eventName.indexOf(' ') > -1
					? eventName.split(' ')
					: [ eventName ]
		;

		// remove handler
		that.array.forEach( (e: HTMLElement) => {
			eventNames.forEach(function ( n: string ) {
				_removeListener(
					e,
					n,
					cb as EventListenerOrEventListenerObject
				);
			});
		});

		return that;
	}

	on( eventName: string, cb: Function, capture: boolean = false, once: boolean = false ): Function{

		let
			that = this,
			eventNames =
				eventName.indexOf(' ') > -1
					? eventName.split(' ').filter( (s) => s !== '' )
					: [ eventName ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		// attach handler
		that.forEach( (e: HTMLElement) => {
			eventNames.forEach(function ( n: string ) {
				_addListener(
					e,
					n,
					(wrapper as EventListenerOrEventListenerObject),
					capture,
					once
				);
			});
		});

		return wrapper;
	}

	one( eventName: string, cb: Function, capture: boolean = false ): Function{
		let
			that = this,
			eventNames =
				eventName.indexOf(' ') > -1
					? eventName.split(' ').filter( (s) => s !== '' )
					: [ eventName ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		// attach handler
		that.forEach( (e: HTMLElement) => {
			eventNames.forEach(function ( n: string ) {
				_addListener(
					e,
					n,
					(wrapper as EventListenerOrEventListenerObject),
					capture,
					true
				);
			});
		});

		return wrapper;
	}

	parent( selection?: selection, rootNode?: HTMLElement ): DSelector {
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			if ( e.parentNode && (e.parentNode as HTMLElement).tagName !== 'HTML') {
				set.add( e.parentNode as HTMLElement );
			}
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	parents( selection?: selection, rootNode?: HTMLElement ): DSelector {
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			let
				current = e
			;
			while ( current.parentNode && (current.parentNode as HTMLElement).tagName !== 'HTML' ){
				current = current.parentNode as HTMLElement;
				set.add( current );
			}
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	prepend( selection: selection, target?: HTMLElement ): DSelector  {

		let
			that = this,
			addTo = that.array['0'],
			prepend = $d( selection, target )
		;

		if ( addTo ){
			prepend.forEach( (e: HTMLElement) => {
				addTo.prepend( e );
			});
		}

		return that;
	}


	prev( selection?: selection, rootNode?: HTMLElement ): DSelector{
		let
			that: DomSelector = this,
			set: Set<HTMLElement> = new Set()
		;

		that.#set.forEach( (e: HTMLElement) => {
			let
				siblings = e.parentNode?.children as HTMLCollection
			;
			if ( siblings ) {
				let
					arr = Array.from(siblings),
					pos = arr.indexOf(e) - 1
				;
				if ( pos > -1 ){
					set.add( arr.at(pos) as HTMLElement );
				}
			}
		});

		that = $d( [ ...set] );

		if ( selection ){
			let	compare = $d( selection, rootNode );
			that.filter( ( e: HTMLElement ) => compare.has( e ) );
		}

		return that;
	}

	removeClass( ...classes: string[] ): DSelector  {

		// sanitize classes
		classes = words( classes );

		// now set those classes
		this.forEach( element => {
			element.classList.remove( ...classes );
		});

		return this;
	}

	text( text?: string ): string | DSelector{
		let that = this;
		if ( text ){
			that.forEach( (e: HTMLElement) => {
				!e.parentNode
					? (()=>{
						console.warn('Cannot access the .textContent of a  HTMLElement that hasnt been inserted yet', e );
						console.trace( e, this );
						console.info( 'If you called this from inside a Web Component constructor, consider using the %c[connected] event callback!', 'color: blue;' )
					})()
					: e.innerText = text;
			});
			return that as DomSelector;
		}

		let t = that.array[0].innerText;
		return  t ?? '' as string;
	}

	toggleClass( ...classes: string[] ): DSelector {

		// sanitize classes
		classes = words( classes );

		// now set those classes
		this.forEach( element => {
			element.classList.toggle( ...classes );
		});

		return this;
	}

	trigger( eventName: string, data: any = {}, bubbles: boolean = false, cancelable: boolean = false, composed: boolean = false ): DSelector{
		let
			that = this,
			ev: Event,
			options: LooseObject = { bubbles, cancelable, composed },
			eventNames =
				eventName.indexOf(' ') > -1
					? eventName.split(' ')
					: [ eventName ]
		;

		that.#set.forEach( (e: HTMLElement) => {
			eventNames.forEach(function ( n: string ) {
				// console.log()
				ev = new Event( n, options );
				(ev as LooseObject ).data = data;
				e.dispatchEvent( ev );
			});
		});

		return that;
	}

	// INFO:
	// val(): LooseObject; // no param: get all as LooseObject
	// val( p1: string ): Array<string> | number | string; // get from input name
	// val( p1: object ): DomSelector; // set from object
	// val( p1: string, p2: any ): DomSelector; // set from name and value
	val( p1?: any, p2?: any ): any {

		if ( !this.length ) return this;

		const
			that = this
		;

		let result: any,
			handlers: LooseObject = getInputHandlers( that.array[0] );

		// work with it
		if ( !p1 ){ // get all inputs as LooseObject
			result = Object.assign( {}, handlers );
			// Object.keys( handlers ).forEach( ( key ) => result[key] = handlers.hasOwnProperty(key) ? handlers[key].val : undefined );
		} else {
			if ( isObject(p1) ){ // set inputs from Object
				Object.assign( handlers, p1 );
				// Object.keys( p1 ).forEach( ( key ) => !!handlers[key] ? handlers[key].val = p1[key] : false );
				result = that;
			} else if ( typeof p1 === 'string' ){
				if ( p2 === undefined ){ // get a single input
					result = handlers[p1];
				} else { // set a single input
					handlers[p1] = p2;
					result = that;
				}
			}
		}

		return result;
	}

	values( values?: LooseObject ): LooseObject {
		const
			that = this,
			holder: LooseObject = {};

		let
			data: LooseObject = {};

		//console.log( '$d().values()', values );

		if ( !that.length ) {
			console.warn('no dom selection to search inputs in');
			return {};
		}

		let handlers = getInputHandlers( that.array[0] );

		//console.log( 'handlers', Object.keys(handlers), handlers['cb1'] );

		// let values = that.val();

		// console.log( 'values', values );

		Object.keys( handlers ).forEach( (key: string) => {
			//console.log(key);
			data[key] = handlers[key];
		});

		// data = Object.assign( {}, that.val() )
		//console.log( 'data', that.array[0], data, Object.keys( data ) );

		if ( Object.keys( data ).length === 0 ) {
			console.warn('no inputs in this dom selection', that.array[0], data );
		}

		//console.log( '$d val()', data );
		if (values) {
			Object
				.keys( values )
				.forEach( key => {
					if ( handlers.hasOwnProperty(key) ) {
						console.log( 'key found', key );
						handlers[key] = values[key];
					}
				});
				// .forEach( key => handlers[key] = values[key] )
		}

		return structuredClone( Object.assign( handlers ) );
	}

	/*
	array method mapping
	*/

	at( index: number ): DSelector {
		return $d( this.array.at( index ) as HTMLElement ) as DomSelector;
	}

	concat( items: DSelector | any[] ): DSelector{
		let
			that = this,
			arr = items instanceof DomSelector
				? items.array
				: items as HTMLElement[]
		;

		arr.forEach( (e: HTMLElement) => {
			that.#set.add( e );
		});

		return that;
	}

	entries(): IterableIterator<[number, any]> {
		return this.array.entries();
	}

	every( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): boolean {
		return this.array.every( predicate, thisArg );
	}

	filter( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): DSelector {
		this.#set = new Set( this.array.filter( predicate, thisArg ) );
		return this;
	}

	forEach( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): DSelector {
		this.array.forEach( predicate, thisArg );
		return this;
	}

	includes( item: HTMLElement ): boolean{
		return this.array.indexOf( item, 0 ) !== -1;
	}

	indexOf( item: HTMLElement, start?: number ): number{
		return this.array.indexOf( item, start );
	}

	map( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): Array<any>{
		return this.array.map( predicate, thisArg );
	}

	pop(): HTMLElement {
		return this.array.pop() as HTMLElement;
	}

	push( ...items: HTMLElement[] ): DSelector {
		items.forEach( (item: HTMLElement) => {
			this.#set.add( item );
		});
		return this;
	}

	shift(): HTMLElement | undefined {
		let
			that = this,
			element = that.#set.size ? that.array[0] : undefined
		;

		if ( element ){
			that.#set.delete( element );
		}

		return element;
	}

	slice( start: number, end?: number ): DSelector {
		return $d( this.array.slice( start, end ) );
	}

	some( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): boolean{
		return this.array.some( predicate, thisArg );
	}

	splice( start: number, deleteCount?: number | undefined ): DSelector;
	splice( start: number, deleteCount: number, ...items: HTMLElement[] ): DSelector {
		let
			that = this,
			arr = that.array
		;

		arr.splice( start, deleteCount, ...items );
		return $d( arr );
	}

}

export interface DSelector extends DomSelector{};

class TbSelector{

	#set: Set<TB> = new Set();

	public get set(): Set<TB> {
		return this.#set;
	}

	public get array(): TB[] {
		return Array.from( this.#set );
	}

	public set set( set: Set<TB> ) {
		this.#set = set;
		return;
	}

	public get length(): number {
		return this.#set.size;
	}

	public set length( value: number ){
		//ignore
	}

	constructor( selection?: selection, element: HTMLElement = document.body ){

		if ( !selection ) {
			return this;
		}

		$d( selection, element ).forEach( (e: THTMLElement) => {
			$t( e ).forEach( (t: TB ) => {
				this.#set.add(t);
			});
		});

	}

	$d( selection?: selection, element: HTMLElement = document.body ): DSelector {
		let
			result = new DomSelector(),
			set: Set<HTMLElement> = new Set(),
			compare: boolean = !!selection || false,
			compareSelection: Set<HTMLElement> = new Set()
		;

		if ( compare ){
			compareSelection = $d( selection, element ).set;
		}

		this.#set.forEach( (e: TB) => {
			if ( !compare || compareSelection.has( (e as any)._tg ) ) {
				set.add( (e as any)._tg );
			}
		});

		result.set = set;

		return result;
	}

	children( nameSpace?: string ): TSelector{

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e.children( nameSpace ).forEach( ( tb: TB ) => {
				set.add( tb );
			});
		});

		that.#set = set;

		return that;
	}

	descendants( nameSpace?: string ): TSelector{

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e.descendants( nameSpace ).forEach( ( tb: TB ) => {
				set.add( tb );
			});
		});

		that.#set = set;

		return that;
	}

	first( nameSpace?: string ): TSelector{

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e
				.parent()
				.children()
				.$d()
				.first()
				.$t()
				.forEach( (tb: TB) => {
					set.add( tb );
				});
		});

		that.#set = set;

		if ( nameSpace ){
			that.filter( ( tb: TB ) => (tb as any).nameSpace = nameSpace );
		}

		return that;
	}

	has( element: TB ): boolean{
		return this.#set.has( element );
	}

	last( nameSpace?: string ): TSelector{
		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e
				.parent()
				.children()
				.$d()
				.last()
				.$t()
				.forEach( (tb: TB) => {
					set.add( tb );
				});
		});

		that.#set = set;

		if ( nameSpace ){
			that.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return that;
	}

	next( nameSpace?: string ): TSelector{

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			let
				elements: Set<HTMLElement> = new Set(),
				element: HTMLElement
			;

			element = (e as any)._tg;
			while ( element.nextElementSibling !== null ){
				element = element.nextElementSibling as HTMLElement;
				elements.add( element );
			}

			$t([...elements])
				.$d()
				.first()
				.$t()
				.forEach( (tb: TB) => {
					set.add( tb );
				});
		});

		that.#set = set;

		if ( nameSpace ){
			that.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return that;
	}

	ns( nameSpace: string ): TSelector{
		return this.filter( (tb: TB) => (tb as any).nameSpace === nameSpace );
	}

	off( name: string, cb: Function ): TSelector{
		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ').filter( (s) => s !== '' )
					: [ name ]
		;

		// remove handlers
		that.$d().array.forEach( (e: HTMLElement) => {
			eventNames.forEach(function ( n: string ) {
				// console.log('Tb Selector remove event', `[TB]${n}`, e, cb );
				_removeListener(
					e,
					`[TB]${n}`,
					cb as EventListenerOrEventListenerObject
				);
			});
		});

		return that;
	}

	on( name: string, cb: Function, once: boolean = false ): Function{

		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ').filter( (s) => s !== '' )
					: [ name ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		// attach handler
		that.forEach( ( tb: TB) => {
			eventNames.forEach(function ( n: string ) {
				_addListener(
					(tb as any)._tg,
					`[TB]${n}`,
					(wrapper as EventListenerOrEventListenerObject),
					false,
					once
				);
			});
		});

		return wrapper;
	}

	one( name: string, cb: Function ): Function{
		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ').filter( (s) => s !== '' )
					: [ name ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		// attach handler
		that.forEach( ( tb: TB) => {
			eventNames.forEach(function ( n: string ) {
				_addListener(
					(tb as any)._tg,
					`[TB]${n}`,
					(wrapper as EventListenerOrEventListenerObject),
					false,
					true
				);
			});
		});

		return wrapper;
	}

	parent( nameSpace?: string ): TSelector {

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e.parent( nameSpace ).forEach( ( tb: TB ) => {
				set.add( tb );
			});
		});

		that.#set = set;

		return that;
	}

	parents( nameSpace?: string ): TSelector {

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			e.parents( nameSpace ).forEach( ( tb: TB ) => {
				set.add( tb );
			});
		});

		that.#set = set;

		return that;
	}

	prev( nameSpace?: string ): TSelector{

		let
			that: TbSelector = this,
			set: Set<TB> = new Set()
		;

		that.#set.forEach( (e: TB) => {
			let
				elements: Set<HTMLElement> = new Set(),
				element: HTMLElement
			;

			element = (e as any)._tg;
			while ( element.previousElementSibling !== null ){
				element = element.previousElementSibling as HTMLElement;
				elements.add( element );
			}

			$t([...elements])
				.$d()
				.last()
				.$t()
				.forEach( (tb: TB) => {
					set.add( tb );
				});
		});

		that.#set = set;

		if ( nameSpace ){
			that.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return that;
	}

	trigger( ev: string, data: any = {}, bubble: string = 'l' ): TSelector{
		let
			that = this
		;

		that.forEach( ( tb: TB ) => {
			tb.trigger( ev, data, bubble );
		});

		return that;
	}

	/*
	array method mapping
	*/

	at( index: number ): TSelector {
		return $t( this.array.at( index ) ) as TbSelector;
	}

	concat( items: TSelector ): TSelector {

		let
			that = this
		;

		items.forEach( ( tb: TB) => {
			that.#set.add( tb );
		});

		return that;
	}

	entries(): IterableIterator<[number, any]> {
		return this.array.entries();
	}

	every( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): boolean {
		return this.array.every( predicate, thisArg );
	}

	filter( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): TSelector {
		return $t( this.array.filter( predicate, thisArg ) );
	}

	forEach( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): TSelector {
		let
			that = this
		;
		that.array.forEach( predicate, thisArg );
		return that;
	}

	includes( item: TB ): boolean{
		return this.array.indexOf( item ) !== -1;
	}

	indexOf( item: TB, start?: number ): number{
		return this.array.indexOf( item, start );
	}

	map( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): Array<any>{
		return this.array.map( predicate, thisArg );
	}

	pop(): TB {

		let
			that = this,
			arr = that.array,
			result = arr.pop()
		;

		that.#set = new Set( arr );

		return result as TB;
	}

	push( item: TB ): TSelector {
		let
			that = this
		;
		that.#set.add( item );
		return that;
	}

	shift(): TB | undefined {
		let
			that = this,
			first = this.array.shift() as TB
		;
		that.#set.delete( first );
		return first;
	}

	slice( start: number, end?: number ): TSelector {
		return $t( this.array.slice( start, end ) );
	}

	some( predicate: (value: any, index: number, array: any[]) => unknown, thisArg?: any ): boolean{
		return this.array.some( predicate, thisArg );
	}

	splice( start: number, deleteCount?: number | undefined ): TSelector;
	splice( start: number, deleteCount: number, ...items: never[] ): TSelector {
		let
			that = this,
			arr = that.array,
			result = arr.splice( start, deleteCount, ...items )
		;

		return $t( result );
	}

}

export interface TSelector extends TbSelector{};

/*
exported interfaces
*/

export interface LooseObject {
	[key: string | symbol]: any;
}

export interface THTMLElement extends HTMLElement{
	_tb: LooseObject;
}

interface TbEvent extends Event{
	data: any;
	bubble: string;
}

class TbEvent extends Event{
	constructor( type: string, data: any = {}, bubble: string = 'l' ){ // bubble standard = local only
		super( type );
		this.data = data;
		this.bubble = bubble;
	}
}

export interface TEvent extends TbEvent{};

/*
exported helpers
*/

export const debounce = ( func: Function, milliseconds: number ): Function => {
	let timeout: number;
	return () => {

		clearTimeout( timeout );

		timeout = setTimeout(
			function(){
				func( ...arguments );
			},
			milliseconds
		);
	};
};

export const nameSpace = ( ns: string, obj: LooseObject, val: any = null, orig?: any ): any => {
	let leftOver: Array<any> = ns.split('.');

	if ( !orig ){
		orig = obj;
	}

	// console.log( 'start', ns, obj, val, leftOver );

	let
		last: string = leftOver.shift(),	// this property name
		lns: string = leftOver.join('.')	// the leftover namespace
	;

	if ( val !== null && last.length && obj[last] === undefined ){ // extend if val given and ns leftover...
		// console.log( 'add prop', last );
		obj[last] = {};
	}

	let
		lob: any = obj[last]	// the leftover value or obj
	;

	// console.log( 'lob', lob, 'last:', last, 'lns:', lns, 'obj:', obj );

	if ( lns.length === 0 ){ // this should be the value to set or get
		if ( val !== null ){
			obj[last] = val;
			return orig;
		}
		return obj[last];
	}

	if ( isObject( obj[last] ) ){
		return nameSpace( lns, obj[last], val, orig );
	} else {
		if (val){
			return orig;
		}

		return lob[last];
	}

}

export const parse = function( what: string | Object | any[], parseThis: Object ){
	var args = Array.from(arguments);

	if (!args.length){
		console.error('no arguments give to parse');
		return;
	}

	if (args.length === 1){
		return args[1];
	} else if (args.length > 2) {
		while (args.length > 1){
			args[0] = parse( args[0], args[1]);
			args.splice(1, 1);
		}
		return args[0];
	}

	// implicit else: exactly 2 arguments
	if ( typeof what === 'string' ){
		var vars = what.match( /\{[^\{\}]*\}/g );

		if ( !!vars ) {
			vars
				.forEach(
					function (pPropname) {
						var propname = pPropname.substr(1, pPropname.length - 2),
							value = nameSpace( propname, parseThis );

						if ( typeof value !== 'undefined' ){
							what = (what as string).replace( pPropname, value );
						}
					}
				);
		}
	} else if ( isObject(what) ){
		switch ( what.constructor ){
			case Object:
				Object
					.keys( what )
					.forEach(
						function( pKey ){
							if ( what.hasOwnProperty( pKey ) ){
								( what as LooseObject )[ pKey ] = parse( ( what as LooseObject )[ pKey ], parseThis );
							}
						}
					);
				break;
			case Array:
				( what as Array<any> )
					.forEach(
						function( pValue, pKey, original ){
							original[ pKey ] = parse( (what as any)[ pKey ], parseThis );
						}
					);
				break;
		}
	}

	return what;
};

export interface TB{
	_id: string;
	_in: any;
	_ns: string;
	_sh: ShadowRoot;
	_tg: HTMLElement;
}

export class TB{

	constructor( target: HTMLElement, data?: any){
		let that: any = this;
		Object.defineProperty( that, '_id', { enumerable: false, writable: false, configurable: false, value: getId() } );
		Object.defineProperty( that, '_tg', { get: () => { return target } } );
		Object.defineProperty( that, '_in', { value: data, writable: false, configurable: false } );
	}

	children( nameSpace?: string ): TSelector{

		let
			that = this,
			id: string = getId(),
			target = that._tg;
		;

		// generate temp attribute
		target.setAttribute( 'data-tempid', id );

		let selector = $d(target)
				.descendants()
				.$t()
				.filter( e => $t(e).parent().$d().attr('data-tempid') === id );

		// remove temporary attribute
		target.removeAttribute( 'data-tempid' );

		if ( nameSpace ){
			selector.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return selector;
	}

	descendants( nameSpace?: string ): TSelector{

		let
			that = this,
			id: string = getId(),
			target = that._tg;
		;

		// generate temp attribute
		target.setAttribute( 'data-tempid', id );

		let selector = $d(target)
				.descendants()
				.$t();

		// remove temporary attribute
		target.removeAttribute( 'data-tempid' );

		if ( nameSpace ){
			selector.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return selector;
	}

	off( name: string, cb: Function ){
		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ')
					: [ name ]
		;

		eventNames.forEach( (name) => {
			$d( that._tg ).off( `[TB]${name}`, cb );
		})

		return this;
	}

	on( name: string, cb: Function, once: boolean = false ): Function{

		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ')
					: [ name ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		eventNames.forEach( (n) => {
			_addListener(
					that._tg,
					`[TB]${name}`,
					(wrapper as EventListenerOrEventListenerObject),
					false,
					once
				);

		})

		return wrapper;
	}

	one( name: string, cb: Function ): Function{
		let
			that = this,
			eventNames =
				name.indexOf(' ') > -1
					? name.split(' ')
					: [ name ]
		;

		function wrapper(){ wrapper.cb.apply(that, Array.from(arguments) ); };
		wrapper.cb = cb;

		eventNames.forEach( (n) => {
			_addListener(
					that._tg,
					`[TB]${name}`,
					(wrapper as EventListenerOrEventListenerObject),
					false,
					true
				);

		})

		return wrapper;
	}

	parent( nameSpace?: string ): TSelector{
		let
			that = this,
			result: TbSelector
		;

		result = $d(that._tg)
			.parents()
			.filter( e => e.tb().length )
			.first()
			.$t();

		if ( nameSpace ){
			result.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return result;
	}

	parents( nameSpace?: string ): TSelector{
		let
			that = this,
			result: TbSelector
		;

		result = $d(that._tg)
			.parents()
			.filter( e => e.tb().length )
			.$t();

		if ( nameSpace ){
			result.filter( ( e: TB ) => ( e as LooseObject).nameSpace = nameSpace );
		}

		return result;
	}

	shadow( style: string | HTMLStyleElement ): ShadowRoot{
		let that = this;

		if ( !that._sh){
			that._sh = that._tg.attachShadow({ mode: "open" });
			(that._sh as any).close = () => {
				that._sh = that._tg.attachShadow({ mode: "closed" });
			}
		} else {
			console.warn('IGNORED: shadow DOM already created! You may modify this._sh, however...');
		}

		if ( style ){
			if ( style instanceof HTMLStyleElement ){
				that._sh.appendChild( style );
			} else { // a style string
				let styleElement = document.createElement('style');
				styleElement.textContent = style.replace( /[\s]/g, ' ').replace( /  /g, ' '); // shorten it
				that._sh.appendChild( styleElement );
			}
		}
		return that._sh;
	}

	siblings( nameSpace?: string ): Object | TB | void{
		let that = this,
			target: any = that._tg
		;
		if (nameSpace !== undefined){
			let i = target._tb[nameSpace];
			return i;
		}
		return target._tb;
	}

	values( name: string, initial: Object = {} ){
		//console.log( '[TB].values(', name, ',', initial, ')', this );
		let that = this;
		(this as LooseObject)[name] = values( that._sh !== undefined ? that._sh : that._tg, initial, that, name );
	}

	trigger( ev: TEvent | string, data: any = {}, bubble: string = 'l' ): TB{

		let
			that = this,
			tbEvent: TEvent = ev instanceof TbEvent ? ev : new TbEvent( ev as string, data, bubble );
		;

		// if event __stopped__ , handling is cancelled
		// if ( (ev as TbEvent).__stopped__ || (ev as TbEvent).__immediateStopped__ ) {
		// 	return that;
		// }

		$d( (that as any)._tg ).trigger( tbEvent.type, data );

		// if event __stopped__ , handling is cancelled
		// if ( !!tbEvent.__stopped__  ) {
		// 	return that;
		// }

		setTimeout(
			() => {

				// bubble up
				if ( tbEvent.bubble.indexOf('u') > -1 ){
					(that as any)
						.parent()
						.trigger(
							new TbEvent(
								tbEvent.type,
								tbEvent.data,
								'lu'
							));
				}

				// bubble down
				if ( tbEvent.bubble.indexOf('d') > -1 ){
					(that as any)
						.children()
						.forEach( (tbInstance: TB) => {
							tbInstance.trigger(
								new TbEvent(
									tbEvent.type,
									tbEvent.data,
									'ld'
								)
							);
						})
					;
				}

			},
			0
		);

		return that;
	}

}

/*
functions
*/

export const values = ( target: HTMLElement | ShadowRoot, values: Object = {}, tb?: TB, observable?: string ): LooseObject => {

	//console.log( 'values(', target, ',', values, ',', observable, ')' );

	//console.log( 'values()', values );
	let handlers = getInputHandlers( target );

	function updateForm( values: Object ){
		Object
			.keys( values )
			.forEach( (key) => {
				if ( handlers.hasOwnProperty(key) && values.hasOwnProperty(key) ) {
					//console.log( 'set value', key, values[key] );
					handlers[key] = (values as LooseObject)[key];
				}
		});
	}

	if ( Object.keys(values).length > 0 ){
		updateForm( values );
	}

	//console.log( 'set observable on form change', observable );
	if ( observable !== undefined ){
		let obs = ( tb as LooseObject)[observable];
		if ( !obs.constructor?.hasFormHandler ){
			obs.constructor.hasFormHandler = true;
			obs.observe( updateForm );
		}
		( tb as any)[observable].observe( () => { } );
		( tb as any)[observable].observe( () => { } );
		$d('input, textarea', target).on( 'keyup change', () => { ( tb as any)[observable] = structuredClone( Object.assign( handlers ) )} );
		$d('select', target).on( 'keyup select change', () => { ( tb as any)[observable] = structuredClone( Object.assign( handlers ) )} );
	}

	return observable ? ( tb as any)[observable] : Object.assign( {}, handlers );
}

export const load = ( fileName: string ): Promise<any> => {
	let promise: Promise<any>;

	// special handling for .css and .js files
	if ( /\.css$/.test( fileName ) ){
		// css file
		//console.log( `css file: ${fileName}` );
		promise = new Promise( ( resolve, reject ) => {
			let link = document.createElement('link');
			link.setAttribute('rel', 'stylesheet');
			link.setAttribute('href', fileName);

			link.onload = () => {
				loadingScripts.delete( fileName );
				resolve( 'ccs loaded ' + fileName );
			};

			link.onerror = () => {
				loadingScripts.delete( fileName );
				console.warn('could not load '+ fileName);
				resolve( 'ccs NOT loaded!' );
			}
			$d(document.head).append( link );
		});
	} else if ( /\.js$/.test( fileName ) ){
		// js file, assumed to be a module
		//console.log( `js file: ${fileName}` );
		promise = new Promise( ( resolve, reject ) => {
			const se: HTMLScriptElement = document.createElement('script');

			se.setAttribute( 'src', fileName );
			se.setAttribute( 'blocking', 'render' );
			se.setAttribute( 'type', 'module' );

			loadingScripts.add(fileName);

			se.onload = () => {
				loadingScripts.delete( fileName );
				resolve( 'js loaded ' + fileName );
			};

			se.onerror = () => {
				loadingScripts.delete( fileName );
				console.warn('could not load '+ fileName);
				resolve( 'js NOT loaded!' );
			}
			document.head.append( se );
		});
	} else {
		// other file
		// console.log( `other file: ${fileName}` );

		promise = fetch(fileName).then((p) => p.text());
	}

	//console.log( promiseArray );
	return promise;
}

export const loadAll = ( ...files: string[] ): Promise<any> => {
	let promiseArray: Promise<any>[] = [];

	// sanitze input ( flatten input array using whitespaces )
	files = words( files );

	// load files
	// special handling for .css and .js files
	files.forEach( fileName => {
		promiseArray.push( load( fileName ) );
	});

	return Promise.all( promiseArray );
}

export const $d = ( selection: selection, element: any = document ): DSelector => {

	let
		set = new Set<HTMLElement>()
	;

	if ( selection instanceof Array ){
		selection.forEach( value => {
			$d( value, element ).forEach( n => {
				set.add( n );
			});
		});
		return new DomSelector( [...set] );
	} else {
		return new DomSelector( selection, element );
	}
}

export const $t = ( selection?: selection, element: any = document ): TSelector => {
	return $d( selection, element ).$t();
}

const _convertToObservables = ( target: LooseObject, propertyName?: string ) => {

	Object.keys(target).forEach( (key) => {
		if ( key[0] === '_' ) return;

		let
			val: any = target[key],
			Constructor: any
		;

		if ( !val.prototype ){
			switch ( typeof val ){
				case "string":
					Constructor = String;
					break;
				case "number":
					Constructor = Number;
					break;
				case "boolean":
					Constructor = Boolean;
					break;
				case "object":
					if ( val instanceof Array){
						Constructor = Array;
					} else {
						Constructor = Object;
					}
					break;
				default:
					return;
			}

		}

		let Observable = class extends Constructor{

			constructor( val: any ){
				super( val );
				(this.constructor as LooseObject).callbacks = [];
				if ( isObject(val) ){ // you cannot bind native values to the dom
					(this.constructor as LooseObject).bound = [];
					Object.getOwnPropertyNames( val ).forEach( (key) => {
						this[key] = val[key];
					});
				}
			}

			observe( f: Function ){
				//console.log('observe');
				(this.constructor as any).callbacks.push( f );
				return this;
			}

			bind( target: HTMLElement | ShadowRoot ){

				const
					that = this,
					iso = isObject( that.valueOf() ),
					bound = (that.constructor as any).bound
				;

				//console.log('bind', iso, target);

				if ( iso ){
					// console.log('bind...', that.valueOf(), target );

					function walk( element: Node ){
						//console.log('-walk:', element, element.childNodes );

						if ( !!element['nodeType'] && element.nodeType === 3 ){ // text node
							//console.log('--type: textNode(3)');

							var placeholders = Array.from( element.nodeValue && element.nodeValue.match( /\{[^\{\}]*\}/g ) || []);

							if ( placeholders.length > 0 ){
								//console.log('--placeholders:', placeholders);
								var f=(function( template: string ){
									//console.log('template', template);
									return function( values: any ){
										var t,
											changed = false;

										//console.log( 'placeholders:', placeholders, 'values:', values);
										if ( placeholders ){
											if ( iso ){
												placeholders.forEach(function(placeholder){
													if ( ( f as LooseObject).values[placeholder] !== values[placeholder] ){
														(f as LooseObject).values[placeholder] = values[placeholder];
														changed = true;
													}
												});
											} else { // it is a simple value
												(f as LooseObject).values = values;
												changed = true;
											};
										}
										if (changed){ // only reflow if changed
											t = template;
											//console.log('--template parse', t, (f as LooseObject).values );
											if ( iso ){
													element.nodeValue = parse(
														t,
														(f as LooseObject).values
													);
											} else { // it is a simple value -> replace every placeholder whatever the name wi
												element.nodeValue = t.replace( /\{.*\}/g ,(f as LooseObject).values );
											};
										}
										return values;
									};
								})( element.nodeValue || '' );

								(f as LooseObject).values = iso ? {} : undefined;
								placeholders = Array.from( placeholders ).map((pKey) => pKey.replace(/[\{\}]/g, '').trim());

								let initial: any = iso ? {} : undefined;
								placeholders.forEach(function(pKey){
									if ( iso ){
										initial[pKey] = "";
									}
								});

								bound.push(f);
								// console.log( `--bound textNode`, placeholders, f );
								//(observableFunction as Function)(initial);
							}

							//console.log('--childNodes:', element.childNodes );
							Array.from( element.childNodes )
								.forEach(function( childNode ){
									walk( childNode );
								});

							that.notify();
						}

						if ( !!element['nodeType'] && element.nodeType === 1 ){ // HTML element
							//console.log('--type: HTMLElement(1)');
							Array.from( (element as HTMLElement).attributes )
								.forEach(
									function( attributeNode ){
										var placeholders = Array.from( attributeNode.value && attributeNode.value.match( /\{[^\{\}]*\}/g ) || []);

										if ( placeholders.length > 0 ){
											//console.log( `--[${attributeNode.value}] placeholders:`, placeholders );

											var f=(function( template: string ){
												return function( values: any ){
													var t,
														changed = false;

													//console.log( 'placeholders:', placeholders, 'values:', values);
													if ( placeholders ){
														placeholders.forEach(function(pKey){
															if ( ( f as LooseObject).values[pKey] !== values[pKey] ){
																(f as LooseObject).values[pKey] = values[pKey];
																changed = true;
															}
														});
													}
													if (changed){ // only reflow if changed
														t = template;
														element.nodeValue = parse(
															t,
															(f as LooseObject).values
														);
													}
													return values;
												};
											})( attributeNode.value || '' );

											(f as LooseObject).values = {};
											placeholders = Array.from( placeholders ).map((pKey) => pKey.replace(/[\{\}]/g, '').trim());
											let initial: LooseObject = {};
											placeholders.forEach(function(pKey){
												initial[pKey] = "";
											});

											bound.push(f);
											// console.log( `--bound element attribute`, placeholders, f );
											// console.log( `--bound`, observableFunction.bound );
											//(observableFunction as Function)(initial);
										}
									}
								);

							//console.log('--childNodes:', element.childNodes );
							Array.from( element.childNodes )
								.forEach(function( childNode ){
									walk( childNode );
								});

							that.notify();

						}

						if ( !!element['nodeType'] && element.nodeType === 11 ){ // shadowRoot node
							// console.log( element, element.childNodes );
							Array.from( element.childNodes )
								.forEach(function( childNode ){
									walk( childNode );
								});
						}
					}

					walk( target );

					if ( bound.length > 0 ){
						that.notify();
					}
				} else {
					console.warn( 'IGNORED - cannot bind a non-Object to the dom, IS:', typeof that.valueOf() );
				}

				return this;
			}

			notify(){
				let
					that = this,
					data = isObject( that.valueOf() ) && ( that.valueOf() instanceof Array ) === false
						? structuredClone( Object.assign({}, that) )
						: that.valueOf()
				;

				// console.log('notify', that.constructor, (that.constructor as any).callbacks.length, (that.constructor as any)?.bound?.length, data );
				(that.constructor as any).callbacks.forEach( (f: Function) => {
					// console.log( 'callback', f, data);
					f( data );
				});
				if ( isObject( that.valueOf() ) ){
					// console.log('bound', this.constructor);
					(that.constructor as any).bound.forEach( (f: Function) => {
						// console.log( 'bound', f, data);
						f( data );
					});
				}
				return this;
			}
		}

		//(Observable.constructor as LooseObject).callbacks = [];
		let o = new Observable( val );

		Object.defineProperty(
			target,
			key,
			{
				enumerable: true,
				get(){
					if ( isObject( o.valueOf() ) ){
						let check = structuredClone( Object.assign( {}, o ) );
						setTimeout(
							debounce(
								function checkChanges(){
									let state = structuredClone( Object.assign( {}, o ) );
									//console.log( 'check changes', state, check );
									if ( !deepEqual( state, check ) ){
										//console.log( '-> not equal!' );
										o.notify();
									}
								},
								0
							),
							0
						);
					}
					return o;
				},
				set( val: any ){
					//console.log( 'SET:', isObject( val.valueOf() ), 'set:', val, 'to', isObject( o.valueOf() ), o, );
					if ( typeof val === typeof o.valueOf() ){

						let iso = isObject( o.valueOf() ) && ( o.valueOf() instanceof Array ) === false;
						//let iso = o.constructor.prototype.__proto__.constructor === Object;
						//console.log('target iso:', iso);
						if ( iso ){
							let
								old = structuredClone( Object.assign( {}, o ) )
							;

							val = structuredClone( Object.assign( {}, val ) );

							let
								hasFormHandler = (o.constructor as LooseObject)?.hasFormHandler ?? false,
								callbacks = (o.constructor as LooseObject).callbacks,
								bound = (o.constructor as LooseObject).bound
							;

							o = new Observable( structuredClone( Object.assign( {}, val ) ) );

							//console.log('after set o=', o, 'check', old);
							(o.constructor as LooseObject).callbacks = hasFormHandler;
							(o.constructor as LooseObject).callbacks = callbacks;
							(o.constructor as LooseObject).bound = bound;

							setTimeout( // check for changes
								debounce(
									function checkChanges(){
										let state = structuredClone( Object.assign( {}, o ) );
										//console.log( 'check changes', state, check );
										if ( !deepEqual( state, old ) ){
											//console.log( '-> not equal!' );
											o.notify();
										}
									},
									0
								),
								0
							);
						} else { // simple native var
							let
								old = o.valueOf(),
								callbacks = (o.constructor as LooseObject).callbacks
							;
							o = new Observable( val );
							(o.constructor as LooseObject).callbacks = callbacks;
							if ( val !== old ){
								o.notify();
							}
						}
					} else {
						console.warn( 'IGNORED - cannot change observable types, IS:', typeof o.valueOf(), '- CANNOT BECOME:', typeof val );
					}
				}
			}
		);

		target[key] = val;

	});

}

export function createTb<T extends TB>( target: unknown, definitionClass: new ( target: HTMLElement, data: any ) => T, data: any = {} ): T {
	//console.log( 'createTbInstance', target, definitionClass, data );

	if ( !target ){
		throw new Error('no target HTML element given');
	}
	if ( !definitionClass ){
		throw new Error('no definition class given');
	}

	const instance = new definitionClass( target as THTMLElement, data );

	_autoAttachListeners( target as HTMLElement, instance ); // auto-attaches all native and tb event listeners

	(target as THTMLElement)._tb = (target as THTMLElement)._tb || {};
	(target as THTMLElement)._tb[definitionClass.name] = instance;
	//console.log( 'in cTb', target, data )

	_convertToObservables( instance );

	let nameSpace = definitionClass.name || (target as THTMLElement).tagName.toLowerCase().split('-').join('.');
	Object.defineProperty( instance, '_ns', { value: nameSpace, writable: false, configurable: false } );

	(target as THTMLElement).dispatchEvent( new Event('[TB]oneInit') );
	(target as THTMLElement).dispatchEvent( new Event('[TB]onInit') );

	return instance;
}

const createCustomElement = ( tagName: string, definitionClass: { new(...args: any[]): any; }, observedAttributes: String[] = [], userDefinedBuiltinObject: string  = 'x' ) => {
	//console.log( 'createCustomElement', tagName, definitionClass, observedAttributes, userDefinedBuiltinObject );
	// create custon element
	if( customElements.get( tagName ) === undefined ){

		let
			wantUBE = userDefinedBuiltinObject === 'x' ? false : true,
			extendClass: any = HTMLElement
		;

		//console.log( 'wantUBE', wantUBE );
		//console.log( 'create custom element' );

		// parameter sanitation
		if ( !userDefinedBuiltinObject ){
			if (tagName.indexOf('-') === -1){
				throw('createCustomElement: pTagName must be a string containing at least one hyphen: "-" ');
			}
		} else {
			let e = document.createElement(userDefinedBuiltinObject);

			if ( e instanceof HTMLUnknownElement ){
				//console.log(`[${userDefinedBuiltinObject}] is an unknown HTML element: default to HTMLElement`);
			} else {
				extendClass = (window as LooseObject)[ document.createElement(userDefinedBuiltinObject).constructor.name ];
			}
		}

		// auto-define autonomous custom element
		customElements.define(
			tagName,
			class extends extendClass{

					constructor(data:any){
						super();
						let that: unknown = this;
						createTb(
							( that as THTMLElement ),
							definitionClass,
							data
						);
					}

					static get observedAttributes(){
						return observedAttributes;
					}

					connectedCallback(){
						let that: unknown = this;
						//console.log( 'createCustomElement trigger [TB]oneConnected' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]oneConnected' ) );
						//console.log( 'createCustomElement trigger [TB]onConnected' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]onConnected' ) );
					}

					disconnectedCallback(){
						let that: unknown = this;
						//console.log( 'createCustomElement trigger [TB]oneDisconnected' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]oneDisconnected' ) );
						//console.log( 'createCustomElement trigger [TB]onDisconnected' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]onDisconnected' ) );
					}

					adoptedCallback(){
						let that: unknown = this;
						//console.log( 'createCustomElement trigger [TB]oneAdopted' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]oneAdopted' ) );
						//console.log( 'createCustomElement trigger [TB]onAdopted' );
						( that as HTMLElement ).dispatchEvent( new Event( '[TB]onAdopted' ) );
					}

					attributeChangedCallback( name: string, oldValue: any, newValue: any ){
						let that: unknown = this,
							ev: Event,
							data = { name: name, oldValue: oldValue, newValue: newValue };

						//console.log( 'createCustomElement trigger [TB]oneAttributechanged' );
						ev = new Event( '[TB]oneAttributeChanged' );
						( ev as any ).data = data;
						( that as HTMLElement ).dispatchEvent( ev );

						//console.log( 'createCustomElement trigger [TB]onAttributechanged' );
						ev = new Event( '[TB]onAttributeChanged' );
						( ev as any ).data = data;
						( that as HTMLElement ).dispatchEvent( ev );

					}

			} as any,
			wantUBE
				? ( { extends: userDefinedBuiltinObject.toLocaleLowerCase() } as ElementDefinitionOptions )
				: undefined
		);

	}

}

const getCustomElement = ( tagName: string ): CustomElementConstructor | undefined => {
	return customElements.get( tagName );
};

const CEconstructors: LooseObject = {};
const CEinventory: Set<string> = new Set();

export class CE{
	constructor( tagName: string, definitionClass: { new(...args: any[]): any; }, observedAttributes: String[] = [] ){
		if ( !CEinventory.has(tagName) ){
			createCustomElement( tagName, definitionClass, observedAttributes );
			CEinventory.add( tagName );
			CEconstructors[ definitionClass.name ] = getCustomElement( tagName );
		} else {
			console.warn('IGNORED: new CE(...) - custom element', tagName, 'is already defined!', [...CEinventory])
		}
	}

	static get( tagName?: string ): Set<any> | { new(...args: any[]): any; } | void {
		if ( tagName ){
			let constructor: any = getCustomElement(tagName);
			return constructor;
		}
		return CEinventory;
	}
}

const UBEconstructors: LooseObject = {};
const UBEinventory: Set<string> = new Set();

export class UBE{
	constructor( tagName: string, definitionClass: { new(...args: any[]): any; }, observedAttributes: String[] = [], userDefinedBuiltinObject: string = 'x' ){
		if ( !UBEinventory.has(tagName) ){
			createCustomElement( tagName, definitionClass, observedAttributes, userDefinedBuiltinObject );
			UBEinventory.add( tagName );
			UBEconstructors[ definitionClass.name ] = getCustomElement( tagName );
		} else {
			console.warn('IGNORED: new UBE(...) - user defined built-in element', userDefinedBuiltinObject, 'is already defined!', [...UBEinventory])
		}
	}

	static get( tagName?: string ): Set<any> | { new(...args: any[]): any; } | void {
		if ( tagName ){
			let constructor: any = getCustomElement(tagName);
			return constructor;
		}
		return UBEinventory;
	}
}
