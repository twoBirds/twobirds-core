tb.createCustomElement(
    
    'test-greatgrandchild',
    
    class GreatGrandChild extends Tb{

        constructor( pConfig, pTarget ){
            super( pConfig, pTarget );

            let that = this;

            that.target = pTarget;

            that
                .one(
                    'connected',
                    that.connected
                );

            console.log('greatgrandchild constructor');
        }

        connected(){
            let that = this;

            that.updateStyle();
        }

        updateStyle(){

            var that = this;

            function random(min,max) {
                var random = Math.floor(Math.random()*(max-min+1)+min);
                return random;
            }

            function randomBorderColor(){
                return 'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) + ')';
            }

            $(that.target).attr('style', 'border-color:'+randomBorderColor() );

        }

    }

);
