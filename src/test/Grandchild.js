tb.createCustomElement(
    
    'test-grandchild',
    
    class GrandChild extends Tb{

        constructor( pConfig, pTarget ){
            super( pConfig, pTarget );

            let that = this;

            that.target = pTarget;

            that
                .one(
                    'connected',
                    that.connected
                )
                .one(
                    'ready',
                    that.ready
                );

            console.log('grandchild constructor');
        }

        connected(){
            let that = this,
                fragment = $( '<test-greatgrandchild/>'.repeat(2) );

            // add fragment to DOM
            $( that.target ).append( fragment );
        }

        // methods
        ready(){
        }

    }

);
