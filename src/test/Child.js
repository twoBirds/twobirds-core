tb.createCustomElement(
    
    'test-child',
    
    class Child extends Tb{

        constructor( pConfig, pTarget ){
            super( pConfig, pTarget );

            let that = this;

            that.target = pTarget;

            that
                .one(
                    'connected',
                    that.connected
                )
                .one(
                    'ready',
                    that.ready
                );

            // simple internal classes...
            that.Embedded1 = ( class Embedded1 extends Tb{ 
                constructor(){
                    super();
                    this.embedded2 = new that.Embedded2();
                }
            });

            that.Embedded2 = ( class Embedded2 extends Tb{ 
                constructor(){
                    super();
                }
            });

            console.log('child constructor');
        }

        connected(){
            let that = this,
                fragment = $( '<test-grandchild/>'.repeat(3) );

            // add fragment to DOM
            $( that.target ).append( fragment );
        }

        // methods
        ready(){
            let that = this;

            that.embedded1 = new that.Embedded1();
        }

        test( e ){
            var that = this;

            if ( e.data instanceof test.Embedded2 ){
                e.stopPropagation();
            }
            //console.info( 'child ::test() reached' );
        }

    }

);
