tb.createCustomElement(

    'test-grandparent',

    class GrandParent extends Tb{

        constructor( pConfig, pTarget ){
            super( pConfig, pTarget );

            let that = this;

            that.target = pTarget;

            that
                .one(
                    'connected',
                    that.connected
                )
                .one(
                    'ready',
                    that.ready
                );

            console.log('grandparent constructor');
        }

        connected(){
            let that = this,
                fragment = $( '<test-parent/>'.repeat(5) );

            $( that.target ).append( fragment );
            console.log('grandparent connected');
        }

        // methods
        ready(){
            console.log('grandparent ready');
        }

    }

)

