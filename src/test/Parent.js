tb.createCustomElement(
    
    'test-parent',
    
    class Parent extends Tb{

        constructor( pConfig, pTarget ){
            super( pConfig, pTarget );

            let that = this;

            that.target = pTarget;

            that
                .one(
                    'connected',
                    that.connected
                )
                .one(
                    'ready',
                    that.ready
                );

            console.log('parent constructor');
        }

        connected(){
            let that = this,
                fragment = $( '<test-child/>'.repeat(10) );

            $( that.target ).append( fragment );
        }

        // methods
        ready(){
        }

    }

);
