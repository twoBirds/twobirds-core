YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "tb",
        "tb.Event",
        "tb.Require",
        "tb.Selector",
        "tb.Util",
        "tb.dom"
    ],
    "modules": [],
    "allModules": [],
    "elements": []
} };
});